(in-package :chrest-avow)

; some tests

(defun test-circuit-avow ()
  (let ((circuit1 (create-new-circuit-pattern 
                   (list
                    (create-new-resistor 'A 'POS 'GND '(1 1)))))
        (circuit2+1 (create-new-circuit-pattern
                     (list
                      (create-new-resistor 'A 'POS 'ONE '(1 1))
                      (create-new-resistor 'B 'ONE 'GND '(1 3))
                      (create-new-resistor 'C 'POS 'GND '(2 2)))))
        (circuit2-1 (create-new-circuit-pattern
                     (list
                      (create-new-resistor 'A 'POS 'ONE '(1 1))
                      (create-new-resistor 'B 'POS 'ONE '(3 1))
                      (create-new-resistor 'C 'ONE 'GND '(2 2)))))
        (circuits (create-new-circuit-pattern 
                   (list 
                    (create-new-resistor 'A 'POS 'ONE '(1 1))
                    (create-new-resistor 'B 'ONE 'GND '(1 2)))))
        (circuit-trial (create-new-circuit-pattern
                        (list
                         (create-new-resistor 'A 'POS 'ONE '(3 1))
                         (create-new-resistor 'B 'POS 'TWO '(5 1))
                         (create-new-resistor 'C 'ONE 'THREE '(1 2))
                         (create-new-resistor 'D 'ONE 'THREE '(3 2))
                         (create-new-resistor 'E 'ONE 'TWO '(4 2))
                         (create-new-resistor 'F 'THREE 'GND '(2 3))
                         (create-new-resistor 'G 'TWO 'GND '(5 3)))))        
        )

    (check (equal-patterns-p (circuit-avow circuit1)
                                  (create-new-avow-pattern (create-box-feature 'A)))
                "circuit-avow 1")
    (check (equal-patterns-p (circuit-avow circuits)
                                  (create-new-avow-pattern
                                   (list
                                    (create-reln-feature (bottom-of 'A) (top-of 'B))
                                    (create-reln-feature (left-of 'A) (left-of 'B))
                                    (create-reln-feature (right-of 'A) (right-of 'B)))))
                "circuit-avow 2")
    (check (equal-patterns-p (circuit-avow circuit2+1)
                                  (create-new-avow-pattern
                                   (list
                                    (create-reln-feature (bottom-of 'A) (top-of 'B))
                                    (create-reln-feature (top-of 'A) (top-of 'C))
                                    (create-reln-feature (bottom-of 'B) (bottom-of 'C))
                                    (create-reln-feature (right-of 'A) (left-of 'C))
                                    (create-reln-feature (right-of 'B) (left-of 'C))
                                    (create-reln-feature (left-of 'A) (left-of 'B))
                                    (create-reln-feature (right-of 'A) (right-of 'B)))))
                "circuit-avow 3")
    (check (equal-patterns-p (circuit-avow circuit2-1)
                                  (create-new-avow-pattern
                                   (list
                                    (create-reln-feature (top-of 'A) (top-of 'B))
                                    (create-reln-feature (bottom-of 'A) (bottom-of 'B))
                                    (create-reln-feature (bottom-of 'A) (top-of 'C))
                                    (create-reln-feature (bottom-of 'B) (top-of 'C))
                                    (create-reln-feature (right-of 'A) (left-of 'B))
                                    (create-reln-feature (left-of 'A) (left-of 'C))
                                    (create-reln-feature (right-of 'B) (right-of 'C)))))
                "circuit-avow 4")
    (check (equal (get-first-from (get-node-in 'TWO (get-node-list circuit-trial)))
                       'E)
                "Get first from")
    (check (equal (get-last-from (get-node-in 'TWO (get-node-list circuit-trial)))
                       'B)
                "Get last from")
    (check (equal-lists-p (get-horizontal-relations (get-node-in 'TWO (get-node-list circuit-trial)))
                               (list 
                                (create-reln-feature (left-of 'E) (left-of 'G))
                                (create-reln-feature (right-of 'B) (right-of 'G))
                                (create-reln-feature (right-of 'E) (left-of 'B))) 
                               #'equal-features-p )
                "Troublesome node")
    ))

