(in-package :chrest-avow)

; **** simple tests for this class ****

(defun test-circuit ()
  ; check circuit and curriculum create get- set-
  (let ((a-circuit (create-new-circuit)))
    (set-circuit a-circuit (create-new-circuit-pattern '(A B C)))
    (check (equal (get-pattern (circuit a-circuit)) '(A B C))
           "alter circuit")
    (set-name a-circuit "Parallel")
    (check (equal (name a-circuit) "Parallel")
           "alter circuit name"))
  (let ((a-curr (create-new-curriculum)))
    (set-curriculum a-curr '(A B C))
    (check (equal (curriculum a-curr) '(A B C))
           "alter curriculum")
    (set-name a-curr "Basic")
    (check (equal (name a-curr) "Basic")
           "alter curriculum name"))
  ; check the description function
  (check (equal (describe-scene (create-new-circuit
                                       "Series"
                                       (create-new-circuit-pattern
                                         (list 
                                           (create-new-resistor 'A 'POS 'ONE '(1 1))
                                           (create-new-resistor 'B 'ONE 'GND '(1 2))))))
                (list (list 'A 'POS 'ONE)
                      (list 'B 'ONE 'GND)))
         "describe-scene"))

