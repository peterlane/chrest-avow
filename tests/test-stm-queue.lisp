(in-package :chrest-avow)

; some tests for this class

(defun test-stm-queue ()
  (let* ((my-obs (create-new-observer))
         (dummy-subject (create-new-chrest-subject my-obs "Dummy" (create-new-network-root)))
         (stm-1 (new-stm-queue dummy-subject))
         (stm-2 (new-stm-queue dummy-subject 2))
         (pat-list (loop for i from 1 to 10
                         collect (create-new-pattern (create-new-feature i)))))
    (check (= (stm-size stm-1) 4)
                "new-stm-queue")
    (check (= (stm-size stm-2) 2)
                "new-stm-queue")
    (dolist (a-pat pat-list)
      (add-to-stm stm-1 (make-stm-item :pattern a-pat :node nil)))
    (check (= (stm-size stm-1) (length (stm-list stm-1)))
                "add-to-STM")
    (dolist (a-pat pat-list)
       (add-to-stm stm-2 (make-stm-item :pattern a-pat :node nil)))
    (check (= (stm-size stm-2) (length (stm-list stm-2)))
                "add-to-STM")
    (clear-stm stm-1)
    (check (endp (stm-list stm-1))
                "clear-STM")
    ))

