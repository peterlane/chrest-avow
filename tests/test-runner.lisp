(in-package :chrest-avow)

;; used in each file to create a tester - should see a row of dots, unless 
;; the test fails, when the error-message is displayed.
(defun check (query error-message)
  (if query
    (format t ".")
    (format t "~&Error: ~a~&" error-message)))

;; for batch testing, chain the test routines here.
(defun run-tests ()
  (format t "~&Testing CHREST: ")
  (test-avow-pattern)
  (test-chrest+)
  (test-chrest-subject)
  (test-circuit)
  (test-circuit-pattern)
  (test-resistor)
  (test-stm-queue)
  (test-utility)
  (format t "~%--------------- Done~&")
  )


