(in-package :chrest-avow)

; some tests 

(defmethod equal-node-p ((node-1 circuit-graph-node)
                         (node-2 circuit-graph-node))
  "Function used only in test-circuit-pattern"
  (and (equal (my-label node-1) (my-label node-2))
       (equal-lists-p (get-from node-1) (get-from node-2))
       (equal-lists-p (get-to node-1) (get-to node-2))))

(defun test-circuit-pattern ()
  (let (;(graph-a (new-circuit-graph-node 'A '(A B C) '(A B) '(A B C) '(A B)))
        ;(graph-b (new-circuit-graph-node 'B '(A B C) '(C D) () ()))
        ;(graph-c (new-circuit-graph-node 'C '(A) '(A B C D) () ()))
        ;(graph-d (new-circuit-graph-node 'E '(A B C) '(A B) '(E F G) '(E F)))
        (single-a (create-new-circuit-pattern
                   (list (create-new-resistor 'A 'POS 'GND '(1 1)))))
        (single-a1 (create-new-circuit-pattern
                    (list (create-new-resistor 'A 'POS 'ONE '(1 1)))))
        (single-b (create-new-circuit-pattern
                   (list (create-new-resistor 'B 'POS 'GND '(1 1)))))
        ;(single-b2 (create-new-circuit-pattern
        ;            (list (create-new-resistor 'B 'ONE 'GND '(1 1)))))
        ;(single-c (create-new-circuit-pattern
        ;            (list (create-new-resistor 'C 'ONE 'TWO '(2 1)))))
        ;(single-1 (create-new-circuit-pattern
        ;           (list (create-new-resistor 'A 'POS 'ONE '(1 1)))))
        ;(series-ab (create-new-circuit-pattern
        ;            (list (create-new-resistor 'A 'POS 'ONE '(1 1))
        ;                  (create-new-resistor 'B 'ONE 'GND '(1 2)))))
        (series-cd (create-new-circuit-pattern
                    (list (create-new-resistor 'C 'POS 'TWO '(3 1))
                          (create-new-resistor 'D 'TWO 'GND '(3 2)))))
        (one+one+two (create-new-circuit-pattern
                  (list (create-new-resistor 'A 'POS 'GND '(1 1))
                        (create-new-resistor 'B 'POS 'GND '(2 1))
                        (create-new-resistor 'C 'POS 'TWO '(3 1))
                        (create-new-resistor 'D 'TWO 'GND '(3 2)))))
        (parallel-ab (create-new-circuit-pattern
                      (list (create-new-resistor 'A 'POS 'GND '(1 1))
                            (create-new-resistor 'B 'POS 'GND '(2 1)))))
        ;(parallel-12 (create-new-circuit-pattern
        ;              (list (create-new-resistor 'B 'ONE 'GND '(1 2))
        ;                    (create-new-resistor 'C 'ONE 'GND '(2 2)))))
        ;(trio-a-bc (create-new-circuit-pattern
        ;            (list (create-new-resistor 'A 'POS 'ONE '(2 1))
        ;                  (create-new-resistor 'B 'ONE 'GND '(1 2))
        ;                  (create-new-resistor 'C 'ONE 'GND '(3 2)))))
        ;(trio-d-ef (create-new-circuit-pattern                    
        ;            (list (create-new-resistor 'D 'POS 'TWO '(2 1))
        ;                  (create-new-resistor 'E 'TWO 'GND '(1 2))
        ;                  (create-new-resistor 'F 'TWO 'GND '(3 2)))))
        ;(trio-ab-c (create-new-circuit-pattern
        ;            (list (create-new-resistor 'A 'POS 'ONE '(1 1))
        ;                  (create-new-resistor 'B 'POS 'ONE '(3 1))
        ;                  (create-new-resistor 'C 'ONE 'GND '(2 2)))))
        ;(trio-1-2 (create-new-circuit-pattern
        ;           (list (create-new-resistor 'A 'POS 'ONE '(1 1))
        ;                 (create-new-resistor 'B 'ONE 'GND '(1 3))
        ;                 (create-new-resistor 'C 'POS 'GND '(2 2)))))
        ;(trio-1-2a (create-new-circuit-pattern
        ;            (list (create-new-resistor 'D 'TOP 'MID '(3 3))
        ;                  (create-new-resistor 'E 'MID 'BOT '(3 5))
        ;                  (create-new-resistor 'F 'TOP 'BOT '(4 4)))))
        ;(trio-2-1 (create-new-circuit-pattern
        ;           (list (create-new-resistor 'A 'POS 'ONE '(2 1))
        ;                 (create-new-resistor 'B 'ONE 'GND '(2 3))
        ;                 (create-new-resistor 'C 'POS 'GND '(1 2)))))
        ;(circ-1 (create-new-circuit-pattern
        ;         (list
        ;          (create-new-resistor 'A 'POS 'ONE '(1 1))
        ;          (create-new-resistor 'B 'POS 'ONE '(2 1))
        ;          (create-new-resistor 'C 'POS 'TWO '(3 1))
        ;          (create-new-resistor 'D 'POS 'TWO '(4 1))
        ;          (create-new-resistor 'E 'ONE 'THR '(1 2))
        ;          (create-new-resistor 'F 'ONE 'THR '(2 2))
        ;          (create-new-resistor 'G 'TWO 'FOU '(3 2))
        ;          (create-new-resistor 'H 'TWO 'FOU '(4 2))
        ;          (create-new-resistor 'I 'THR 'GND '(1 3))
        ;          (create-new-resistor 'J 'THR 'GND '(2 3))
        ;          (create-new-resistor 'K 'FOU 'GND '(3 3))
        ;          (create-new-resistor 'L 'FOU 'GND '(4 3)))))
        ;(circ-2 (create-new-circuit-pattern
        ;         (list
        ;          (create-new-resistor 'A 'POS 'ONE '(1 1))
        ;          (create-new-resistor 'B 'POS 'ONE '(2 1))
        ;          (create-new-resistor 'C 'POS 'TWO '(3 1))
        ;          (create-new-resistor 'D 'POS 'TWO '(4 1))
        ;          (create-new-resistor 'E 'ONE 'THR '(1 2))
        ;          (create-new-resistor 'F 'ONE 'FOU '(2 2))
        ;          (create-new-resistor 'G 'TWO 'THR '(3 2))
        ;          (create-new-resistor 'H 'TWO 'FOU '(4 2))
        ;          (create-new-resistor 'I 'THR 'GND '(1 3))
        ;          (create-new-resistor 'J 'THR 'GND '(2 3))
        ;          (create-new-resistor 'K 'FOU 'GND '(3 3))
        ;          (create-new-resistor 'L 'FOU 'GND '(4 3)))))
        )

     ; run the generic set of tests 
     ; - note details of augmenting etc depend on circuit structure, so need further testing
     (test-sub-pattern (null-circuit-pattern) (null-circuit-pattern)
                       single-a single-a1 single-b
                       parallel-ab series-cd one+one+two)
     ))
;     ; - further checks for matching-patterns, based on graph isomorphisms
;     ; - for circuits, patterns match even if names are different
;     (check (matching-patterns-p single-a single-c)
;            "matching-patterns-p")
;     (check (matching-patterns-p single-a single-a)
;            "matching-patterns-p")
;     (check (matching-patterns-p single-a series-ab)
;            "matching-patterns-p")
;     (check (matching-patterns-p single-a trio-a-bc)
;            "matching-patterns-p")
;     (check (matching-patterns-p trio-a-bc trio-d-ef)
;            "matching-patterns-p")
;     (check (not (matching-patterns-p trio-a-bc trio-ab-c))
;            "matching-patterns-p")
;     (check (not (matching-patterns-p trio-a-bc single-a))
;            "matching-patterns-p")
;     (check (matching-patterns-p (null-circuit-pattern) single-a)
;            "matching-patterns-p")
;
;     ; - similarly, check for equal-patterns-p
;     (check (equal-patterns-p trio-a-bc trio-d-ef)
;            "equal-patterns-p")
;     (check (not (equal-patterns-p trio-a-bc trio-ab-c))
;            "equal-patterns-p")
;     (check (not (equal-patterns-p trio-a-bc single-a))
;            "equal-patterns-p")
;
;     ; - augment-patterns    
;     (check (equal-patterns-p (augment-pattern (get-my-match single-a series-ab)
;                                               single-b2)
;                              series-ab)
;            "augment-pattern 1")
;     (check (equal-patterns-p trio-a-bc
;                              (augment-pattern single-1 parallel-12))
;            "augment-pattern 2")
;     (check (equal-patterns-p (create-new-circuit-pattern 
;                                (list
;                                  (create-new-resistor 'A 'POS 'ONE '(1 1))
;                                  (create-new-resistor 'B 'POS 'ONE '(2 2))
;                                  (create-new-resistor 'C 'ONE 'GND '(2 3))))
;                              (augment-pattern 
;                                (get-my-match
;                                  (create-new-circuit-pattern 
;                                    (list
;                                      (create-new-resistor 'A 'POS 'ONE '(1 1))
;                                      (create-new-resistor 'B 'POS 'ONE '(2 2))))
;                                  (create-new-circuit-pattern 
;                                    (list
;                                      (create-new-resistor 'A 'POS 'ONE '(1 1))
;                                      (create-new-resistor 'B 'POS 'ONE '(2 2))
;                                      (create-new-resistor 'C 'ONE 'GND '(2 3)))))
;                                (create-new-circuit-pattern
;                                  (list
;                                    (create-new-resistor 'B 'ONE 'GND '(2 3))))))
;            "augment-pattern 3")
;     (let ((new-test (create-new-circuit-pattern (list (create-new-resistor 'E 'POS 'ONE '(1 1)))))
;           (target (create-new-circuit-pattern (list 
;                                                      (create-new-resistor 'A 'POS 'ONE '(2 1))
;                                                      (create-new-resistor 'B 'ONE 'GND '(1 2))
;                                                      (create-new-resistor 'C 'ONE 'GND '(2 2))
;                                                      (create-new-resistor 'D 'ONE 'GND '(3 2)))))
;           (old-cont (create-new-circuit-pattern (list
;                                                        (create-new-resistor 'B 'ONE 'GND '(2 1))
;                                                        (create-new-resistor 'C 'ONE 'GND '(2 2))
;                                                        (create-new-resistor 'D 'ONE 'GND '(3 2))))))
;       (check (equal-patterns-p target 
;                                (augment-pattern (get-my-match old-cont target) new-test))
;              "augment-pattern 4"))
;
;     ; - get-new-feature    
;     (check (equal-patterns-p (get-new-feature trio-a-bc single-a 'SINGLE)
;                              single-b)
;            "get-new-feature")
;
;     ; extract patterns
;     (check (equal-patterns-p (remove-pattern 
;                                (remove-pattern parallel-12 single-a) single-a)
;                              (new-null-for parallel-12))
;            "remove-pattern 1")
;
;     ; - get-my-match
;     (check (equal 
;              (descriptor (first (get-pattern (get-my-match single-a trio-a-bc))))
;              'A)
;            "get-my-match")
;     (check (and 
;              (equal 
;                (descriptor (first (get-pattern (get-my-match parallel-ab trio-a-bc))))
;                'B)
;              (equal 
;                (descriptor (second (get-pattern (get-my-match parallel-ab trio-a-bc))))
;                'C))
;            "get-my-match")
;     (check (and
;              (equal 
;                (descriptor (first (get-pattern (get-my-match single-a series-ab))))
;                'A)
;              (equal 
;                (from-node (first (get-pattern (get-my-match single-a series-ab))))
;                'POS)
;              (equal 
;                (to-node (first (get-pattern (get-my-match single-a series-ab))))
;                'ONE))
;            "get-my-match")
;     (check (equal-patterns-p (get-my-match single-a single-b) single-c)
;            "get-my-match")
;
;     ; - remove-pattern
;     (check (equal-patterns-p parallel-ab
;                              (remove-pattern trio-a-bc single-a))
;            "remove-pattern 2")
;
;     ; - distinguish-patterns
;     (check (equal-patterns-p (distinguish-patterns single-a trio-a-bc 'SINGLE)
;                              single-b)
;            "distinguish-patterns")
;     (check (equal-patterns-p (create-new-circuit-pattern
;                                (list
;                                  (create-new-resistor 'B 'POS 'ONE '(2 3))))
;                              (distinguish-patterns
;                                (create-new-circuit-pattern 
;                                  (list
;                                    (create-new-resistor 'A 'POS 'ONE '(1 1))
;                                    (create-new-resistor 'C 'ONE 'GND '(2 2))))
;                                (create-new-circuit-pattern 
;                                  (list
;                                    (create-new-resistor 'A 'POS 'ONE '(1 1))
;                                    (create-new-resistor 'B 'POS 'ONE '(2 2))
;                                    (create-new-resistor 'C 'ONE 'GND '(2 3))))
;                                'SINGLE))
;            "distinguish-patterns")
;
;     ; - combining patterns
;     (check (combined-equals-pattern-p (list  single-a single-b)
;                                       parallel-ab)
;            "combined-equals-pattern-p 1")
;     (check (not (combined-equals-pattern-p (list single-a)
;                                            parallel-ab))
;            "combined-equals-pattern-p 2")
;     (check (not (combined-equals-pattern-p () parallel-ab))
;            "combined-equals-pattern-p 3")
;
;     ; - collecting subpatterns    
;     (check (equal (length (get-all-with-same-size trio-a-bc trio-d-ef))
;                   1)
;            "get-all-with-same-size")
;     (check (equal 
;              (get-all-pairings '(A B C) '(1 2 3))
;              (list '((A 1) (B 2) (C 3)) 
;                    '((A 1) (B 3) (C 2)) 
;                    '((A 2) (B 1) (C 3)) 
;                    '((A 2) (B 3) (C 1)) 
;                    '((A 3) (B 1) (C 2)) 
;                    '((A 3) (B 2) (C 1))))
;            "get-all-pairings")
;     (check (equal (get-all-pairings (nodes-in (get-node-list single-a1))
;                                     (nodes-in (get-node-list single-b2)))
;                   '(((POS ONE) (ONE GND)) ((POS GND) (ONE ONE))))
;            "get-all-pairings")
;
;     ; - similarity function for graphs
;     (check (similar-node-p graph-a graph-b)
;            "Similar-node-p")
;     (check (not (similar-node-p graph-a graph-c))
;            "Similar-node-p")
;     (check (equal-node-p graph-a graph-a)
;            "Equal-node-p")
;     (check (not (equal-node-p graph-a graph-c))
;            "Equal-node-p")
;     (check (similar-node-list-p () ())
;            "Similar-node-list-p 1")
;     (check (similar-node-list-p (list graph-a graph-b) (list graph-b graph-b))
;            "Similar-node-list-p 2")
;     (check (not (similar-node-list-p (list graph-a) (list graph-a graph-b)))
;            "Similar-node-list-p 3")
;     (check (not (similar-node-list-p (list graph-a) (list graph-c)))
;            "Similar-node-list-p 4")
;
;     ; - extracting the node list from a graph or circuit
;     (check (equal-lists-p (nodes-loads-in (list graph-a)) (list 'A 'B 'C))
;            "nodes-loads-in")
;     (check (equal-lists-p (nodes-in (list graph-a)) (list 'A))
;            "nodes-in")
;     (check (equal-lists-p (nodes-in (get-node-list trio-ab-c)) (list 'POS 'GND 'ONE))
;            "nodes-in")
;     (let ((trio-list (get-node-list trio-ab-c)))
;       (check (and (equal (my-label (first trio-list)) 'POS)
;                   (null (get-from (first trio-list)))
;                   (equal (get-num-to (first trio-list)) 2)
;                   (equal (my-label (second trio-list)) 'GND)
;                   (equal (get-num-from (second trio-list)) 1)
;                   (null (get-to (second trio-list)))
;                   (equal (my-label (third trio-list)) 'ONE)
;                   (equal (get-num-from (third trio-list)) 2)
;                   (equal (get-num-to (third trio-list)) 1))
;              "get-node-list"))
;     (check (equal-node-list-p 
;              (get-node-list single-b2)
;              (apply-pairings
;                (get-node-list single-a1)
;                (first (get-all-pairings (nodes-in (get-node-list single-a1))
;                                         (nodes-in (get-node-list single-b2))))))
;            "equal-node-list-p")
;     (check (equal-node-list-p (get-node-list single-c)
;                               (apply-pairings (get-node-list single-a)
;                                               '((POS ONE) (A C) (GND TWO))))
;            "Equal-node-list-p / apply-pairings")
;
;     (check (node-not-in 'ONE ())
;            "Node-not-in")
;     (check (node-not-in 'ONE (list graph-a graph-b))
;            "Node-not-in")
;     (check (not (node-not-in 'A (list graph-a graph-b graph-c)))
;            "Node-not-in")
;     (check (string= 
;              (my-label (get-node-in 'A (list graph-a graph-b graph-c)))
;              'A)
;            "get-node-in")
;     (check (null (get-node-in 'ONE (list graph-a graph-b graph-c)))
;            "get-node-in")
;     (let ((test-node-list ()))
;       (setf test-node-list (add-new-from test-node-list 
;                                          (create-new-resistor 'A 'POS 'ONE '(1 1))
;                                          'POS))
;       (check (and (equal (length test-node-list) 1)
;                   (equal (my-label (first test-node-list)) 'POS)
;                   (equal (get-num-from (first test-node-list)) 0)
;                   (equal (get-num-to (first test-node-list)) 1)
;                   (equal (first (get-to (first test-node-list))) 'A))
;              "add-new-from")
;       (setf test-node-list (add-new-to test-node-list 
;                                        (create-new-resistor 'B 'POS 'ONE '(2 1))
;                                        'POS))
;       (check (and (equal (length test-node-list) 1)
;                   (equal (my-label (first test-node-list)) 'POS)
;                   (equal (get-num-from (first test-node-list)) 1)
;                   (equal (first (get-from (first test-node-list))) 'B)
;                   (equal (get-num-to (first test-node-list)) 1)
;                   (equal (first (get-to (first test-node-list))) 'A))
;              "add-new-to")
;       (setf test-node-list (add-new-to test-node-list 
;                                        (create-new-resistor 'C 'POS 'TWO '(3 1))
;                                        'ONE))
;       (check (and (equal (length test-node-list) 2)
;                   (equal (first 
;                            (get-from (get-node-in 'ONE test-node-list)))
;                          'C))
;              "add-new-to")
;       )
;
;     ; full isomorphism checks
;     (check (isomorphicp single-a single-a)
;            "Isomorphicp")
;     (check (isomorphicp single-a single-b)
;            "Isomorphicp")
;     (check (isomorphicp single-a1 single-b2)
;            "Isomorphicp")
;     (check (isomorphicp single-a single-c)
;            "Isomorphicp")
;     (check (isomorphicp single-b single-c)
;            "Isomorphicp")
;     (check (isomorphicp trio-a-bc trio-a-bc)
;            "Isomorphicp")
;     (check (isomorphicp trio-a-bc trio-d-ef)
;            "Isomorphicp")
;     (check (not-isomorphicp single-a series-ab)
;            "not-Isomorphicp")
;     (check (not-isomorphicp series-ab single-a)
;            "not-Isomorphicp")
;     (check (not-isomorphicp trio-a-bc trio-ab-c)
;            "not-Isomorphicp")
;     (check (not-isomorphicp trio-ab-c trio-d-ef)
;            "not-Isomorphicp")
;     (check (not-isomorphicp circ-1 circ-2)
;            "not-Isomorphicp")
;     (let ((old-cont (create-new-circuit-pattern (list
;                                                        (create-new-resistor 'B 'POS 'ONE '(2 1))
;                                                        (create-new-resistor 'C 'ONE 'GND '(2 2))
;                                                        (create-new-resistor 'D 'ONE 'GND '(3 2)))))
;           (test-iso (create-new-circuit-pattern (list
;                                                        (create-new-resistor 'B 'ONE 'GND '(2 2))
;                                                        (create-new-resistor 'C 'ONE 'GND '(3 2))
;                                                        (create-new-resistor 'A 'POS 'ONE '(2 1))))))
;       (check (isomorphicp old-cont test-iso)
;              "isomorphicp")
;       (check (equal-node-list-p (get-node-list old-cont) (get-node-list test-iso))
;              "equal-node-list-p")
;       (check (equal-node-list-p (get-node-list old-cont)
;                                 (list
;                                   (new-circuit-graph-node 'POS '() '(B) () '(ONE))
;                                   (new-circuit-graph-node 'ONE '(B) '(C D) '(POS) '(GND))
;                                   (new-circuit-graph-node 'GND '(C D) '() '(ONE) ())))
;              "get-node-list"))
;
;     ; - check filter on get-all-of-size-n
;     (check (equal 3 (length (get-all-of-size-n trio-ab-c 2)))
;            "get-all-of-size-n 1")
;     (check (equal 1 (length (get-all-of-size-n trio-ab-c 2 trio-ab-c)))
;            "get-all-of-size-n 2")
;     (check (equal-patterns-p parallel-ab (first (get-all-of-size-n trio-ab-c 2)))
;            "get-all-of-size-n 3")
;     (check (not (crossed-node-p trio-ab-c parallel-ab))
;            "crossed-node-p")
;     (check (crossed-node-p trio-ab-c series-ab)
;            "crossed-node-p")
;
;     ; - check the node-replacement functions, which act on lists
;     (check (equal (replace-item 'A '((A 1) (B 2))) 1)
;            "replace-item")
;     (check (equal (replace-list '(A B C) '((D 2) (A 1) (B 2) (E 3) (C 4)))
;                   (list 1 2 4))
;            "replace-item")
;     (check (equal-node-p (first (apply-pairings (list graph-a) '((A E) (B F) (C G))))
;                          graph-d)
;            "apply-pairings")
;
;     ; check size-of and null relations
;     (check (equal (size-of trio-a-bc) 3)
;            "size-of")
;
;     (check (and (null-p (new-end-for single-c))
;                 (has-end-p (new-end-for single-c)))
;            "new-end-for")
;
;     ; check that matching-subcircuit rejects left-to-right symmetries
;     (check (and (isomorphicp trio-1-2 trio-2-1)
;                 (not (identical-layout-p trio-1-2 trio-2-1)))
;            "identical-layout-p")
;
;     (check (identical-layout-p trio-1-2 trio-1-2a)
;            "identical-layout-p")  ; check this works with name changing
;
;     ; test visible-feature
;     (check (visible-feature-p (create-new-resistor 'C 'ONE 'TWO '(2 1)) #@(80 40) 10)
;            "visible-feature-p")
;     (check (visible-feature-p (create-new-resistor 'C 'ONE 'TWO '(2 1)) #@(75 35) 10)
;            "visible-feature-p")
;     (check (not (visible-feature-p (create-new-resistor 'C 'ONE 'TWO '(2 1)) #@(70 35) 10))
;            "visible-feature-p")
;
;     ; test extract-features
;     (check (equal-patterns-p 
;              (extract-features 
;                (create-new-circuit-pattern
;                  (list (create-new-resistor 'C 'ONE 'TWO '(2 1))))
;                #@(75 35) 10)
;              (create-new-circuit-pattern
;                (list (create-new-resistor 'C 'ONE 'TWO '(2 1)))))
;            "extract-features")
;     (check (null-p 
;              (extract-features 
;                (create-new-circuit-pattern
;                  (list (create-new-resistor 'C 'ONE 'TWO '(2 1))))
;                #@(70 35) 10))
;            "extract-features")
;     (check (equal-patterns-p 
;              (extract-features trio-ab-c #@(80 20) 50)
;              parallel-ab)
;            "extract-features")
;
;     ;; test save/read operations
;     (check (equal-patterns-p trio-ab-c (read-circuit-pattern (rest (write-object trio-ab-c))))
;            "Read/write operations"))
;
;  ;; test circuit relabellings
;  (check (equal (list '(B A) '(C B))
;                (get-circuit-relabelling 
;                  (create-new-circuit-pattern 
;                    (list
;                      (create-new-resistor 'A 'POS 'ONE '(1 1))
;                      (create-new-resistor 'B 'POS 'ONE '(2 1))))
;                  (create-new-circuit-pattern
;                    (list 
;                      (create-new-resistor 'B 'POS 'GND '(1 1))
;                      (create-new-resistor 'C 'POS 'GND '(2 1))))))
;         "Circuit relabelling")
;
;  )
;
;; (test-circuit-pattern)
