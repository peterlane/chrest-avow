(in-package :chrest-avow)

;; generic test routine for pattern classes
;; all subclasses of pattern should be able to pass this suite
(defun test-sub-pattern (a-null b-null a-single a2-single b-single ab-double cd-double abcd-quad)
  ; check matching patterns 
  ; - nulls match
  (check (matching-patterns-p a-null b-null)
         "matching-patterns-p")
  ; - something does not match null
  (check (not (matching-patterns-p a-single a-null))
         "matching-patterns-p")
  ; - null matches something
  (check (matching-patterns-p a-null a-single)
         "matching-patterns-p")
  ; - larger does not match smaller
  (check (not (matching-patterns-p ab-double b-single))
         "matching-patterns-p")
  ; - same patterns match
  (check (matching-patterns-p a2-single a-single)
         "matching-patterns-p")

  ; - smaller matches larger
  (check (matching-patterns-p a-single ab-double)
         "matching-patterns-p")

  ; check equal patterns
  ; - nulls are equal
  (check (equal-patterns-p a-null b-null)
         "equal-patterns-p")
  ; - same patterns are equal
  (check (equal-patterns-p a-single a2-single)
         "equal-patterns-p")
  ; - different patterns are not equal
  (check (not (equal-patterns-p ab-double a-single))
         "equal-patterns-p")

  ; can we distinguish patterns?
  ; - distinguish longer from smaller pattern should suggest extra feature
  (check (not (equal-patterns-p a-single ab-double))
         "not equal-patterns-p 1")
  ; - check that pat-b is suggested
  (check (equal-patterns-p (distinguish-patterns a-single ab-double 'SINGLE) b-single)
         "distinguish-patterns 1")
  ; - distinguish smaller from longer pattern should suggest '.'
  (check (not (equal-patterns-p ab-double a-single))
         "not equal-patterns-p 2") 
  ; - check that '.' is suggested 
  (check (and (has-end-p (distinguish-patterns ab-double a-single 'SINGLE))
              (null-p (distinguish-patterns ab-double a-single 'SINGLE)))
         "distinguish-patterns 2")
  ; - equal patterns cannot be distinguished
  (check (equal-patterns-p a-single a2-single)
         "equal-patterns-p 3")
  (check (equal-patterns-p (distinguish-patterns a-single a2-single 'SINGLE)
                                (null-pattern))
         "distinguish-patterns 3")

  ; augmenting a pattern
  (check (equal-patterns-p (augment-pattern a-single b-single) ab-double)
         "augment-pattern")

  ; get a new feature
  ; - should produce extra feature
  (check (equal-patterns-p (get-new-feature ab-double a-single 'SINGLE) b-single)
         "get-new-feature")

  ; - cannot get a new feature
  (check (equal-patterns-p (get-new-feature a-single ab-double 'SINGLE) a-null)
         "get-new-feature")

  ; extending a pattern
  (check (equal-patterns-p (learn-more-of-pattern a-single ab-double 'SINGLE) ab-double)
         "learn-more-of-pattern")

  ; removing a pattern from another
  (check (equal-patterns-p (remove-pattern abcd-quad ab-double) cd-double)
         "remove-pattern a")
  (check (equal-patterns-p (remove-pattern ab-double a-single) b-single)
         "remove-pattern b")

  ; extract one pattern from another
  (check (equal-patterns-p (remove-pattern ab-double a-single)
                                b-single)
         "remove-pattern c")
  (check (equal-patterns-p (remove-pattern 
                                  (remove-pattern ab-double a-single) b-single)
                                (new-null-for ab-double))
         "remove-pattern d")

  ; does a combined list of patterns equal proposed whole
  (check (combined-equals-pattern-p (list ab-double cd-double) abcd-quad)
         "combined-equals-pattern-p A")
  (check (not (combined-equals-pattern-p (list ab-double) abcd-quad))
         "combined-equals-pattern-p B")
  (check (not (combined-equals-pattern-p () abcd-quad))
         "combined-equals-pattern-p C")

  ; check 'end' is handled correctly
  (check (not (has-end-p ab-double))
         "has-end-p")
  (check (has-end-p (add-end ab-double)) 
         "add-end")

  ; tests for null patterns
  (check (non-null-p ab-double)
         "non-null-p")
  (check (not (non-null-p a-null))
         "non-null-p")
  )

