(in-package :chrest-avow)

;; class of patterns

;; Peter Lane, 29th January 2001

;; this class provides a generic holder for patterns in the CHREST 
;; models.  Follows STRATEGY pattern, with each subclass of pattern 
;; providing its own methods to slot in the TEMPLATE of a CHREST 
;; or other learning algorithm

;; patterns have a slot for the pattern's features, 
;; and a flag for indicating if the pattern is a complete pattern.
;; In CHREST, as with general trie representations, there is a potential 
;; problem in distinguishing a subpattern from a larger one.  e.g. '(A B C) from '(A B C D).
;; An END-MARKER is used for this purpose, i.e. '(A B C END) '(A B C D END)
;; Patterns have an END-MARKER if the calling system determines that the features listed 
;; are a complete set.

;; By default, the pattern classes assumes a set-structure for the features in its 
;; patterns.  Different class types can be used by over-riding the appropriate 
;; methods, as indicated below.

;; ------ Class method summary ------ 

;; Externally called methods:

;; ****** TO FILL OUT ******

;; Over-rideable methods:

;; for creation:        create-new-pattern-for, new-null-for
;; in comparison:       get-my-match, get-strict-match, size-of, matching-patterns-p, 
;;                      equal-features-p
;; for learning:        get-all-object
;; for identification:  visible-feature-p
;; for saving/reading:  write-object, read-pattern

;; new method types:    null-XXX-pattern, create-new-XXX-pattern, create-new-XXX-feature

;; --------------- class definitions and methods -------------------

;; patterns are made up from lists of features, each feature has a visual location

(defclass FEATURE () 
  ((location :accessor location :initarg :location :initform (make-point 0 0))
   (feature :accessor feature :initarg :feature)))

(defclass PATTERN ()
  ((my-pattern :accessor get-pattern :initarg :pattern :type list)
   (has-end-p :accessor has-end-p :initarg :has-end-p :initform ())))

;; to-string is the specific display routine for patterns only
(defmethod to-string ((a-pattern pattern))
  "Convert pattern into a descriptive textual form"
  (format nil (if (has-end-p a-pattern) "~a." "~a") 
          (mapcar #'to-string (get-pattern a-pattern))))

(defmethod to-string ((a-pattern t))
  (when (null-p a-pattern) (format nil "(---)")))

(defmethod to-string ((a-feature feature))
  "Convert a feature into text -- this should be over-ridden if more information output needed"
  (format nil "~a" (feature a-feature)))


;; generic routines valid for all subclasses of pattern
(defmethod get-size ((a-pat pattern))
  "Use get-width and get-height to return the pattern's size"
  (make-point (get-width a-pat) (get-height a-pat)))

(defmethod get-single-pattern-field-size ((a-pattern pattern))
  (add-points 
   (make-point (get-width a-pattern) (get-height a-pattern))
   (make-point 27 17)))

;; constructor routines for pattern class
;; all subclasses need to over-ride these methods
;;     -- null-pattern  
;;     -- new-null-for

(defun null-pattern ()
  "Create a new null pattern"
  (make-instance 'pattern :pattern ()))

; create a null pattern with end set
(defmethod new-null-for ((a-pat pattern))
  "New-null-for uses method dispatching to return a null of appropriate pattern type"
  (declare (ignore a-pat))
  (null-pattern))

;; generic routines valid for all subclasses of pattern
(defmethod new-end-for ((a-pattern pattern))
  "New-end-for uses method dispatching to return a null with end set of appropriate pattern type"
  (add-end (new-null-for a-pattern)))

(defmethod null-p ((a-pattern pattern))
  "Check whether the given pattern has any features"
  (null (get-pattern a-pattern)))

(defmethod null-p ((a-pattern t)) ())

(defmethod non-null-p ((a-pattern pattern))
  (not (null-p a-pattern)))


(defmethod add-end ((a-pattern pattern))
  "Include the end-marker on a pattern"
  (setf (has-end-p a-pattern) T)
  a-pattern)

(defmethod create-new-pattern (&optional (new-pattern ()))
  "Create a new pattern instance -- a problem somewhere, as avow-features should be forced into avow-patterns"
  (if new-pattern
    (cond ((equal (type-of (first (alexandria:ensure-list new-pattern))) 'avow-feature)
           (make-instance 'avow-pattern :pattern (alexandria:ensure-list new-pattern)))
          (T
            (make-instance 'pattern :pattern (alexandria:ensure-list new-pattern))))
    (make-instance 'pattern :pattern ())))


(defmethod create-new-pattern-for ((a-pat pattern) &optional (new-pattern ()))
  "For non-specific patterns, just create instance with feature-list in the pattern slot"
  (declare (ignore a-pat))
  (if new-pattern
    (create-new-pattern-for (first (alexandria:ensure-list new-pattern)) new-pattern)
    (make-instance 'pattern :pattern ())))

(defmethod create-new-pattern-for ((a-feature feature) &optional (new-pattern ()))
  "use type of feature to determine new pattern type"
  (declare (ignore a-feature))
  (create-new-pattern new-pattern))

(defmethod create-new-feature (new-feature)
  "Create a feature instance for given datum"
  (make-instance 'feature :feature new-feature))

(defmethod equal-features-p ((feature-1 feature) (feature-2 feature))
  "Simple equality check for two features"
  (equal (feature feature-1) (feature feature-2)))

(defmethod get-my-match ((target-pat pattern) (large-pat pattern)
                                              &optional context-pat)
  "get-my-match returns those elements of large-pat which are also in target-pat - default ignores the context-pat"
  (declare (ignore context-pat))
  (create-new-pattern 
    (intersection (get-pattern target-pat) (get-pattern large-pat) :test #'equal-features-p)))

(defmethod get-strict-match ((target-pat pattern) (large-pat pattern)
                                                  &optional context-pat)
  "Strict match is the same as a normal match for basic patterns -- called by decompose-pattern in CHREST module"
  (declare (ignore context-pat))
  (get-my-match target-pat large-pat))



;; comparing pattern sizes
;; subclasses should over-ride
;;     -- size-of

(defmethod size-of ((a-pattern pattern))
  "Return the number of features as basic size of a pattern"
  (length (get-pattern a-pattern)))

;; generic functions valid for all subclasses of pattern
(defmethod is-larger-p ((pat1 pattern) (pat2 pattern))
  (> (size-of pat1) (size-of pat2)))

(defmethod not-larger-p ((pat1 pattern) (pat2 pattern))
  (not (is-larger-p pat1 pat2)))

(defmethod equal-sizes-p ((pat1 pattern) (pat2 pattern))
  (equal (size-of pat1) (size-of pat2)))


;; comparing patterns
;; subclasses provide the following functions
;;     -- matching-patterns-p     checks if pat-1 is a subpattern of pat-2
;;                                this may need specialising
;;     -- equal-features-p        checks if two features are equal
;; 

(defmethod matching-patterns-p ((pat-1 pattern) (pat-2 pattern))
  "In general case, do a feature by feature match, i.e. exact match of patterns -- subclasses can over-ride this method"
  (feature-based-match-of-patterns-p pat-1 pat-2))

;; generic methods valid for all subclasses of pattern
(defmethod equal-patterns-p ((a-pat-1 pattern) (a-pat-2 pattern))
  "Compares patterns using matching-patterns-p, which can be over-ridden in subclass for generic comparison types, e.g. graphs"
  (compare-patterns a-pat-1 a-pat-2 #'matching-patterns-p))

(defmethod equal-patterns-exactly-p ((a-pat-1 pattern) (a-pat-2 pattern))
  "Enforces comparison of patterns based on features"
  (compare-patterns a-pat-1 a-pat-2 #'feature-based-match-of-patterns-p))

(defmethod overlapping-patterns-p ((pat-1 pattern) (pat-2 pattern))
  "Overlapping-patterns-p returns T if the patterns agree on at least one feature"
  (some (check-feature-in-pattern pat-2) (get-pattern pat-1)))


;; generic helper routines for comparison operations
(defmethod compare-patterns ((pattern-1 pattern) (pattern-2 pattern) comparison-fn)
  "General comparison (for equality) of patterns based on given comparison-fn"
  (or (and (null-p pattern-1) (null-p pattern-2))
      (and (apply comparison-fn pattern-1 (list pattern-2))
           (apply comparison-fn pattern-2 (list pattern-1)))))

(defmethod feature-in-pattern-p ((test-feature feature) (a-pattern pattern))
  "general method to check if a feature is in a given pattern is to use equal-features-p"
  (member test-feature (get-pattern a-pattern) :test #'equal-features-p))

(defmethod feature-not-in-pattern-p ((a-feat feature) (a-pat pattern))
  (not (feature-in-pattern-p a-feat a-pat)))

(defmethod check-feature-in-pattern ((a-pattern pattern))
  "Returns a test for whether a feature is in the supplied pattern"
  (lambda (test-feature) (feature-in-pattern-p test-feature a-pattern)))


(defmethod feature-based-match-of-patterns-p ((pat-1 pattern) (pat-2 pattern))
  "pat-1 matches pat-2 if every feature in pat-1 is within pat-2"
  (if (equal (type-of pat-1) (type-of pat-2))   
    (or (null-p pat-1)
        (every (check-feature-in-pattern pat-2) (get-pattern pat-1)))
    ()))

(defmethod exact-matching-patterns-p ((pat-1 pattern) (pat-2 pattern))
  "Sugared call to feature-based-match"
  (feature-based-match-of-patterns-p pat-1 pat-2))




;; following combination, augmenting and distinguishing routines are designed to 
;; support CHREST+'s learning and pattern recognition operations

;; subclasses of pattern must provide:
;;    -- get-my-match   which returns the overlapping portion of the patterns, 
;;                      and renames its elements

(defmethod combined-equals-pattern-p ((pattern-list list) (a-pattern pattern))
  "checks if the combined pattern-list equals the given pattern"
  (equal-patterns-p (combine-patterns pattern-list) a-pattern))

(defmethod remove-pattern ((base-pattern pattern) (target-pattern pattern))
  "Extract the target-pattern from the base-pattern: first matches target-pattern to base-pattern"
  (create-new-pattern-for 
    target-pattern (remove-if (check-feature-in-pattern (get-my-match target-pattern base-pattern)) 
                              (get-pattern base-pattern))))

(defmethod augment-pattern ((base-pattern pattern) (additional-pattern pattern))
  "Combine the features from the two patterns: add end if required"
  (let ((new-pattern 
          (cond ((null-p base-pattern) additional-pattern)
                ((null-p additional-pattern) base-pattern)
                (T 
                  (create-new-pattern-for 
                    base-pattern
                    (union (get-pattern base-pattern) (get-pattern additional-pattern)
                           :test #'equal-features-p))))))
    (when (or (has-end-p base-pattern) (has-end-p additional-pattern))
      (add-end new-pattern))
    new-pattern))

;; generic learning routines
;; CHREST+ will call distinguish-patterns during discrimination
;;         and       learn-more-of-pattern during familiarisation
;; subclasses of pattern may over-ride
;;     -- get-all-object

(defmethod distinguish-patterns ((base-pattern pattern) (target-pattern pattern) learning-rate)
  "Extract part of target-pattern which distinguishes it from base-pattern"
  (cond ((and (null-p base-pattern) (null-p target-pattern))
         (null-pattern))
        ((matching-patterns-p target-pattern base-pattern) (new-end-for target-pattern))
        (T (let ((distinguish-pat (get-new-feature target-pattern base-pattern learning-rate)))
             (if (null-p distinguish-pat)
               (add-end distinguish-pat) ; must be a subsequence
               distinguish-pat)))))

(defmethod learn-more-of-pattern ((base-pattern pattern) (target-pattern pattern) learning-rate)
  "Use get-new-feature and learn-rate to add a specific amount of target-pat to base-pat; ses get-my-match to ensure agreement with base-pattern"
  (augment-pattern (get-my-match base-pattern target-pattern)
                   (get-new-feature target-pattern base-pattern learning-rate)))


(defmethod get-new-feature ((target-pattern pattern) (base-pattern pattern) learning-rate)
  "Retrive some subset of target-pattern not present in base-pattern, using learning rate"
  (flet ((force-type (query-pattern target-pattern)
                     "force-type is a nuisance, but needed to ensure typing of results"
                     (if (null-p query-pattern)
                       (new-null-for target-pattern)
                       query-pattern)))

    (let* ((matched-target (force-type (get-my-match base-pattern target-pattern) target-pattern))
           (remaining-pattern (force-type (remove-pattern target-pattern matched-target) base-pattern)))
      (if (null-p remaining-pattern) 
        (new-null-for target-pattern)
        (create-new-pattern-for 
          remaining-pattern
          (get-extra-pattern remaining-pattern matched-target learning-rate))))))

; get-extra-pattern based on learning rate and remaining pattern in image
; - single learning, extracts just the next feature/relation from the remaining pattern
;   21/6/01 - random-element from feature list, not the next
; - objects learning, extracts all features/relations relating to an object in the remaining pattern
; - whole learning, extracts all the remaining features/relations
(defmethod get-extra-pattern ((remaining-pat pattern) (known-pat pattern) learning-rate)
  (cond ((learn-rate-single-p learning-rate) (first (get-pattern remaining-pat)))
        ((learn-rate-objects-p learning-rate) (get-all-object remaining-pat))
        ((learn-rate-whole-p learning-rate) (get-pattern remaining-pat))
        (T (first (get-pattern remaining-pat)))))

; get-all-object selects an object in the target-pattern 
; and returns all features/relations relating to that object 
; - default, just takes next feature/relation
(defmethod get-all-object ((remaining-pattern pattern))
  "Select an object in remaining-pattern and return all features/relations relating to it -- by default, just return next feature"
  (first (get-pattern remaining-pattern)))


(defmethod combine-patterns ((pattern-list list))
  "Collapse patterns in pattern-list into a single pattern - return null-pattern if list is empty"
  (if pattern-list
    (reduce #'augment-pattern (rest pattern-list) :initial-value (first pattern-list))
    (null-pattern)))


;; pattern features have a location, and sub-patterns may be extract be providing a focal 
;; point and field-of-view
;; subclasses of pattern over-ride:
;;    -- visible-feature-p    to determine if a given feature is visible

(defmethod extract-features ((a-pattern pattern) a-point radius)
  "Creates a new pattern of same type as original containing those features in original which are within radius of a-point"
  (create-new-pattern-for 
    a-pattern (remove-if-not #'(lambda (a-feature) (visible-feature-p a-feature a-point radius))
                             (get-pattern a-pattern))))


;; methods for reading/writing objects 
(defmethod write-object ((a-pattern pattern))
  "Convert a pattern into a writable list"
  (list 'pattern
        (mapcar #'write-object (get-pattern a-pattern))
        (has-end-p a-pattern)))

(defmethod write-object ((a-feature feature))
  "Convert a feature into a writable list"
  (list (point-h (location a-feature))
        (point-v (location a-feature))
        (feature a-feature)))

(defmethod read-pattern ((a-list list))
  "Convert list back into a pattern"
  (assert (= 2 (length a-list)))
  (make-instance 'pattern 
                 :pattern (mapcar #'read-feature (first a-list))
                 :has-end-p (second a-list)))

(defmethod read-feature ((a-list list))
  "Convert list back into a feature"
  (assert (= 3 (length a-list)))
  (make-instance 'feature
                 :location (make-point (first a-list) (second a-list))
                 :feature (third a-list)))




