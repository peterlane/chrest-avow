(in-package :chrest-avow)

;;; stm-queue

;;; Peter Lane, 19th March, 2001

;;; a class for the subject's STM

;; stm-list contains a list of patterns and their associated nodes in the LTM
;; stm-size specifies how large a list of patterns will be retained

(defclass STM-QUEUE ()
  ((stm-size :accessor stm-size :initarg :stm-size)
   (stm-list :accessor stm-list :initarg :stm-list :initform () :type list)
   (my-subject :accessor my-subject :initarg :my-subject :type chrest-subject)))

(defmethod new-stm-queue ((my-subject chrest-subject) &optional (size 4))
  (make-instance 'stm-queue :my-subject my-subject :stm-size size))

(defmethod clear-stm ((stm stm-queue))
  (setf (stm-list stm) ()))

(defmethod get-stm-patterns ((stm stm-queue))
  (assert (listp (stm-list stm)))
  (mapcar #'stm-item-pattern (stm-list stm)))

; extracts those entries of stm-list of same type as the first (hypothesis) element
; then combines the patterns into a single pattern
(defmethod get-stm-contents ((stm stm-queue))
   (flet ((same-as-hypothesis-type-p (a-pat) 
             (typep a-pat (type-of (first (get-stm-patterns stm))))))
      (combine-patterns 
       (remove-if-not #'same-as-hypothesis-type-p (get-stm-patterns stm)))))

;; a structure for stm items
(defstruct STM-ITEM pattern node)

; need to add explicit hypothesis slot
(defmethod add-to-stm ((stm stm-queue) an-item)
   (assert (stm-item-p an-item))
   (setf (stm-list stm) (add-to-stm-list an-item (stm-list stm)))
   (when (> (length (stm-list stm)) (stm-size stm))
      (setf (stm-list stm) (subseq (stm-list stm) 0 (stm-size stm))))
   (when (learn-mode-p (my-subject stm))
      (learn-from-STM stm)))

(defmethod add-to-stm-list (an-item stm-list)
  "Add to head of list if new, else bring to front of list if old"
  (assert (stm-item-p an-item))
  (flet ((equal-stm-patterns-p (x y)
           "Tests whether two stm-patterns, i.e. pattern-node pairs, have equal patterns"
           (assert (and (stm-item-p x) (stm-item-p y)))
           (equal-patterns-exactly-p (stm-item-pattern x) 
                                     (stm-item-pattern y))))
    (let ((my-posn 
            (position an-item stm-list :test #'equal-stm-patterns-p)))
       (if my-posn
          (append (list an-item) 
                   (subseq stm-list 0 my-posn) 
                   (subseq stm-list (+ 1 my-posn)))
          (push an-item stm-list)))))

(defmethod learn-from-stm ((stm stm-queue))
  "Learn the current contents of STM"
  (learn (if (equal-patterns-exactly-p (get-stm-contents stm) 
                                       (viewed-object (my-subject stm)))
           (add-end (get-stm-contents stm))
           (get-stm-contents stm))
         (get-LTM (my-subject stm))
         (my-subject stm))
  (notify-observers-of-ltm-change (my-subject stm)))


