(in-package :chrest-avow)

;;;; a class for fixations

;;;; Peter Lane

;;; eye fixations specialise the general fixation class
;;; use method dispatch to take actions based on fixation type

; a fixation defines the point it was made at
; subclasses can introduce further information about the pattern causing it to be made
(defclass FIXATION () ((point :accessor get-fixation-point :initarg :point :initform (make-point 0 0))))

(defclass DEFAULT-FIXATION (fixation) 
   ())
(defclass IMAGE-FIXATION (fixation) 
   ((current-image :accessor current-image :initarg :current-image :type pattern)))
(defclass TEST-FIXATION (fixation) 
   ((this-test :accessor this-test :initarg :this-test :type pattern)))
(defclass NOVEL-FIXATION (fixation) 
   ())

(defmethod create-default-fixation (a-point) 
   (make-instance 'default-fixation :point a-point))
(defmethod create-image-fixation (a-point &optional (current-image (null-pattern))) 
  (make-instance 'image-fixation :point a-point :current-image current-image))
(defmethod create-test-fixation (a-point &optional (this-test (null-pattern))) 
  (make-instance 'test-fixation :point a-point :this-test this-test))
(defmethod create-novel-fixation (a-point) 
   (make-instance 'novel-fixation :point a-point))

; check if fixations are of the same type
(defmethod same-fixation-type-p ((fixation-1 fixation) (fixation-2 fixation))
  (typep fixation-1 (type-of fixation-2)))

; tests for fixation type
(defmethod is-test-fixation-p ((a-fixation test-fixation)) T)
(defmethod is-test-fixation-p ((a-fixation t)) ())

; names for displaying each fixation type
(defmethod get-fixation-name ((a-fixation default-fixation)) "Default")
(defmethod get-fixation-name ((a-fixation image-fixation)) "Image")
(defmethod get-fixation-name ((a-fixation test-fixation)) "Test")
(defmethod get-fixation-name ((a-fixation novel-fixation)) "Novel")

; writable list for fixations 
(defmethod write-object ((a-fixation default-fixation)) 
  (list 'D (point-h (get-fixation-point a-fixation)) (point-v (get-fixation-point a-fixation))))
(defmethod write-object ((a-fixation image-fixation)) 
  (list 'I (point-h (get-fixation-point a-fixation)) (point-v (get-fixation-point a-fixation))
         (write-object (current-image a-fixation))))
(defmethod write-object ((a-fixation test-fixation)) 
  (list 'T (point-h (get-fixation-point a-fixation)) (point-v (get-fixation-point a-fixation))
         (write-object (this-test a-fixation))))
(defmethod write-object ((a-fixation novel-fixation)) 
  (list 'N (point-h (get-fixation-point a-fixation)) (point-v (get-fixation-point a-fixation))))

; read back the fixation list
(defmethod read-fixation ((a-list list))
  (assert (or (= 3 (length a-list)) (= 4 (length a-list))))
  (let ((fixation-point (make-point (second a-list) (third a-list))))
    (cond ((equal 'D (first a-list))
            (create-default-fixation fixation-point))
           ((equal 'I (first a-list))
            (create-image-fixation fixation-point (read-a-pattern (fourth a-list))))
           ((equal 'T (first a-list))
            (create-test-fixation fixation-point (read-a-pattern (fourth a-list))))
           ((equal 'N (first a-list))
            (create-novel-fixation fixation-point))
           (T
            (error "Unrecognised fixation type")))))
