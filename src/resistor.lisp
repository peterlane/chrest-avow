(in-package :chrest-avow)

;; this file contains the resistor class
;; it is designed as a subclass of circuit-feature

(defclass RESISTOR (circuit-feature)
  ((descriptor :accessor descriptor :initarg descriptor)
   (from-node :accessor from-node :initarg from-node)
   (to-node :accessor to-node :initarg to-node)))

(defun create-new-resistor (&optional (descript "nil") (from ()) (to ()) 
                                      (location '(0 0)))
  (make-instance 'resistor
    'descriptor descript
    'from-node from
    'to-node to
    :location (make-point (first location) (second location))))

(defmethod equal-resistors-p ((res-1 resistor) (res-2 resistor))
   (and (resistors-have-same-names-p res-1 res-2)
         (equal (from-node res-1) (from-node res-2))
         (equal (to-node res-1) (to-node res-2))))

(defmethod equal-resistors-p ((res-1 t) (res-2 t))
  (equal res-1 res-2))

(defmethod resistors-have-same-names-p ((res-1 resistor) (res-2 resistor))
  (equal (descriptor res-1) (descriptor res-2)))

(defmethod to-string ((a-resistor resistor))
  (format nil "(~a ~a ~a)"
          (descriptor a-resistor) (from-node a-resistor) (to-node a-resistor)))

(defmethod describe-me ((a-resistor resistor))
  (format t "~&Resistor: ~a runs from ~a to ~a, placed at (~a ~a)~&"
          (descriptor a-resistor) (from-node a-resistor) (to-node a-resistor)
          (point-v (location a-resistor)) (point-h (location a-resistor))))

(defmethod describe-resistor ((a-resistor resistor))
  (list (descriptor a-resistor) (from-node a-resistor) (to-node a-resistor)))

;; reading/writing resistors
(defmethod write-object ((a-resistor resistor))
   "Convert a resistor into a writable list"
   (list (point-h (location a-resistor))
          (point-v (location a-resistor))
          (descriptor a-resistor)
          (from-node a-resistor)
          (to-node a-resistor)))

(defmethod read-resistor ((a-list list))
   "Convert list back into a resistor"
   (assert (= 5 (length a-list)))
   (make-instance 'resistor
      :location (make-point (first a-list) (second a-list))
      'descriptor (third a-list)
      'from-node (fourth a-list)
      'to-node (fifth a-list)))



