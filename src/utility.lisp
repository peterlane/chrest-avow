(in-package :chrest-avow)

;;;; utility routines

;;; for lists

(defmethod equal-lists-p ((list-1 list) (list-2 list) &optional (test #'eql))
   (and (= (length list-1) (length list-2))
        (null (set-exclusive-or list-1 list-2 :test test))))

(defmethod equal-ordered-lists ((list-1 list) (list-2 list) &optional (test #'equal))
   (and (equal (length list-1) (length list-2))
         (loop for item-1 in list-1
                and item-2 in list-2
                always (apply test item-1 (list item-2)))))

(defmethod sum-list ((a-list list))
   "Assumes a-list is a list of numbers"
   (reduce #'+ a-list))

(defmethod include-in-lists ((list-1 list) (list-2 list))
   "For list '((a b) (c d)) '(e f) return '((a b e) (c d f))"
   (assert (= (length list-1) (length list-2)))
   (loop for item-1 in list-1
          as item-2 in list-2
          collect (append item-1 (list item-2))))


;; get all size n sets from a given list

(defun get-all-length-n (alist n)
  (if (>= n (length alist)) 
    (list alist)
    (let ((result '()))
      (alexandria:map-combinations #'(lambda (c) (push c result))
                                   alist
                                   :length n)
      result)))

; return alist ++ alist with every element appended by item
(defun add-item-to-list (item alist)
   (if (null alist)
      (list (list item))
      (append alist
               (loop for element in (cons () alist)
                      if (listp element)
                      collect (append element (list item))
                      else collect (append (list element item))))))

;;; for strings

(defun smaller-label-p (a-label b-label)
   "Return the label which is alphabetically 'smaller'"
   (if (string< a-label b-label)
      a-label
      b-label))

(defun larger-label-p (a-label b-label)
   "Return the label which is alphabetically 'larger'"
  (if (string> a-label b-label)
    a-label
    b-label))

;;; for numbers

(defmethod square (a-num)
  (assert (numberp a-num))
  (* a-num a-num))

(defmethod squared-distance (point-a point-b)
  (+ (square (- (point-h point-b) (point-h point-a))) 
     (square (- (point-v point-b) (point-v point-a)))))

(defun positivep (a-num)
   (assert (numberp a-num))
   (> a-num 0))

(defun sort-horizontally-p (x y)
   (< (point-h x) (point-h y)))


(defclass POINT ()
  ((h :accessor point-h :initarg :h)
   (v :accessor point-v :initarg :v)))

(defun make-point (h v)
  (make-instance 'point :h h :v v))

(set-dispatch-macro-character #\# #\@ 
                              #'(lambda (s c n) 
                                  (let ((items (read s nil (values) t)))
                                    (if (= 2 (length items))
                                      (make-point (first items) (second items))
                                      (error "#@ needs two values")))))

(defmethod equal-point-p ((point-1 point) (point-2 point))
  (and (= (point-h point-1) (point-h point-2))
       (= (point-v point-1) (point-v point-2))))

(defmethod points-adjacent-p (potential-point focal-size target-point)
  (> (square focal-size) (squared-distance potential-point target-point)))

(defmethod add-points ((point-1 point) (point-2 point))
  (make-point (+ (point-h point-1) (point-h point-2))
              (+ (point-v point-1) (point-v point-2))))

(defmethod subtract-points ((point-1 point) (point-2 point))
  (make-point (- (point-h point-1) (point-h point-2))
              (- (point-v point-1) (point-v point-2))))

(defmethod read-a-pattern ((a-list list))
   "Create a relevant pattern type for the read list -- this must cover all pattern types"
   (assert (= 3 (length a-list)))
   (cond ((equal 'pattern (first a-list)) (read-pattern (rest a-list)))
          ((equal 'circuit-pattern (first a-list)) (read-circuit-pattern (rest a-list)))
          ;((equal 'avow-pattern (first a-list)) (read-avow-pattern (rest a-list))) ;; TODO
          (T (error "Unknown pattern type read"))))


