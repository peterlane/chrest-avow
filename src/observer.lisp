(in-package :chrest-avow)

;;;; observed class

;;;; for those objects which can have an observer attached
;; class supports methods for attaching/removing/clearing and notifying observers

;;;; Peter Lane, 31st January 2001

;;;; 26/03/01 - add messaging system

(defclass OBSERVED ()
  ((my-observers :accessor my-observers :initarg my-observers 
                 :initform nil :type list)))

;; we have a class for update-messages which carries a msg labelling the type of message

(defclass UPDATE-MESSAGE ()
  ((my-msg :accessor my-msg :initarg :my-msg)))

;; functions for updating the observed object

(defmethod attach-observer ((an-obs observed) (observer-list list))
  "Lists of observers can be attached one at a time"
  (dolist (observer observer-list)
    (attach-observer an-obs observer)))

(defmethod attach-observer ((an-obs observed) (observer t))
  "A single observer can be attached to the observed object"
  (push observer (my-observers an-obs)))

(defmethod remove-observer ((an-obs observed) observer)
  (setf (my-observers an-obs) (remove observer (my-observers an-obs))))

(defmethod clear-observers ((an-obs observed))
  (setf (my-observers an-obs) nil))

(defmethod notify-observers ((an-obs observed) (msg update-message))
  (dolist (observer (my-observers an-obs))
    (update observer an-obs msg)))


;;;; update-message class for CHREST-subject observers
;;;; enables system to respond purely to relevant updates

;;;; Peter Lane, 26th March, 2001
;;;; rewritten 30th July 2001 using symbol creation

;; on loading, the following is executed, which works through a list of message types
;; and creates two functions for each message type:  i.e. for MSG

;; update-MSG-message-p            checks whether a msg is of the named type
;; notify-observers-of-MSG-change  
;;              calls notify-observers on the given instance of observed 
;;                    with an instance of the named message

;; though more complex, this format is used to avoid listing all the functions 
;; for each message type

(defun create-message (msg-type)
  (let ((check (intern (format nil "~a~a~a" 'update- msg-type '-message-p)))
        (notify (intern (format nil  "~a~a~a" 'notify-observers-of- msg-type '-change)))
        (opt msg-type)) ; create a reference to item name separate for each message group
    ; the check function checks if the message is of the named type
    (setf (symbol-function check)
          #'(lambda (msg) 
              (assert (typep msg 'update-message))
              (equal opt (my-msg msg))))
    ; the notify function simply acts as syntactic sugar for the notify-observers 
    ; call, creating an instance of the named message
    (setf (symbol-function notify)
          #'(lambda (an-obs)
              (assert (typep an-obs 'observed))
              (notify-observers an-obs 
                                (make-instance 'update-message :my-msg opt))))))

(dolist (msg-type (list 'LTM 'CLEAR-LTM 'STM 'ENVIRONMENT 'TEST-HISTORY 'TRAINING-HISTORY
                        'FIXATION-HISTORY 'FIXATION-RECORD 'TEST-RECORD
                        'LEARNING-OPERATIONS))
  (create-message msg-type))

;; learner-observer class - modified to remove quickdraw requirements

(defclass LEARNER-OBSERVER (observed)
  ((subject-list :accessor my-subject-list :initform ())
   (current-circuit :accessor current-circuit :initform (create-new-circuit) :type circuit)
   (current-circuit-list :accessor current-circuit-list :initarg :current-circuit-list :type list)
   (current-subject :accessor current-subject :initform ())
   (current-subject-list :accessor current-subject-list :initform () :type list)
   (attached-windows :accessor attached-windows :initform () :type list)))

(defmethod create-new-observer ()
  "Creating a new observer requires the circuit and subject lists to be reset"
  (let ((new-observer (make-instance 'learner-observer 
                                     :current-circuit-list (get-circuit-list))))
    (reset-current-circuit new-observer)
    (reset-current-subject new-observer)
    (reset-current-subject-list new-observer)
    new-observer))

;;; manage information on the current circuit(s)

(defmethod reset-current-circuit ((an-obs learner-observer))
  (setf (current-circuit an-obs) (get-first-example an-obs)))

(defmethod get-first-example ((an-obs learner-observer))
  (first (current-circuit-list an-obs)))

;;; manage information on the current list of subjects
(defmethod reset-current-subject-list ((an-obs learner-observer))
   (setf (current-subject-list an-obs) ()))

(defmethod add-to-current-subject-list ((an-obs learner-observer) new-subject)
  "Place a new subject onto the observer's subject list.
  If this is the first subject, then also make it the current subject"
  (when (null (current-subject-list an-obs))
    (setf (current-subject an-obs) new-subject))
  (push new-subject (current-subject-list an-obs)))

;;; manage information on the current subject

(defmethod reset-current-subject ((an-obs learner-observer))
   (setf (current-subject an-obs) ()))

(defmethod set-current-subject ((an-obs learner-observer) new-subject)
   (setf (current-subject an-obs) new-subject))

(defmethod update ((an-obs learner-observer) (an-obsrvd observed) (msg update-message))
  "Update what this observer is displayed - to be overridden in child classes"
  (declare (ignore an-obs an-obsrvd msg))
  '())
