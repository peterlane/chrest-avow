(in-package :chrest-avow)

;; CHREST+ Discrimination network

;; Peter Lane, rewrite 29th January 2001
;; better object-oriented packaging through new pattern and feature classes

;; 23/3/01 - pass subject to learning routines, so trace of familiarisation/discrimination possible

;; STILL TO DO
; **** chunking of tests
; **** similarity links
; **** templates

(defclass CHREST-SUBJECT (observed)
  ((i-observe :accessor i-observe :initarg i-observe :type learner-observer)
   (my-name :accessor name :initarg my-name :type string)
   (my-stm :accessor get-STM :initarg my-STM :type stm-queue)
   (active-node :accessor active-node :initform nil) ;  :type node)
   (focused-pattern :accessor focused-pattern :initform (null-pattern) :type pattern)
   (my-ltm :accessor get-LTM :initarg my-LTM :type node)
   (my-focal-size :accessor my-focal-size :initarg my-focal-size :initform 10)
   (my-fixation-point :accessor current-fixation-point :initform (make-point 0 0))

   (my-fixation-history :accessor my-fixation-history :initform nil :type list)
   (my-fixation-record :accessor my-fixation-record :initform nil :type list)
   (my-training-history :accessor my-training-history :initarg :my-training-history :initform nil :type list)
   (my-testing-history :accessor my-testing-history :initarg :my-testing-history :initform nil :type list)
   (my-solutions :accessor my-solutions :initform nil :type list)

   ; current-circuit slot should be a generic problem-solution type slot
   (current-circuit :accessor current-circuit :initarg current-circuit :type circuit)
   (viewed-object :accessor viewed-object :initform (null-pattern) :type pattern)
   (viewed-object-name :accessor viewed-object-name :initform ())
   (learning-operations :accessor learning-operations :initform nil)
   (properties-display :accessor properties-display :initform nil)
   (subject-display :accessor subject-display :initform nil)

   (train-mode :initform 'SINGLE :accessor train-mode)
   (learn-rate :initform 'SINGLE :accessor learn-rate)
   (learn-recall-mode :initform 'LEARN :accessor learn-recall-mode)

   (recognised-patterns :initform nil :accessor recognised-patterns)

   (eye-strategy :initform (make-instance 'eye-strategy-none) :accessor eye-strategy)
   (familiarise-after-discrimination :initform () :accessor familiarise-after-discrimination-p)
   (test-on-learning :initform () :accessor test-on-learning-p)
   (learn-whilst-testing :initform () :accessor learn-whilst-testing-p)))

(defclass EYE-STRATEGY () ())

(defclass EYE-STRATEGY-NONE (eye-strategy) ())
(defclass EYE-STRATEGY-RANDOM (eye-strategy) ())
(defclass EYE-STRATEGY-MEMORY (eye-strategy) ())
(defclass EYE-STRATEGY-READING (eye-strategy) ())
(defclass EYE-STRATEGY-REVERSE (eye-strategy) ())
(defclass EYE-STRATEGY-FOLLOW (eye-strategy) ())

(defmethod get-eye-strategy-description ((eye eye-strategy-none)) "None")
(defmethod get-eye-strategy-description ((eye eye-strategy-random)) "Random")
(defmethod get-eye-strategy-description ((eye eye-strategy-memory)) "Random with memory")
(defmethod get-eye-strategy-description ((eye eye-strategy-reading)) "As reading")
(defmethod get-eye-strategy-description ((eye eye-strategy-reverse)) "Reverse reading")
(defmethod get-eye-strategy-description ((eye eye-strategy-follow)) "Follow last direction")

(defmethod get-eye-strategy-letter ((eye eye-strategy-none)) "No")
(defmethod get-eye-strategy-letter ((eye eye-strategy-random)) "Ra")
(defmethod get-eye-strategy-letter ((eye eye-strategy-memory)) "Me")
(defmethod get-eye-strategy-letter ((eye eye-strategy-reading)) "Re")
(defmethod get-eye-strategy-letter ((eye eye-strategy-reverse)) "Ba")
(defmethod get-eye-strategy-letter ((eye eye-strategy-follow)) "Fo")

(defun get-eye-strategy-class (eye-strategy-code)
   "Convert the letter code back into an eye strategy instance"
   (cond ((equal eye-strategy-code "No") (make-instance 'eye-strategy-none))
          ((equal eye-strategy-code "Ra") (make-instance 'eye-strategy-random))
          ((equal eye-strategy-code "Me") (make-instance 'eye-strategy-memory))
          ((equal eye-strategy-code "Re") (make-instance 'eye-strategy-reading))
          ((equal eye-strategy-code "Ba") (make-instance 'eye-strategy-reverse))
          ((equal eye-strategy-code "Fo") (make-instance 'eye-strategy-follow))
          (T (error "Unknown eye strategy code"))))

;; handle setting and testing of eye-strategy
(defmethod set-eye-strategy-none ((a-subject chrest-subject))
  (setf (eye-strategy a-subject) (make-instance 'eye-strategy-none)))

(defmethod eye-strategy-none-p ((a-subject chrest-subject))
  (typep (eye-strategy a-subject) 'eye-strategy-none))

(defmethod eye-strategy-none-p (eye-strategy)
  (typep eye-strategy 'eye-strategy-none))

(defmethod set-eye-strategy-random ((a-subject chrest-subject))
  (setf (eye-strategy a-subject) (make-instance 'eye-strategy-random)))

(defmethod eye-strategy-random-p ((a-subject chrest-subject))
  (typep (eye-strategy a-subject) 'eye-strategy-random))

(defmethod eye-strategy-random-p (eye-strategy)
  (typep eye-strategy 'eye-strategy-random))

(defmethod set-eye-strategy-random-memory ((a-subject chrest-subject))
  (setf (eye-strategy a-subject) (make-instance 'eye-strategy-memory)))

(defmethod eye-strategy-random-memory-p ((a-subject chrest-subject))
  (typep (eye-strategy a-subject) 'eye-strategy-memory))

(defmethod eye-strategy-random-memory-p (eye-strategy)
  (typep eye-strategy 'eye-strategy-memory))

(defmethod set-eye-strategy-reading ((a-subject chrest-subject))
  (setf (eye-strategy a-subject) (make-instance 'eye-strategy-reading)))

(defmethod eye-strategy-reading-p ((a-subject chrest-subject))
  (typep (eye-strategy a-subject) 'eye-strategy-reading))

(defmethod eye-strategy-reading-p (eye-strategy)
  (typep eye-strategy 'eye-strategy-reading))

(defmethod set-eye-strategy-reverse-reading ((a-subject chrest-subject))
  (setf (eye-strategy a-subject) (make-instance 'eye-strategy-reverse)))

(defmethod eye-strategy-reverse-reading-p ((a-subject chrest-subject))
  (typep (eye-strategy a-subject) 'eye-strategy-reverse))

(defmethod eye-strategy-reverse-reading-p (eye-strategy)
  (typep eye-strategy 'eye-strategy-reverse))

(defmethod set-eye-strategy-follow-last ((a-subject chrest-subject))
  (setf (eye-strategy a-subject) (make-instance 'eye-strategy-follow)))

(defmethod eye-strategy-follow-last-p ((a-subject chrest-subject))
  (typep (eye-strategy a-subject) 'eye-strategy-follow))

(defmethod eye-strategy-follow-last-p (eye-strategy)
  (typep eye-strategy 'eye-strategy-follow))

; two data structures - one for the nodes and the other for the links in the network
(defclass NODE ()
  ((my-contents :initarg :contents :accessor node-contents :initform (null-pattern) :type pattern)
   (my-image :initarg :image :accessor node-image :initform (null-pattern) :type pattern)
   (my-equivalence :initarg :equivalence :accessor node-equivalence :initform nil) ; :type node)
   (my-link-list :initarg :link-list :accessor get-node-children :initform nil :type list)
   (is-root-node :initarg :is-root-node :accessor is-root-node :initform nil :type boolean)))

(defclass LINK ()
  ((my-key :initarg :key :accessor link-key :initform (null-pattern) :type pattern)
   (my-node :initarg :node :accessor link-node :initform nil))) ; :type node)))

; accessor functions - create, retrieve information, search and build network

(defmethod create-new-node ((new-contents pattern) &optional (new-image (new-null-for new-contents)))
   (assert (typep new-image 'pattern))
   (make-instance 'node :contents new-contents :image new-image))

(defun create-new-network-root ()
  (make-instance 'node :is-root-node T))

(defmethod create-new-link ((given-test pattern) (given-node node))
  (make-instance 'link :key given-test :node given-node))

; get-node-image::node -> pattern
(defmethod get-node-image ((self node))
  (if (is-root-node self)
    (create-new-pattern (create-new-feature '(type rootnode)))  ; this needs changing
    (node-image self)))

(defmethod update-node-image ((new-image pattern) (self node))
  (setf (node-image self) new-image))

(defmethod get-node-contents ((self node))
  (node-contents self))

; get-child-nodes:node -> [node]
(defmethod get-child-nodes ((a-node node))
  (mapcar #'link-node (get-node-children a-node)))


(defmethod add-new-child ((new-child link) (self node))
  (when (not (member-if #'(lambda (x) (equal-patterns-p x (get-node-contents (link-node new-child))))
                        (mapcar #'(lambda (x) (get-node-contents (link-node x))) (get-node-children self))))
    (push new-child (get-node-children self))))

(defmethod has-children-p ((a-node node))
  (if (get-node-children a-node) T ()))

(defmethod leaf-node-p ((a-node node))
   (if (get-node-children a-node) () T))

; has-equivalence-p::node -> boolean
(defmethod has-equivalence-p ((a-node node))
  (if (node-equivalence a-node) T ()))



; recognise-pattern:: (pattern node) -> node
(defmethod recognise-pattern ((target pattern) (given-root node))
  (find-node target given-root))

; decompose-pattern:: (pattern node) -> [node]
; this method identifies several nodes within the network whose 
; combined images comprise the target pattern. 
(defmethod decompose-pattern ((target pattern) (given-root node))
  (let ((found-sub-patterns ())
        (unmatched-pattern target))
    (loop as safety-net below 10  ; ten chunks is a reasonable upper limit
          until (combined-equals-pattern-p found-sub-patterns target)
          do
          (let* ((new-found-node (find-node unmatched-pattern given-root))
                 (found-image (node-image new-found-node)))
            (when (is-root-node new-found-node)
              (return)) ; cannot find any relevant node, so exit
            (push (get-strict-match found-image unmatched-pattern target) found-sub-patterns)
            (setf unmatched-pattern (remove-pattern 
                                      unmatched-pattern 
                                      (get-strict-match found-image unmatched-pattern target)))))
    (reverse (remove-if #'null-p found-sub-patterns))))

; learn:: (pattern node) -> void
; note, must decide whether to familiarise or discriminate 
; - does latter if target and found-image differ, and found-image cannot be extended
; - a-subject is passed in, so that the learning-rate parameter can be used,
;   and to update learning-history
(defmethod learn ((target pattern) (given-root node) (a-subject chrest-subject))
  (let* ((found-node (find-node target given-root))
         (found-image (node-image found-node)))
    (cond ((is-root-node found-node) 
           (discriminate target found-node a-subject))
          ((null-p found-image)
           (familiarise target found-node a-subject))
          ((and (has-end-p found-image) (equal-patterns-p found-image target))
           ()) ; do nothing as they are equal and image is fully familiarised
          ((and (not (has-end-p found-image)) (matching-patterns-p found-image target))
           (familiarise target found-node a-subject))
          (T 
           (discriminate target found-node a-subject)))))

; create-equivalence-link:: node pattern1 pattern2 -> void
; inserts an equivalence link between the nodes indexed by the given patterns
; if an equivalence link already exists, this function first clears it
(defmethod create-equivalence-link ((given-root node) (pattern1 pattern) (pattern2 pattern))
   (let ((node-1 (recognise-pattern pattern1 given-root))
         (node-2 (recognise-pattern pattern2 given-root)))
     (when (and node-1 node-2)
       (clear-existing-equivalence node-1)
       (clear-existing-equivalence node-2)
       (setf (node-equivalence node-1) node-2)
       (setf (node-equivalence node-2) node-1))))

(defmethod clear-existing-equivalence ((given-node node))
  (when (has-equivalence-p given-node)
    (setf (node-equivalence (node-equivalence given-node)) ())
    (setf (node-equivalence given-node) ())))

; retrieve-equivalence-link:: pattern node -> node
(defmethod retrieve-equivalence-link ((target pattern) (given-root node))
  (when (non-null-p target)
    (let ((found-node (recognise-pattern target given-root)))
      (if (and (not (is-root-node found-node)) (node-equivalence found-node))
        (node-equivalence found-node)
        given-root))))


;; retrieve information about the network
(defmethod count-nodes ((given-root node))
  (network-count-if given-root #'identity))

(defmethod count-same-type ((given-root node) (a-pat pattern))
  (network-count-if given-root #'(lambda (a-node) (typep (node-image a-node) (type-of a-pat)))))

; every node must be the child of a single test link, except for the root-node
(defmethod count-test-links ((given-root node))
  (network-count-if given-root #'(lambda (a-node) (not (is-root-node a-node)))))

; halve total, as each lateral link is referenced twice in the network
(defmethod count-equivalence-links ((given-root node))
  (/ (network-count-if given-root #'has-equivalence-p) 2))

; count-if instantiates a visitor pattern, counting the number of times test-function is T for nodes from given-root
(defmethod network-count-if ((given-root node) test-function)
  (do-network-count given-root (get-child-nodes given-root) (update-count 0 given-root test-function) test-function))

(defmethod do-network-count ((given-root node) children count test-function)
  (if (endp children) 
    count
    (do-network-count (first children) 
                      (append (rest children) (get-child-nodes (first children)))
                      (update-count count (first children) test-function) 
                      test-function)))

(defmethod update-count (old-count (a-node node) test-function)
  (if (apply test-function a-node ()) 
    (incf old-count)
    old-count))

(defmethod get-leaf-nodes ((a-root node))
   "Return the leaf nodes below the given root"
   (remove-if-not #'leaf-node-p (get-all-nodes a-root)))

(defmethod get-all-nodes ((a-root node))
   "Return a list of nodes visible from the given root"
   (labels ((get-nodes (child-list node-list)
               (if (endp child-list) 
                  node-list
                  (get-nodes (append (get-child-nodes (first child-list)) (rest child-list))
                             (append (list (first child-list)) node-list)))))
      (get-nodes (list a-root) ())))

;; compute the similarity of two chrest trees - return a pair (match-1-2 match-2-1)
; difficulty in the similarity match, as more than one node may match the similarity function
(defvar *earlier-matches* ())  ; uses list to memo-ise results

(defmethod degree-of-match ((root-1 node) (root-2 node))
   (setf *earlier-matches* ())
   (let* ((nodes-1 (get-all-nodes root-1))
           (nodes-2 (get-all-nodes root-2))
           (num-similar-nodes 
            (length (intersection nodes-1 nodes-2 :test #'similar-nodes-p))))
      (list
       (/ num-similar-nodes (length nodes-1))
       (/ num-similar-nodes (length nodes-2)))))

(defmethod similar-nodes-p ((node-1 node) (node-2 node))
   (flet ((similar-node-p ()
             (let ((input-nodes-are-similar
                     (and (equal-patterns-p (node-image node-1) (node-image node-2))
                           (or (not (has-equivalence-p node-1))
                                (not (has-equivalence-p node-2))
                                (equal-patterns-p (node-image (node-equivalence node-1))
                                                  (node-image (node-equivalence node-2)))))))
                (if (and (leaf-node-p node-1) (leaf-node-p node-2))
                   input-nodes-are-similar
                   (and input-nodes-are-similar ; the nodes themselves must be similar
                         (= (length (get-node-children node-1))
                             (length (get-node-children node-2))) ; shortcut checking child nodes
                         (null (set-exclusive-or (get-child-nodes node-1) (get-child-nodes node-2)
                                                     :test #'similar-nodes-p)))))))
      (let ((result (find-if #'(lambda (x) (and (eq node-1 (first x))
                                                    (eq node-2 (second x)))) *earlier-matches*)))
         (cond (result T)  ;; done before, so return true
                (T          ;; else, compute new result, and add to *earlier-matches* list
                 (setf result (similar-node-p))
                 (when result (push (list node-1 node-2) *earlier-matches*))
                 result)))))


;; internal functions

; find-node uses a recursive procedure call to return first matching node 
; from the given-root for the given target
; Note: it will only follow a test containing an end-feature if the 
; target pattern is equal to the contents of the given root 
(defmethod find-node ((target pattern) (given-root node) &optional (children (get-node-children given-root)))
  (cond ((endp children) given-root)
        ((and (has-end-p (link-key (first children)))
              (equal-patterns-p target (get-node-contents (link-node (first children)))))
         (link-node (first children)))
        ((and (not (has-end-p (link-key (first children))))
              (matching-patterns-p (get-node-contents (link-node (first children))) target))
         (find-node target (link-node (first children))))
        (T (find-node target given-root (rest children)))))

; familiarise attempts to extend the image of the given-node with something from the target
; if the image already equals the target, then the end-marker is added
(defmethod familiarise ((target pattern) (given-node node) (a-subject chrest-subject))
  (add-to-familiarisation-list a-subject)
  (if (equal-patterns-p (node-image given-node) target)
    (update-node-image (add-end (node-image given-node)) given-node)
    (update-node-image (learn-more-of-pattern (node-image given-node) target (learn-rate a-subject)) given-node)))

; discriminate constructs a new link and link node based on the mismatch between the target
; and the contents of the given-node, i.e. uses all possible features before attempting 
; to use the end-marker
(defmethod discriminate ((target pattern) (given-node node) (a-subject chrest-subject))
  (when (can-discriminate-p target given-node)
    (add-to-discrimination-list a-subject) ; mismatch, as discriminate may feel if tries to duplicate a child
    (let* ((new-test (discriminate-patterns (node-contents given-node) target (learn-rate a-subject)))
           (new-node (create-new-node (create-new-contents given-node new-test target)))
           (new-link (create-new-link new-test new-node)))
      (add-new-child new-link given-node)
      (when (familiarise-after-discrimination-p a-subject)
        (familiarise target new-node a-subject)))))

; can only discriminate when the patterns are distinguishable OR ELSE end is not in previous test
; note: as end is only taken if the target and image match, we can never want to discriminate 
; further.  Any longer images will be discriminated on the previous level
(defmethod can-discriminate-p ((target-pat pattern) (given-node node))
  (or (is-root-node given-node)
      (null-p (node-contents given-node))
      (not (equal-patterns-p (node-contents given-node) target-pat))
      (not (has-end-p (node-contents given-node)))))

(defmethod discriminate-patterns ((base-pat pattern) (target-pat pattern) learn-rate)
  (let ((new-test (distinguish-patterns base-pat target-pat learn-rate)))
    (if (non-null-p new-test)
      new-test
      (new-end-for base-pat))))

(defmethod create-new-contents ((given-node node) (new-test pattern) (target pattern))
  (if (null-p (node-contents given-node))
    new-test
    (augment-pattern (get-my-match (node-contents given-node) target) new-test)))


;; handle setting and testing of learn-rate
(defmethod set-learn-rate-single ((a-subject chrest-subject))
  (setf (learn-rate a-subject) 'SINGLE))
;

