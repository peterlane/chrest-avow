(in-package :chrest-avow)

;;;; class of circuit-pattern

;;;; This is a subclass of patterns, and contains the same set of 
;;;; methods.  Also uses class circuit-feature to hold resistors et al.

;;;; Uses notion of circuit as a graph to provide matching 
;;;; independent of variable names.

;;;; Peter Lane, 31st January 2001

(defclass CIRCUIT-FEATURE (feature) ())

(defclass CIRCUIT-PATTERN (pattern) ())

(defun create-new-circuit-pattern (&optional (new-pattern ()))
  (make-instance 'circuit-pattern :pattern (alexandria:ensure-list new-pattern)))

(defmethod create-new-pattern-for ((a-pat circuit-pattern) &optional (new-pattern ()))
  (declare (ignore a-pat))
  (create-new-circuit-pattern new-pattern))

(defmethod create-new-pattern-for ((a-feat circuit-feature) &optional (new-pattern ()))
  (declare (ignore a-feat))
  (create-new-circuit-pattern new-pattern))

(defun null-circuit-pattern () (create-new-circuit-pattern))

(defmethod new-null-for ((a-pat circuit-pattern))
  (declare (ignore a-pat))
  (null-circuit-pattern))

;; methods for reading/writing objects 
(defmethod write-object ((a-pattern circuit-pattern))
   "Convert a pattern into a writable list"
   (list 'circuit-pattern
          (mapcar #'write-object (get-pattern a-pattern))
          (has-end-p a-pattern)))

(defmethod read-circuit-pattern ((a-list list))
   "Convert list back into a circuit pattern"
   (assert (= 2 (length a-list)))
   (make-instance 'circuit-pattern 
      :pattern (mapcar #'read-resistor (first a-list))
      :has-end-p (second a-list)))

(defstruct NODE-POSN name size)

(defmethod get-topmost-node ((a-pat circuit-pattern))
  (let ((node-sizes ()))
    (dolist (a-resistor (get-pattern a-pat))
      (push (make-node-posn :name (from-node a-resistor)
                            :size (- (point-v (location a-resistor)) 1))
            node-sizes))
    (node-posn-name (first (sort node-sizes #'< :key #'node-posn-size)))))

(defmethod get-lowest-node ((a-pat circuit-pattern))
  (let ((node-sizes ()))
    (dolist (a-resistor (get-pattern a-pat))
      (push (make-node-posn :name (to-node a-resistor)
                            :size (+ (point-v (location a-resistor)) 1))
            node-sizes))
    (node-posn-name (first (sort node-sizes #'> :key #'node-posn-size)))))

(defmethod matching-patterns-p ((pattern-1 circuit-pattern) (pattern-2 circuit-pattern))
   "Use matching-subcircuit-p to check if pattern-1 matches pattern-2"
   (or (and (null-p pattern-1) (null-p pattern-2))
        (null-p pattern-1)
        (and (not-larger-p pattern-1 pattern-2)
              (or (null-p pattern-1)
                   (matching-subcircuit-p pattern-1 pattern-2)))))

; get-my-match returns some sub-circuit of large-pat which is isomorphic to a subcircuit of target-pat
; note: context-pattern provides context for matching the subcircuit
(defmethod get-my-match ((target-pat circuit-pattern) (large-pat circuit-pattern) 
                         &optional (context-pat large-pat))
  (declare (ignore context-pat))
  (loop for try-circuit in (get-all-smaller target-pat large-pat)
        when (matching-subcircuit-p try-circuit target-pat)
        return try-circuit
        finally (return (null-circuit-pattern))))

(defmethod get-strict-match ((target-pat circuit-pattern) (large-pat circuit-pattern) 
                         &optional (context-pat large-pat))
  (loop for try-circuit in (get-all-smaller target-pat large-pat context-pat)
        when (matching-subcircuit-p try-circuit target-pat)
        return try-circuit
        finally (return (null-circuit-pattern))))

; check if feature is in the given pattern - match is based on resistor name only
(defmethod feature-in-pattern-p ((a-resistor circuit-feature) (a-pat circuit-pattern))
   (member-if #'(lambda (y) (resistors-have-same-names-p a-resistor y)) (get-pattern a-pat)))

; basic feature class for circuit-patterns is the resistor - see separate file

(defmethod equal-features-p ((feat-1 circuit-feature) (feat-2 circuit-feature))
  (equal-resistors-p feat-1 feat-2))

(defmethod visible-feature-p ((a-feat circuit-feature) a-point radius)
  "Determines whether a circuit-feature is visible from the given point with radius field-of-view"
  (points-adjacent-p a-point radius (get-scaled-location a-feat)))

; find coordinates for nodes
(defmethod get-y-coord (node-label (a-pat circuit-pattern))
  (let ((tops 0)
        (bases 0))
    (dolist (resistor (get-pattern a-pat))
      (when (equal node-label (from-node resistor))
        (setf tops
              (if (equal tops 0)
                (get-resistor-top-node (point-v (location resistor)))
                (min tops (get-resistor-top-node (point-v (location resistor)))))))
      (when (equal node-label (to-node resistor))
        (setf bases
              (if (equal bases 0)
                (get-resistor-base-node (point-v (location resistor)))
                (max bases (get-resistor-base-node (point-v (location resistor))))))))
    (cond ((equal tops 0) bases)
          ((equal bases 0) tops)
          (T (floor (/ (+ tops bases) 2))))))

(defmethod get-min-x-coord (node-label (a-pat circuit-pattern))
  (loop for resistor in (get-pattern a-pat)
        when (or (equal node-label (from-node resistor))
                 (equal node-label (to-node resistor)))
        minimize (resistor-centre (point-h (location resistor)))))

(defmethod get-max-x-coord (node-label (a-pat circuit-pattern))
  (loop for resistor in (get-pattern a-pat)
        when (or (equal node-label (from-node resistor))
                 (equal node-label (to-node resistor)))
        maximize (resistor-centre (point-h (location resistor)))))

(defmethod get-min-x ((a-pat circuit-pattern))
  (loop for resistor in (get-pattern a-pat)
        minimize (get-resistor-left (point-h (location resistor)))))

(defmethod get-max-x ((a-pat circuit-pattern))
  (loop for resistor in (get-pattern a-pat)
        maximize (get-resistor-right (point-h (location resistor)))))

(defmethod get-min-y ((a-pat circuit-pattern))
  (loop for resistor in (get-pattern a-pat)
        minimize (get-resistor-top (point-v (location resistor)))))

(defmethod get-max-y ((a-pat circuit-pattern))
  (loop for resistor in (get-pattern a-pat)
        maximize (get-resistor-base (point-v (location resistor)))))

(defmethod get-width ((a-pat circuit-pattern))
  (+ (- (get-max-x a-pat) (get-min-x a-pat)) 20))

(defmethod get-height ((a-pat circuit-pattern))
  (+ (- (get-max-y a-pat) (get-min-y a-pat)) 25))

(defmethod get-left-edge ((a-pat circuit-pattern))
  (- (get-min-x a-pat) 10))

(defmethod get-right-edge ((a-pat circuit-pattern))
  (+ (get-left-edge a-pat) (get-width a-pat)))

(defmethod get-top-edge ((a-pat circuit-pattern))
  (- (get-min-y a-pat) 10))

(defmethod get-bottom-edge ((a-pat circuit-pattern))
  (+ (get-top-edge a-pat) (get-height a-pat)))

; provide some rescaling and point calculation methods for circuit-pattern
; scale should be taken from the window itself - but currently unused for variable size displays
(defmethod get-my-scale () 40)

(defmethod resistor-gap ()
   (floor (/ (get-my-scale) 2)))

(defmethod rescale-circuit-point (x)
  (* (get-my-scale) x))

(defmethod get-scaled-location ((a-feat circuit-feature))
  (make-point (rescale-circuit-point (point-h (location a-feat)))
              (rescale-circuit-point (point-v (location a-feat)))))


(defmethod resistor-centre (x)
  (rescale-circuit-point x))

(defmethod half-my-height ()
  (floor (/ (get-my-scale) 3)))

(defmethod half-my-width ()
  (floor (/ (get-my-scale) 5)))

(defmethod resistor-to-node ()
  (- (resistor-gap) (half-my-height)))

(defmethod get-resistor-left (x)
  (- (rescale-circuit-point x) (resistor-to-node)))

(defmethod get-resistor-right (x)
  (+ (rescale-circuit-point x) (resistor-to-node)))

(defmethod get-resistor-top (y)
  (- (rescale-circuit-point y) (half-my-height)))

(defmethod get-resistor-base (y)
  (+ (rescale-circuit-point y) (half-my-height)))

(defmethod get-resistor-top-node (y)
  (- (rescale-circuit-point y) (resistor-gap)))

(defmethod get-resistor-base-node (y)
  (+ (rescale-circuit-point y) (resistor-gap)))


; --------- below deals with graph isomorphisms, customised for circuits

; the following checks for isomorphism of two circuit descriptions
; this code could be generalised for arbitrary directed graphs 
; note that this problem is known to be non-polynomial!
; in this instance, the amount of computation should be relatively 
; small, as the circuits are small.  
(defmethod isomorphicp ((circ-1 circuit-pattern) (circ-2 circuit-pattern))
   "Tests for the presence of a valid mapping between the two sets of circuit nodes"
   (let* ((nodes-1 (get-node-list circ-1))
           (nodes-2 (get-node-list circ-2))
           (possible-pairings (get-all-pairings (nodes-in nodes-1)
                                                (nodes-in nodes-2)
                                                (list nodes-1 nodes-2))))
      (some (valid-mapping-test-fn nodes-1 nodes-2) possible-pairings)))

(defmethod not-isomorphicp ((circ-1 circuit-pattern) (circ-2 circuit-pattern))
   (not (isomorphicp circ-1 circ-2)))

; find some sub-circuit in large-pat which matches the target-pat
(defmethod matching-subcircuit-p ((target-pat circuit-pattern) (large-pat circuit-pattern))
   (flet ((match (try-circuit) (and (isomorphicp target-pat try-circuit)
                                      (identical-layout-p target-pat try-circuit))))
      (some #'match (get-all-with-same-size target-pat large-pat))))

; identical-layout-p checks that the two circuits, assumed to be isomorphicp, 
; would be depicted identically, i.e. horizontal alignments from each node are identical
(defmethod identical-layout-p ((pattern-1 circuit-pattern) (pattern-2 circuit-pattern))
   "1. obtain mapping between the two patterns
        2. check that, for each node, the order of descendent nodes in each pattern is the same"
   (let ((mapping (get-circuit-mapping pattern-1 pattern-2)))
      (labels ((get-nodes-from (a-pattern a-node)
                  "Retrieve the nodes accessible from each from-link of a-node, sorted into left-right order"
                  (flet ((node-into-resistor-p (a-resistor)
                           (equal (from-node a-resistor) a-node))
                         (convert-resistor (a-resistor)
                           (list (to-node a-resistor) (location a-resistor))))                    
                    (let ((from-list 
                            (mapcar #'convert-resistor
                                     (remove-if-not #'node-into-resistor-p (get-pattern a-pattern)))))
                       (mapcar #'first (sort from-list #'sort-horizontally-p :key #'second)))))
                (mapped-child-nodes (a-node)
                  (mapcar #'mapped-node (get-nodes-from pattern-1 a-node)))
                (mapped-node (a-node) (replace-item a-node mapping))                
                (same-layout (a-node) 
                  (equal-ordered-lists (mapped-child-nodes a-node)
                                       (get-nodes-from pattern-2 (mapped-node a-node)))))
         (every #'same-layout (mapcar #'my-label (get-node-list pattern-1))))))


(defmethod get-circuit-mapping ((circ-1 circuit-pattern) (circ-2 circuit-pattern))
   "Locate the possible mapping from circ-1 to circ-2"
   (let* ((nodes-1 (get-node-list circ-1))
           (nodes-2 (get-node-list circ-2))
           (possible-pairings (get-all-pairings (nodes-in nodes-1) 
                                                (nodes-in nodes-2)
                                                (list nodes-1 nodes-2))))
      (dolist (a-mapping possible-pairings)
         (when (apply (valid-mapping-test-fn nodes-1 nodes-2) a-mapping ())
            (return a-mapping)))))

;; STUB ROUTINE, HANDLES SAME-LAYOUT CIRCUITS ONLY
(defmethod get-circuit-relabelling ((circ-1 circuit-pattern) (circ-2 circuit-pattern))
   "Return a mapping (list of pairs) indicating how the labels in circ-2 should be replaced with those in circ-1"
   (loop for res-1 in (sort-resistors (get-pattern circ-1))
          and res-2 in (sort-resistors (get-pattern circ-2))
          unless (equal (descriptor res-1) (descriptor res-2)) ; don't take equal pairs
          collect (list (descriptor res-2) (descriptor res-1)))) ; map circ-2 onto circ-1

(defmethod valid-mapping-test-fn ((node-list-1 list) (node-list-2 list))
   "Check a given pairing if it provides a valid mapping between the two node-lists"
   (lambda (a-pairing)
      (equal-node-list-p node-list-2 (apply-pairings node-list-1 a-pairing))))

(defmethod get-all-with-same-size ((target-pat circuit-pattern) (large-pat circuit-pattern))
   (get-all-of-size-n large-pat (size-of target-pat)))

(defmethod get-all-smaller ((target-pat circuit-pattern) (large-pat circuit-pattern)
                              &optional (context-pat ()))
   (loop for n from (size-of target-pat) downto 1
          append (get-all-of-size-n large-pat n context-pat)))

(defmethod get-all-of-size-n ((large-pat circuit-pattern) n 
                                &optional (context-pat ()))
   (let ((proposed-groupings (mapcar #'create-new-circuit-pattern
                                       (get-all-length-n (get-pattern large-pat) n))))
      (if context-pat 
         (remove-if #'(lambda (x) (crossed-node-p context-pat x)) proposed-groupings)
         proposed-groupings)))

; crossed-node-p checks whether any nodes are split by the proposed grouping
; would also be useful to check 'dangling' graphs, i.e. POS -> A  POS -> A -> B  leaving B -> C (horizontal split)
(defmethod crossed-node-p ((large-pat circuit-pattern) (proposed-group circuit-pattern))
   (labels ((not-same-as-old-form-p (test-node)
              (not (similar-node-p 
                     test-node
                     (get-node-in (my-label test-node) (get-node-list large-pat)))))
            (crossedp (a-node) 
              (and (positivep (get-num-to a-node)) (positivep (get-num-from a-node))
                    (not-same-as-old-form-p a-node))))
     (some #'crossedp (get-node-list proposed-group))))

(defmethod crossed-node-p ((pat-1 pattern) (pat-2 pattern))
   "A generic method should not be required for this"
   ())

; following methods define the circuit-graph-node holder
(defclass CIRCUIT-GRAPH-NODE ()
   ((label :accessor my-label :initarg :my-label :initform nil)
    (from :accessor get-from :initarg :my-from :initform () :type list)
    (from-nodes :accessor get-from-nodes :initarg :my-from-nodes :initform () :type list)
    (to :accessor get-to :initarg :my-to :initform () :type list)
    (to-nodes :accessor get-to-nodes :initarg :my-to-nodes :initform () :type list)))

(defun new-circuit-graph-node (label from to from-nodes to-nodes)
   (make-instance 'circuit-graph-node 
      :my-label label :my-from from :my-to to 
      :my-from-nodes from-nodes :my-to-nodes to-nodes))

;; note stored in reverse order
(defmethod get-first-from ((node circuit-graph-node))
   (alexandria:lastcar (get-from node)))

(defmethod get-last-from ((node circuit-graph-node))
   (first (get-from node)))

(defmethod get-num-from ((node circuit-graph-node))
   (length (get-from node)))

(defmethod get-last-to ((node circuit-graph-node))
   (first (get-to node)))

(defmethod get-first-to ((node circuit-graph-node))
   (alexandria:lastcar (get-to node)))

(defmethod get-num-to ((node circuit-graph-node))
   (length (get-to node)))

(defmethod set-from ((node circuit-graph-node) new-from)
   (setf (get-from node) new-from))

(defmethod add-to-from ((node circuit-graph-node) new-from new-from-label)
   (pushnew new-from (get-from node))
   (pushnew new-from-label (get-from-nodes node)))

(defmethod set-to ((node circuit-graph-node) new-to)
   (setf (get-to node) new-to))

(defmethod add-to-to ((node circuit-graph-node) new-to new-to-label)
   (pushnew new-to (get-to node))
   (pushnew new-to-label (get-to-nodes node)))

;; check similarity and equality of two node lists

(defmethod similar-node-p ((node-1 circuit-graph-node) 
                             (node-2 circuit-graph-node))
   (and (equal (get-num-from node-1) (get-num-from node-2))
         (equal (get-num-to node-1) (get-num-to node-2))))

(defmethod similar-node-list-p (node-list-1 node-list-2)
   "Check that the node lists are equal based on their similarity"
   (equal-lists-p node-list-1 node-list-2 #'similar-node-p))

(defmethod equal-node-list-p (node-list-1 node-list-2)
   "Check that the node lists are equal based on their connectivity"
   (flet ((equal-connections-p (node-1 node-2)
             (and (equal (my-label node-1) (my-label node-2))
                   (equal-lists-p (get-from-nodes node-1) (get-from-nodes node-2))
                   (equal-lists-p (get-to-nodes node-1) (get-to-nodes node-2)))))
      (equal-lists-p node-list-1 node-list-2 #'equal-connections-p)))

;; apply pairings to the lists of nodes

(defmethod apply-pairings (node-list pairings)
   "Apply the listed pairings to every node in the node-list"
   (flet ((apply-pairing-to-node (a-node)
            (new-circuit-graph-node 
             (replace-item (my-label a-node) pairings)
             (get-from a-node)
             (get-to a-node)
             (replace-list (get-from-nodes a-node) pairings)
             (replace-list (get-to-nodes a-node) pairings))))
      (mapcar #'apply-pairing-to-node node-list)))


(defun replace-item (x pairs)
   "Apply the given pairings to the item x"
   (loop for pair in pairs
          when (equal x (first pair)) return (second pair)))

(defun replace-list (xs pairs)
   "Apply the pairings to all items in the given list xs"
   (mapcar #'(lambda (x) (replace-item x pairs)) xs))

(defun sort-resistors (resistor-list)
   "Sort a list of resistors (circuit features) into regular order"
   (sort (copy-list resistor-list)
          #'(lambda (x y)
              (if (equal (point-h (location x)) (point-h (location y)))
                 (< (point-v (location x)) (point-v (location y)))
                 (< (point-h (location x)) (point-h (location y)))))))

(defmethod get-node-list ((a-circuit circuit-pattern))
   "Return an ordered list of the nodes within the circuit"
   (flet ((sort-node-list (node-list)
            (sort node-list #'(lambda (x y) 
                                 (if (equal (get-num-from x) (get-num-from y))
                                    (< (get-num-to x) (get-num-to y))
                                    (< (get-num-from x) (get-num-from y)))))))
     (let ((final-node-list ()))
        (dolist (resistor (sort-resistors (get-pattern a-circuit)))
           (setf final-node-list 
                  (add-new-from final-node-list
                                resistor
                                (from-node resistor)))
           (setf final-node-list 
                  (add-new-to final-node-list
                              resistor
                              (to-node resistor))))
        (sort-node-list final-node-list))))

(defmethod nodes-loads-in (node-list)
   (remove-duplicates
    (loop for node in node-list
           collect (my-label node)
           append (get-from node)
           append (get-to node))))

(defmethod nodes-in (node-list)
   (remove-duplicates (mapcar #'my-label node-list)))

; add-new-from augments existing node-list with the information
; note that: the node from which a resistor (label) is linked 
; is connected TO that resistor
(defmethod add-new-from (node-list resistor from-node)
   (if (node-not-in from-node node-list)
      (push (new-circuit-graph-node from-node () (list (descriptor resistor))
                                     () (list (to-node resistor)))
             node-list)
      (add-to-to (get-node-in from-node node-list) (descriptor resistor) (to-node resistor)))
   node-list)

(defmethod add-new-to (node-list resistor to-node)
   (if (node-not-in to-node node-list)
      (push (new-circuit-graph-node to-node (list (descriptor resistor)) () 
                                     (list (from-node resistor)) ())
             node-list)
      (add-to-from (get-node-in to-node node-list) (descriptor resistor) (from-node resistor)))
   node-list)

(defmethod node-not-in (node-label node-list)
   (not (member-if (same-label-as-p node-label) node-list)))

(defmethod get-node-in (node-label node-list)
   (find-if (same-label-as-p node-label) node-list))

(defun same-label-as-p (node-label)
   "Return function which checks if a node has same label as the given label"
   (lambda (a-node) (equal node-label (my-label a-node))))

; ----  some functions to work with lists of nodes ---- 

; get-all-pairings will extract all pairings of two lists
; if the node-list is provided, then only those pairings in 
; which the items of the list refer to similar nodes will be created
(defmethod get-all-pairings ((list-1 list) (list-2 list) &optional (node-list ()))
   (flet ((legal-pairing-p (node-1 node-2)
            "Check if node-1 and node-2 can be paired"
            (or (null node-list)
                 (similar-node-p (get-node-in node-1 (first node-list)) 
                                 (get-node-in node-2 (second node-list))))))
      (if (or (endp list-1) (endp list-2))
         ()  ; finished the lists
         (loop for y in list-2
                when (legal-pairing-p (first list-1) y) ;  node-list)
                append
                (let ((this-pair (list (first list-1) y))
                       (new-pairs (get-all-pairings (rest list-1) (remove y list-2) node-list)))
                   (if new-pairs
                      (loop for pair in new-pairs
                             collect (cons this-pair pair))
                      (list (list this-pair))))))))


