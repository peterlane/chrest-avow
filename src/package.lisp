(defpackage :chrest-avow 
  (:use :cl)
  (:export 
    ; avow-pattern
    :avow-pattern :create-new-avow-pattern :null-avow-pattern :read-avow-pattern :write-object
    :horizontal-relation-list :vertical-relation-list :get-sorted-horizontals :get-sorted-verticals
    :make-avow-box :create-box-feature :top-of :bottom-of :left-of :right-of :centre-of :equal-sides-p
    :get-first-side :get-left-side :get-right-side :get-top-side :get-bottom-side :get-width :get-height
    :get-left-of :get-right-of :get-base-of :get-top-of
    :make-avow-line :avow-line-p :avow-line-x1 :avow-line-x2 :avow-line-y1 :avow-line-y2
    :normalise-lines :normalise-features :construct-relations
    :create-reln-feature :create-less-than-feature :equal-features-p
    :get-all-object :get-lines :get-lines-for :get-boxes
    :extract-features
    ; chrest
    :create-new-network-root 
    :create-new-node :get-node-contents :node-contents :get-node-image :node-image :get-node-children
    :create-new-link :link-node :add-new-child
    :get-leaf-nodes :get-all-nodes :degree-of-match :similar-nodes-p
    :count-nodes :decompose-pattern :learn :recognise-pattern :update-count
    :set-learn-rate-single
    ; circuit
    :create-new-circuit :circuit :name :set-name :set-circuit :describe-scene
    :create-new-curriculum :curriculum :set-curriculum
    ; circuit-avow
    :circuit-avow :get-horizontal-relations :get-vertical-relations
    ; circuit-pattern
    :create-new-circuit-pattern :null-circuit-pattern
    :circuit-graph-node :new-circuit-graph-node :my-label :get-from :get-to
    :get-first-from :get-node-in :get-node-list :get-last-from
    :feature-in-pattern-p :isomorphicp
    ; eye-movement
    :avoids-previous-points-p
    ; fixations
    :same-fixation-type-p :create-default-fixation :create-image-fixation :create-test-fixation 
    :is-test-fixation-p
    ; observer
    create-new-observer :attach-observer :remove-observer :clear-observers :notify-observers :my-observers
    ; pattern
    :matching-patterns-p :equal-patterns-p :distinguish-patterns :get-pattern :has-end-p :add-end
    :null-p :non-null-p :null-pattern :augment-pattern :get-new-feature :remove-pattern 
    :learn-more-of-pattern :new-null-for :combined-equals-pattern-p
    :create-new-feature :location :feature :create-new-pattern :get-my-match :to-string
    ; properties
    ; resistor
    :create-new-resistor :descriptor :from-node :to-node :location :describe-resistor
    ; stm-queue
    :new-stm-queue :stm-list :stm-size :add-to-stm :clear-stm
    :make-stm-item
    ; subject
    :create-new-chrest-subject :make-learning-ops :learning-ops-familiarisations :learning-ops-discriminations
    :incr-familiarisation-count :incr-discrimination-count
    ; utility
    :equal-lists-p :equal-ordered-lists :sum-list :include-in-lists :get-all-length-n
    :add-item-to-list 
    :smaller-label-p :larger-label-p :square :squared-distance :positivep :sort-horizontally-p
    :invalid-message :make-point :point-h :point-v :equal-point-p :points-adjacent-p
))


