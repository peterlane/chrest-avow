(in-package :chrest-avow)

; handle setting and testing of train-mode
;(defmethod set-train-mode-single ((a-subject chrest-subject))
;  (setf (train-mode a-subject) 'SINGLE))
;
;(defmethod train-mode-single-p ((a-subject chrest-subject))
;  (equal (train-mode a-subject) 'SINGLE))
;
(defmethod train-mode-single-p (training-mode)
  (equal training-mode 'SINGLE))

;(defmethod set-train-mode-linked ((a-subject chrest-subject))
;  (setf (train-mode a-subject) 'LINKED))
;
;(defmethod train-mode-linked-p ((a-subject chrest-subject))
;  (equal (train-mode a-subject) 'LINKED))
;
(defmethod train-mode-linked-p (training-mode)
  (equal training-mode 'LINKED))

;
;(defmethod learn-rate-single-p ((a-subject chrest-subject))
;  (equal (learn-rate a-subject) 'SINGLE))
;
(defmethod learn-rate-single-p (learning-mode)
  (equal learning-mode 'SINGLE))

;(defmethod set-learn-rate-objects ((a-subject chrest-subject))
;  (setf (learn-rate a-subject) 'OBJECTS))
;
;(defmethod learn-rate-objects-p ((a-subject chrest-subject))
;  (equal (learn-rate a-subject) 'OBJECTS))
;
(defmethod learn-rate-objects-p (learning-mode)
  (equal learning-mode 'OBJECTS))

;(defmethod set-learn-rate-whole ((a-subject chrest-subject))
;  (setf (learn-rate a-subject) 'WHOLE))
;
;(defmethod learn-rate-whole-p ((a-subject chrest-subject))
;  (equal (learn-rate a-subject) 'WHOLE))
;
(defmethod learn-rate-whole-p (learning-mode)
  (equal learning-mode 'WHOLE))

;; keep options in a simple structure

(defstruct SUBJECT-OPTIONS
  training-mode test-before-learning learning-rate familiarisation-rate
  learn-during-test eye-strategy stm-size fov-size)

(defun get-new-name (options number)
  (assert (subject-options-p options))
  (format nil "~a) Tr~a Te~a Le~a Fa~a Lt~a E~a S~a F~a"
          number
          (if (equal 'SINGLE (subject-options-training-mode options)) "S" "F")
          (if (subject-options-test-before-learning options) "Y" "N")
          (cond ((equal 'SINGLE (subject-options-learning-rate options)) "S")
                ((equal 'OBJECTS (subject-options-learning-rate options)) "O")
                ((equal 'WHOLE (subject-options-learning-rate options)) "W")
                (t (error "Unknown learning rate in batch test")))
          (if (subject-options-familiarisation-rate options) "Y" "N")
          (if (subject-options-learn-during-test options) "Y" "N")
          (get-eye-strategy-letter (subject-options-eye-strategy options))
          (subject-options-stm-size options)
          (subject-options-fov-size options)))


