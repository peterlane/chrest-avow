(in-package :chrest-avow)

;;;; class for chrest-subjects

;;;; the class definition itself is in file CHREST+.lisp

;;;; this file includes constructor and accessor methods for all usable variables

(defun create-new-chrest-subject (observed-object &optional (new-name ()) (new-ltm ()))
  (let ((new-subject 
         (make-instance 'chrest-subject
           'i-observe observed-object
           'my-name new-name
           'my-LTM new-LTM
           'current-circuit (get-first-example observed-object))))
    (setf (get-stm new-subject) (new-stm-queue new-subject))
    new-subject))

(defmethod create-new-chrest ((a-obs learner-observer) options name)
  (assert (subject-options-p options))
  (let ((new-subject
         (make-instance 'chrest-subject
           'i-observe a-obs 'my-LTM (create-new-network-root)
           'current-circuit (get-first-example a-obs))))
    (setf (train-mode new-subject) (subject-options-training-mode options))
    (setf (learn-rate new-subject) (subject-options-learning-rate options))
    (setf (eye-strategy new-subject) (subject-options-eye-strategy options))
    (setf (familiarise-after-discrimination-p new-subject) (subject-options-familiarisation-rate options))
    (setf (test-on-learning-p new-subject) (subject-options-test-before-learning options))
    (setf (learn-whilst-testing-p new-subject) (subject-options-learn-during-test options))
    (setf (my-focal-size new-subject) (subject-options-fov-size options))
    (setf (get-stm new-subject) (new-stm-queue new-subject (subject-options-stm-size options)))
    (set-name new-subject name)
    (add-to-current-subject-list a-obs new-subject)
    new-subject))

; access the subject's name

(defmethod set-name ((self chrest-subject) new-name)
  (setf (name self) new-name))

; access the subject's STM
(defmethod get-stm-patterns ((a-subject chrest-subject))
  (get-stm-patterns (get-STM a-subject)))

(defmethod add-to-stm ((a-subject chrest-subject) (a-pat pattern))
  "Put any non-empty pattern into the STM, and update stm observers."
  (let* ((recognised-node (active-node a-subject))
         (matched-pattern 
          (if (or (is-root-node recognised-node) (null-p (get-node-image recognised-node)))
            a-pat
            (get-my-match (get-node-image recognised-node) 
                          (focused-pattern a-subject)))))
    (when (non-null-p matched-pattern)
      (cond ((and (or (is-root-node recognised-node) (is-larger-p a-pat (get-node-image recognised-node)))
                  (learn-mode-p a-subject)) ; check for primitive learning
             (learn (if (equal-patterns-exactly-p a-pat (viewed-object a-subject))
                      (add-end a-pat)
                      a-pat)
                    (get-LTM a-subject) a-subject)  ; add-end causes a problem in single-relation learning
             (notify-observers-of-ltm-change a-subject))
            (T ; else, place in STM for learning there
             (add-to-STM (get-stm a-subject) (make-stm-item :pattern matched-pattern :node recognised-node)))))
    (notify-observers-of-stm-change a-subject)))

(defmethod clear-chrest-stm ((a-subject chrest-subject))
  (clear-STM (get-STM a-subject))
  (notify-observers-of-stm-change a-subject))

(defmethod seen-before-p ((self chrest-subject) (a-pat pattern))
  (exact-matching-patterns-p a-pat (get-stored-pattern self)))

; change-STM-size is called from property-modifier, which assures 0<new-size<=20
(defmethod change-stm-size ((a-subject chrest-subject) new-size)
  (assert (and (> new-size 0) (<= new-size 20)))
  (setf (get-STM a-subject) (new-stm-queue a-subject new-size))
  (notify-observers-of-stm-change a-subject))

; change the subject's focal size
; Change-focal-size is called from property-modifier, which ensures 0<new-size<=200
(defmethod change-focal-size ((a-subject chrest-subject) new-size)
  (assert (and (> new-size 0) (<= new-size 200)))
  (reset-fixation-history a-subject)
  (setf (my-focal-size a-subject) new-size)
  (notify-observers-of-environment-change a-subject))

; access the subject's LTM
(defmethod set-chrest-ltm ((self chrest-subject) new-ltm)
  (setf (get-LTM self) new-LTM))

(defmethod clear-chrest-ltm ((a-subject chrest-subject))
  (setf (get-LTM a-subject) (create-new-network-root))
  (notify-observers-of-clear-ltm-change a-subject))

; access the subject's viewed-pattern

(defmethod make-viewed-object ((a-subject chrest-subject) (a-pattern pattern) a-name)
  (setf (viewed-object a-subject) a-pattern)
  (setf (viewed-object-name a-subject) a-name))

(defmethod get-viewed-object-pattern ((a-subject chrest-subject))
  (viewed-object a-subject))



; access the subject's focused-pattern

(defmethod get-chrest-focused-pattern ((a-subject chrest-subject))
  (focused-pattern a-subject))

(defmethod set-chrest-focused-pattern ((a-subject chrest-subject) (new-pat pattern))
  (setf (focused-pattern a-subject) new-pat))

(defmethod add-to-chrest-focused-pattern ((a-subject chrest-subject) (additional-pat pattern))
  (set-chrest-focused-pattern a-subject (if (null-p (focused-pattern a-subject))
                                          additional-pat
                                          (augment-pattern (focused-pattern a-subject) additional-pat))))

(defmethod clear-chrest-focused-pattern ((a-subject chrest-subject))
  (setf (focused-pattern a-subject) (null-pattern)))

(defmethod clear-recognised-patterns ((a-subject chrest-subject))
  (setf (recognised-patterns a-subject) nil))

(defmethod add-to-recognised-patterns ((a-subject chrest-subject) (a-pat pattern))
  "Add the part of pattern which matches the current active node to recognised-patterns list"
  (let* ((recognised-node (active-node a-subject))
         (matched-pattern 
          (if (or (is-root-node recognised-node) (null-p (get-node-image recognised-node)))
            a-pat
            (get-my-match (get-node-image recognised-node) 
                          (focused-pattern a-subject))))
         (pattern-number (length (recognised-patterns a-subject))))
    (when (and (non-null-p matched-pattern) 
               (not (crossed-node-p (viewed-object a-subject) matched-pattern)))
      (push (list matched-pattern recognised-node pattern-number) (recognised-patterns a-subject)))))

(defstruct SOLVER problem solution top base)

(defvar *used-chunks* ()) ; preserve the used-chunks in a global variable

(defun display-used-chunk (a-chunk)
   "Display how the chunk is used in problem solving -- UNUSED"
   (format t "~&---------------------~&")
   (format t "  for pattern~&")
   (mapc #'describe-me (get-pattern (solver-problem a-chunk)))
   (format t "   top-most node: ~a~&" (solver-top a-chunk))
   (format t "   lowest node  : ~a~&" (solver-base a-chunk)))

; output-chunks provides means of solving a problem -> note, chunks are in form of solver structure
(defun parallel-schema-p (chunk-1 chunk-2)
   "Check if the parallel schema can be applied"
   (and (not (eq chunk-1 chunk-2)) ; same items do not match
         (equalp (solver-top chunk-1) (solver-top chunk-2))
         (equalp (solver-base chunk-1) (solver-base chunk-2))))

(defun serial-schema-p (chunk-1 chunk-2)
   "Check if the serial schema can be applied"
   (and (not (eq chunk-1 chunk-2)) ; same items do not match   
         (or (equalp (solver-top chunk-1) (solver-base chunk-2))
              (equalp (solver-base chunk-1) (solver-top chunk-2)))))

(defun matching-pair-of-chunks-p (chunk-1 chunk-2)
   "Considers if chunk-1 and chunk-2 match, i.e. if they align in a manner suitable for problem solving"
   (or (parallel-schema-p chunk-1 chunk-2)
        (serial-schema-p chunk-1 chunk-2)))

(defun combine-in-parallel (avow-pat-1 avow-pat-2)
   "Combine the two avow patterns in parallel - pat-1 to the left of pat-2 -> NEED TO PRESERVE LEFT-RIGHT ORDERING"
   (if (equal-patterns-p avow-pat-1 avow-pat-2)
      (error "Equal patterns")
      (let ((new-features (append (get-pattern avow-pat-1) (get-pattern avow-pat-2))))
         ; make sure the lines have been created
         (get-lines-for avow-pat-1) (get-lines-for avow-pat-2)
         ; add the new features for combining patterns in parallel
         (setf new-features (append new-features (list (create-reln-feature (get-top-of avow-pat-1)
                                                                               (get-top-of avow-pat-2))
                                                          (create-reln-feature (get-base-of avow-pat-1)
                                                                               (get-base-of avow-pat-2))
                                                          (create-reln-feature (get-right-of avow-pat-1)
                                                                               (get-left-of avow-pat-2)))))
         (create-new-avow-pattern (remove-if #'is-box-p new-features)))))

(defun combine-in-serial (avow-pat-1 avow-pat-2)
   "Combine the two avow patterns in serial - pat-1 above pat-2"
   (if (equal-patterns-p avow-pat-1 avow-pat-2)
      (error "Equal patterns")
      (let ((new-features (append (get-pattern avow-pat-1) (get-pattern avow-pat-2))))
         ; make sure the lines have been created
         (get-lines-for avow-pat-1) (get-lines-for avow-pat-2)
         ; add the new features for combining patterns in serial
         (setf new-features (append new-features (list (create-reln-feature (get-left-of avow-pat-1)
                                                                               (get-left-of avow-pat-2))
                                                          (create-reln-feature (get-right-of avow-pat-1)
                                                                               (get-right-of avow-pat-2))
                                                          (create-reln-feature (get-base-of avow-pat-1)
                                                                               (get-top-of avow-pat-2)))))
         (create-new-avow-pattern (remove-if #'is-box-p new-features)))))

(defun combine-chunks (chunk-1 chunk-2)
   "Combine the two chunks"
   (if (parallel-schema-p chunk-1 chunk-2) ; if using the parallel schema - assumes chunk-1 left of chunk-2
      (make-solver :problem (combine-patterns (list (solver-problem chunk-1) (solver-problem chunk-2)))
                   :solution (combine-in-parallel (solver-solution chunk-1) (solver-solution chunk-2))
                   :top (solver-top chunk-1)
                   :base (solver-base chunk-1))
      (if (equalp (solver-top chunk-1) (solver-base chunk-2)) ; chunk-1 below chunk-2
         (make-solver :problem (combine-patterns (list (solver-problem chunk-1) (solver-problem chunk-2)))
                      :solution (combine-in-serial (solver-solution chunk-2) (solver-solution chunk-1))
                      :top (solver-top chunk-2)
                      :base (solver-base chunk-1))
         (make-solver :problem (combine-patterns (list (solver-problem chunk-1) (solver-problem chunk-2)))
                      :solution (combine-in-serial (solver-solution chunk-1) (solver-solution chunk-2))
                      :top (solver-top chunk-1)
                      :base (solver-base chunk-2)))))

(defun merge-matching-chunks (chunks-left)
   "Assume that some pair of chunks pass the matching criteria - merge them and return the simplified list"
   ; test-p within inner function is used to enforce use of parallel-schema if it is applicable
   (labels ((process-two-lists (list-1 list-2 test-p)
               (cond ((null list-1) (process-two-lists chunks-left (rest list-2) test-p))
                      ((null list-2) chunks-left) ; this should not happen
                      ((apply test-p (first list-1) (list (first list-2)))
                       (cons (combine-chunks (first list-1) (first list-2))
                              (remove (first list-1) (remove (first list-2) chunks-left))))
                      (T (process-two-lists (rest list-1) list-2 test-p)))))
      (process-two-lists chunks-left chunks-left
                         (if (lists-contain-parallel-matching-chunks-p chunks-left chunks-left)
                            #'parallel-schema-p #'matching-pair-of-chunks-p))))

(defun contains-matching-chunks-p (chunks-left)
   "Checks whether the chunks-left list contains a pair of chunks which pass the matching criteria"
   (some #'(lambda (a-chunk) (member a-chunk chunks-left :test #'matching-pair-of-chunks-p))
          chunks-left))

(defun lists-contain-parallel-matching-chunks-p (chunks-left gathered-chunks)
   "Checks whether the chunks-left list contains any chunk which passes the parallel-schema criteria for any chunk in gathered-chunks"
   (intersection chunks-left gathered-chunks :test #'parallel-schema-p))

; amend this fn to return the matching and matched chunks, ready for combination <- ***********
(defun get-matching-chunk (chunks-left gathered-chunks)
   "Return two chunks, one from each list, which pass matching criteria"
   (let* ((test-to-use (if (lists-contain-parallel-matching-chunks-p chunks-left gathered-chunks)
                          #'parallel-schema-p #'matching-pair-of-chunks-p))
           (matching (first (intersection chunks-left gathered-chunks :test test-to-use)))
           (matched (first (remove-if-not #'(lambda (a-chunk) (apply test-to-use matching (list a-chunk)))
                                             gathered-chunks))))
      (values matching matched)))

(defun matching-chunk-p (chunks-left gathered-chunks)
   "Checks whether the chunks-left list contains any chunk which passes the matching criteria for any chunk in gathered-chunks"
   (remove-if #'(lambda (a-chunk) (member a-chunk chunks-left :test #'parallel-schema-p))
                (intersection chunks-left gathered-chunks :test #'matching-pair-of-chunks-p)))

(defun output-chunks (chunks-left gathered-chunks)
   "Recursively processes chunks-left adding groups to gathered-chunks when they pass matching criteria"
   (cond ((null chunks-left) ; none left to consider
           (reverse gathered-chunks))
          ((null gathered-chunks) ; get process started by selecting first chunk at random <- STRATEGY?
           (pushnew (first chunks-left) *used-chunks*)
           (output-chunks (rest chunks-left) (list (first chunks-left))))
          ((matching-chunk-p chunks-left gathered-chunks) ; look if any chunk can be added to those gathered
           ; here is where a chunk should be formed from the matching and matched parts of the gathered chunks
           (multiple-value-bind (matching matched) (get-matching-chunk chunks-left gathered-chunks)
              (pushnew (combine-chunks matching matched) *used-chunks*)
              (output-chunks (remove matching chunks-left) 
                             (cons (combine-chunks matching matched) (remove matched gathered-chunks)))))
          (T ; failed to add to gathered chunks, so look for matching groups in chunks-list <- MEMORY OVERLOAD?
           (if (contains-matching-chunks-p chunks-left) ; some chunks should match in remainder
              (output-chunks (merge-matching-chunks chunks-left) gathered-chunks)
              (progn ; (inspect (list chunks-left gathered-chunks))
                 (print "Sorry - could not find any matching chunks")
                 gathered-chunks)))))

(defun remove-super-groups (chunk-list)
   "Remove those chunks which are a super-group of the preceding list"
   (labels ((remove-groups (remainder done)
               (cond ((null remainder) done)
                      ((exact-matching-patterns-p (first remainder) (combine-patterns (rest remainder)))
                       (remove-groups (rest remainder) done))
                      (T
                       (remove-groups (rest remainder) (cons (first remainder) done))))))
      (remove-groups chunk-list ())))

(defmethod get-recognised-chunks ((a-subject chrest-subject))
   (let* ((gathered-chunks 
            (order-as-appeared
             (gather-chunks (get-sorted-recognised-chunks a-subject) () (null-pattern))))
           ; create the list of chunks for solving - instantiate the solution chunks for the given problem labelling           (chunks (mapcar #'(lambda (a-chunk)
           (chunks (mapcar #'(lambda (a-chunk)
                                 (make-solver :problem (first a-chunk)
                                              :solution (relabel-avow (node-image (node-equivalence (second a-chunk)))
                                                                      (get-circuit-relabelling (first a-chunk) 
                                                                                               (node-image (second a-chunk))))
                                              :top (get-topmost-node (get-recognised-pattern a-chunk))
                                              :base (get-lowest-node (get-recognised-pattern a-chunk))))
                            gathered-chunks)))
      
      (setf *used-chunks* ()) ; reset the used-chunks variable
      ; run through, solving each in turn - create an AVOW solution sequence for display and saving to CHEAT
      ; (let ((solution (output-chunks chunks ())))
      (output-chunks chunks ())
      (push (mapcar #'(lambda (a-pattern) (create-new-circuit "circuit chunk" a-pattern))
                      (mapcar #'solver-problem *used-chunks*))
             (my-solutions a-subject))
      ;        (create-multiple-pattern-display
      ;         (cons (create-new-circuit "Final circuit" (solver-problem (first solution)))
      ;               (mapcar #'(lambda (a-pattern) (create-new-circuit "circuit chunk" a-pattern))
      ;                        (mapcar #'solver-problem *used-chunks*)))
      ;         "Solution of circuit diagram as circuits"
      ;         (i-observe a-subject))
      ;        (create-multiple-pattern-display
      ;         (cons (create-new-circuit "Final circuit" (solver-problem (first solution)))
      ;               (mapcar #'(lambda (a-pattern) (create-new-circuit "computed chunk" (circuit-avow a-pattern)))
      ;                        (mapcar #'solver-problem *used-chunks*)))
      ;         "Solution of circuit diagram from circuits"
      ;         (i-observe a-subject))
      ;        (create-multiple-pattern-display
      ;         (cons (create-new-circuit "Final pattern" (solver-solution (first solution)))
      ;               (mapcar #'(lambda (a-pattern) (create-new-circuit "Avow chunk" a-pattern))
      ;                        (mapcar #'solver-solution *used-chunks*)))
      ;         "Solution of circuit diagram as AVOWs"
      ;         (i-observe a-subject)))
      ; (extract-chunks-from-list gathered-chunks) ; the original output from this routine
      (reverse (remove-super-groups (mapcar #'solver-problem *used-chunks*)))
      ))

(defmethod get-sorted-recognised-chunks ((a-subject chrest-subject))
  (sort (copy-list (recognised-patterns a-subject)) #'larger-chunk-p))

(defmethod gather-chunks ((potential-chunks list) (selected-chunks list) (aggregated-pattern pattern))
  (cond ((endp potential-chunks) selected-chunks)
        ((overlapping-patterns-p (get-recognised-pattern (first potential-chunks)) aggregated-pattern)
         (gather-chunks (rest potential-chunks) selected-chunks aggregated-pattern))
        (T
         (gather-chunks (rest potential-chunks) (append selected-chunks (list (first potential-chunks)))
                        (augment-pattern 
                         aggregated-pattern
                         (get-recognised-pattern (first potential-chunks)))))))

(defmethod get-recognised-chunks-pattern ((a-subject chrest-subject))
  "Return the composite pattern formed from the recognised chunks"
  (combine-patterns (extract-chunks-from-list (get-sorted-recognised-chunks a-subject))))

(defmethod extract-chunks-from-list ((chunk-list list))
  (mapcar #'get-recognised-pattern chunk-list))

(defmethod order-as-appeared ((chunk-list list))
  (sort chunk-list #'appeared-earlier-p))

(defmethod get-recognised-pattern ((a-pat list)) (first a-pat))

(defmethod get-chunk-number ((a-pat list))
  (if (> (length a-pat) 2) (third a-pat) 0))

(defmethod larger-chunk-p ((pat-1 list) (pat-2 list))
  (is-larger-p (get-recognised-pattern pat-1) (get-recognised-pattern pat-2)))

(defmethod appeared-earlier-p ((pat-1 list) (pat-2 list))
  (< (get-chunk-number pat-1) (get-chunk-number pat-2)))

; access the subject's current-environment

(defmethod current-circuit-list ((self chrest-subject))
  (current-circuit-list (i-observe self)))

(defmethod get-circuit-number ((self chrest-subject))
  (if (member (current-circuit self) (current-circuit-list self) 
              :test #'(lambda (x y) (equal (name x) (name y))))
    (+ 1 (position (current-circuit self) (current-circuit-list self)
                   :test #'(lambda (x y) (equal (name x) (name y)))))
    1))

(defmethod set-current-circuit ((a-subject chrest-subject) new-circuit)
  (setf (current-circuit a-subject) new-circuit)
  (notify-observers-of-environment-change a-subject))

(defmethod clear-current-circuit ((a-subject chrest-subject))
  (setf (current-circuit a-subject) (get-first-example (i-observe a-subject)))
  (notify-observers-of-environment-change a-subject))

; alter or query the learn-recall-mode slot of chrest-subject
(defmethod set-learn-mode ((a-subject chrest-subject))
  (setf (learn-recall-mode a-subject) 'LEARN))

(defmethod set-recall-mode ((a-subject chrest-subject))
  (setf (learn-recall-mode a-subject) 'RECALL))

(defmethod learn-mode-p ((a-subject chrest-subject))
  (equal (learn-recall-mode a-subject) 'LEARN))

(defmethod recall-mode-p ((a-subject chrest-subject))
  (equal (learn-recall-mode a-subject) 'RECALL))

;; manage the history lists

; training-history is a list of each training problem, 
; including information on the pre and succ state of the network

(defmethod reset-training-history ((a-subject chrest-subject))
  (setf (my-training-history a-subject) ())
  (notify-observers-of-training-history-change a-subject))

(defmethod get-training-history ((self chrest-subject))
  (reverse (my-training-history self)))

(defmethod add-to-training-history ((self chrest-subject) new-message)
  (push new-message (my-training-history self)))

(defmethod replace-last-training-history ((self chrest-subject) new-message)
  (setf (my-training-history self) (cons new-message (rest (my-training-history self)))))

(defmethod update-training-history ((a-subject chrest-subject) (a-circuit circuit))
  (if (same-as-last-training-history-p a-subject a-circuit)
    (replace-last-training-history a-subject (create-training-history a-subject a-circuit))
    (add-to-training-history a-subject (create-training-history a-subject a-circuit)))
  (notify-observers-of-training-history-change a-subject))

(defmethod create-training-history ((a-subject chrest-subject) (a-circuit circuit))
  (make-training-history
   :circuit a-circuit
   :total-nodes (count-nodes (get-LTM a-subject))
   :total-circuit (count-same-type (get-LTM a-subject) (null-circuit-pattern))
   :total-avow (count-same-type (get-LTM a-subject) (null-avow-pattern))
   :total-equivalence (count-equivalence-links (get-LTM a-subject))))

(defmethod same-as-last-training-history-p ((a-subject chrest-subject) (a-circuit circuit))
   (if (my-training-history a-subject)
     (eq a-circuit (training-history-circuit (first (my-training-history a-subject))))
     T))

(defmethod get-num-nodes-list ((self chrest-subject))
  (mapcar #'training-history-total-nodes (get-training-history self)))

(defmethod get-num-circuit-nodes-list ((self chrest-subject))
  (mapcar #'training-history-total-circuit (get-training-history self)))

(defmethod get-num-avow-nodes-list ((self chrest-subject))
  (mapcar #'training-history-total-avow (get-training-history self)))

(defmethod get-num-equivalence-links-list ((self chrest-subject))
  (mapcar #'training-history-total-equivalence (get-training-history self)))

;; structure to hold the training history record - this is currently specialised to circuit-avow domain
(defstruct TRAINING-HISTORY circuit total-nodes total-circuit total-avow total-equivalence)

(defun write-training-history (a-training-history)
   "Convert a history structure into a writable list"
   (list (write-object (training-history-circuit a-training-history))
          (training-history-total-nodes a-training-history)
          (training-history-total-circuit a-training-history)
          (training-history-total-avow a-training-history)
          (training-history-total-equivalence a-training-history)))

(defmethod read-training-history ((a-list list))
   "Convert a list into a history structure"
   (assert (= 5 (length a-list)))
   (make-training-history :circuit (read-circuit-object (first a-list))
                          :total-nodes (second a-list)
                          :total-circuit (third a-list)
                          :total-avow (fourth a-list)
                          :total-equivalence (fifth a-list)))

; testing-history is a list of each test problem, 
; including information on the chunks used by the subject in its solution

(defmethod reset-testing-history ((a-subject chrest-subject))
  (setf (my-testing-history a-subject) ())
  (notify-observers-of-test-history-change a-subject)
  (notify-observers-of-test-record-change a-subject))

(defmethod get-testing-history ((a-subject chrest-subject))
  (reverse (my-testing-history a-subject)))

(defmethod get-tested-circuit-list ((a-subject chrest-subject))
  (mapcar #'problem-solved (get-testing-history a-subject)))

(defmethod get-tested-circuit-chunk-list ((a-subject chrest-subject))
  (mapcar #'chunks-used (get-testing-history a-subject)))

(defmethod add-to-testing-history ((a-subject chrest-subject) new-message)
  (push new-message (my-testing-history a-subject))
  (notify-observers-of-test-record-change a-subject))

(defmethod update-test-history ((a-subject chrest-subject) a-solution)
   "Add to test history -- may want to avoid repeated tests for same problem"
   ;  (unless (same-as-last-test-history-p a-subject a-solution)
   (add-to-testing-history a-subject a-solution)
   (notify-observers-of-test-history-change a-subject)) ; )

(defmethod same-as-last-test-history-p ((a-subject chrest-subject) a-solution)
  (eq (first a-solution) (first (first (my-testing-history a-subject)))))

(defmethod get-max-testing-chunks ((a-subject chrest-subject))
  (loop for test in (my-testing-history a-subject)
        maximize (length (chunks-used test))))

(defmethod get-test-record-list ((a-subject chrest-subject))
  (mapcar #'name (get-tested-circuit-list a-subject)))

(defmethod problem-solved (test) (first test))

(defmethod chunks-used (test) (second test))

(defmethod num-chunks-used (test)
  (length (chunks-used test)))

(defmethod get-num-chunks-list ((self chrest-subject))
  (mapcar #'num-chunks-used (get-testing-history self)))

;; read/save of testing-history
(defun write-testing-history (a-testing-history)
   "Convert a test structure into a writable list"
   (list (write-object (first a-testing-history))
          (mapcar #'write-object (second a-testing-history))))

(defmethod read-testing-history ((a-list list))
   "Convert a list into a test structure"
   (assert (= 2 (length a-list)))
   (list (read-circuit-object (first a-list))
          (mapcar #'read-a-pattern (second a-list))))

; fixation-record records the set of eye fixations made by the model over entire lifespan
; record is (list (viewed-object) (list of fixations))

(defmethod reset-fixation-record ((a-subject chrest-subject))
  (setf (my-fixation-record a-subject) ())
  (notify-observers-of-fixation-record-change a-subject))

(defmethod get-fixation-record ((a-subject chrest-subject))
  (reverse (my-fixation-record a-subject)))

(defmethod get-complete-fixation-record ((a-subject chrest-subject))
  (if (my-fixation-history a-subject)
    (append (get-fixation-record a-subject)
                          (list (get-current-fixation-record a-subject)))
    (get-fixation-record a-subject)))

(defmethod get-fixation-record-name ((a-subject chrest-subject) pattern-number)
  (fixation-record-name (nth pattern-number (get-complete-fixation-record a-subject))))

(defmethod get-fixation-record-focal-size ((a-subject chrest-subject) pattern-number)
  (fixation-record-focal-size (nth pattern-number (get-complete-fixation-record a-subject))))

(defmethod get-fixation-record-pattern ((a-subject chrest-subject) pattern-number)
  (fixation-record-pattern (nth pattern-number (get-complete-fixation-record a-subject))))

(defmethod get-fixation-record-fixations ((a-subject chrest-subject) pattern-number)
  (fixation-record-fixation-list (nth pattern-number (get-complete-fixation-record a-subject))))

(defmethod add-to-fixation-record ((a-subject chrest-subject) new-fixations)
  (push new-fixations (my-fixation-record a-subject))
  (notify-observers-of-fixation-record-change a-subject))

(defmethod get-fixation-record-list ((a-subject chrest-subject))
  (mapcar #'fixation-record-name (get-complete-fixation-record a-subject)))

;; a simple structure to hold the fixation record for a given pattern
(defstruct FIXATION-RECORD name pattern fixation-list focal-size)

(defun write-fixation-record (a-fixation-record)
   "Convert a fixation record into a writable list"
   (list (fixation-record-name a-fixation-record)
          (write-object (fixation-record-pattern a-fixation-record))
          (mapcar #'write-object (fixation-record-fixation-list a-fixation-record))
          (fixation-record-focal-size a-fixation-record)))

(defmethod read-fixation-record ((a-list list))
   "Convert a list into a fixation record"
   (assert (= 4 (length a-list)))
   (make-fixation-record :name (first a-list)
                         :pattern (read-a-pattern (second a-list))
                         :fixation-list (mapcar #'read-fixation (third a-list))
                         :focal-size (fourth a-list)))

; fixation-history records the set of eye fixations made by the model on current scene

(defmethod reset-fixation-history ((a-subject chrest-subject))
  (when (my-fixation-history a-subject)
    (add-to-fixation-record a-subject (get-current-fixation-record a-subject)))
  (setf (my-fixation-history a-subject) ())
  (notify-observers-of-fixation-history-change a-subject)
  (notify-observers-of-fixation-record-change a-subject))

(defmethod reset-fixation-history-quietly ((a-subject chrest-subject))
  (when (my-fixation-history a-subject)
    (add-to-fixation-record a-subject (get-current-fixation-record a-subject)))
  (setf (my-fixation-history a-subject) ()))

(defmethod get-fixation-history ((self chrest-subject))
  (reverse (my-fixation-history self)))

(defmethod get-current-fixation-record ((a-subject chrest-subject))
   (make-fixation-record
    :name (viewed-object-name a-subject)
    :pattern (viewed-object a-subject)
    :fixation-list (get-fixation-history a-subject)
    :focal-size (my-focal-size a-subject)))

(defmethod get-fixation-point (a-fixation)
  (first a-fixation))

(defmethod add-to-fixation-history ((a-subject chrest-subject) new-fixation)
  (push new-fixation (my-fixation-history a-subject))
  (notify-observers-of-fixation-history-change a-subject))

(defmethod get-last-saccade ((self chrest-subject))
  "Return distance between last two eye fixations, or '(0 0) if not enough previous fixations."
  (if (> (length (get-fixation-history self)) 1)
    (subtract-points (get-last-fixation self) (get-penultimate-fixation self))
    (make-point 0 0)))

(defmethod get-last-fixation ((self chrest-subject))
  "Returns the last fixation made"
  (get-fixation-point (first (my-fixation-history self))))

(defmethod get-penultimate-fixation ((self chrest-subject))
  "Returns the penultimate fixation made"
  (get-fixation-point (second (my-fixation-history self))))

;; manage list of learning operations taken by subject
;; list is a list of pairs of familiarisations/discriminations
;; each operation increments one or other of these operations

;; use a simple structure to retain the counts of learning operations
(defstruct LEARNING-OPS (familiarisations 0) (discriminations 0))


(defmethod get-familiarisation-list ((a-subject chrest-subject))
   (reverse (mapcar #'learning-ops-familiarisations (learning-operations a-subject))))

(defmethod get-discrimination-list ((a-subject chrest-subject))
   (reverse (mapcar #'learning-ops-discriminations (learning-operations a-subject))))

(defmethod add-to-familiarisation-list ((a-subject chrest-subject))
   (add-to-learning-operations a-subject #'incr-familiarisation-count))

(defmethod add-to-discrimination-list ((a-subject chrest-subject))
   (add-to-learning-operations a-subject #'incr-discrimination-count))

(defmethod add-to-learning-operations ((a-subject chrest-subject) update-fn)
  (if (learning-operations a-subject)
     (push (apply update-fn (first (learning-operations a-subject)) ())
            (learning-operations a-subject))
     (push (make-learning-ops)
            (learning-operations a-subject)))
  (notify-observers-of-learning-operations-change a-subject))

(defmethod clear-learning-operations-list ((a-subject chrest-subject))
   (setf (learning-operations a-subject) ())
   (notify-observers-of-learning-operations-change a-subject))

(defun write-learning-op (a-learning-op)
   "Convert a learning op into a list"
   (list (learning-ops-familiarisations a-learning-op) 
          (learning-ops-discriminations a-learning-op)))

(defun read-learning-op (a-pair)
   "Convert a pair of integers into a learning-op"
   (assert (and (= 2 (length a-pair)) (numberp (first a-pair)) (numberp (second a-pair))))
   (make-learning-ops :familiarisations (first a-pair)
                      :discriminations (second a-pair)))

; create a new learning operation but adding one to familiarisation count
(defun incr-familiarisation-count (a-pair)
   (assert (learning-ops-p a-pair))
   (make-learning-ops
    :familiarisations (+ 1 (learning-ops-familiarisations a-pair))
    :discriminations (learning-ops-discriminations a-pair)))

(defun incr-discrimination-count (a-pair)
    (assert (learning-ops-p a-pair))
    (make-learning-ops
     :familiarisations (learning-ops-familiarisations a-pair)
     :discriminations (+ 1 (learning-ops-discriminations a-pair))))


;;; control the learning 
(defmethod train-chrest ((self learner-observer) &optional (rest ()))
  (declare (ignore rest))
  (when (current-subject self)
    (train-current-subject (current-subject self) (current-circuit self))))

(defmethod train-chrest ((a-subject chrest-subject) &optional (rest ()))
  (declare (ignore rest))
  (if (train-mode-linked-p (train-mode a-subject))
    (train-fully-current-subject a-subject (current-circuit a-subject))
    (train-current-subject a-subject (current-circuit a-subject))))

(defmethod train-fully ((self learner-observer) &optional (rest ()))
  (declare (ignore rest))
  (when (current-subject self)
    (train-fully-current-subject (current-subject self) (current-circuit self))))

(defmethod train-current-subject ((a-subject chrest-subject) (a-course curriculum))
  (dolist (a-circuit (curriculum a-course))
     (train-current-subject a-subject a-circuit)))

(defmethod train-fully-current-subject ((a-subject chrest-subject) (a-course curriculum))
   (dolist (a-circuit (curriculum a-course))
      (train-fully-current-subject a-subject a-circuit)))

(defmethod train-fully-current-subject ((a-subject chrest-subject) (a-circuit circuit))
  (if (fully-learnt-circuit-pair-p a-subject a-circuit)
    (progn
      (update-training-history a-subject a-circuit)
      (when (test-on-learning-p a-subject)
        (update-test-history a-subject (list a-circuit (test-current-subject-result a-subject a-circuit)))))
    (loop as safety-net below 10 ; 10 cycles per circuit
          until (fully-learnt-circuit-pair-p a-subject a-circuit)
          do (train-current-subject a-subject a-circuit))))

; train-current-subject does the actual training, and notifies observers
(defmethod train-current-subject ((a-subject chrest-subject) (a-circuit circuit))
  (when (test-on-learning-p a-subject)
    (update-test-history a-subject (list a-circuit (test-current-subject-result a-subject a-circuit))))
  (scan-and-learn a-subject (circuit a-circuit) (name a-circuit))
  (scan-and-learn a-subject (circuit-avow (circuit a-circuit)) (format nil "~a - solution" (name a-circuit)))
  (when (fully-learnt-circuit-pair-p a-subject a-circuit)
    (create-equivalence-link (get-LTM a-subject) (circuit a-circuit)
                             (circuit-avow (circuit a-circuit))))
  (update-training-history a-subject a-circuit)
  (notify-observers-of-ltm-change a-subject))

(defmethod train-subject-circuit-only ((a-subject chrest-subject) (a-circuit circuit))
  (scan-and-learn a-subject (circuit a-circuit) (name a-circuit))
  (update-training-history a-subject a-circuit)
  (notify-observers-of-ltm-change a-subject))

(defmethod fully-learnt-p ((a-subject chrest-subject) (a-course curriculum))
  (loop for a-circuit in (curriculum a-course)
        always (fully-learnt-circuit-pair-p a-subject a-circuit)))

(defmethod fully-learnt-p ((a-subject chrest-subject) (a-circuit circuit))
  (let ((retrieved-image 
         (get-node-image (recognise-pattern (circuit a-circuit)
                                            (get-LTM a-subject)))))
  (and (has-end-p retrieved-image)
       (equal-patterns-p (circuit a-circuit) retrieved-image))))

(defmethod fully-learnt-p ((a-subject chrest-subject) (an-avow-pattern avow-pattern))
  (let ((retrieved-image 
         (get-node-image (recognise-pattern an-avow-pattern
                                            (get-LTM a-subject)))))
  (and (has-end-p retrieved-image)
       (equal-patterns-p an-avow-pattern retrieved-image))))

(defmethod fully-learnt-circuit-pair-p ((a-subject chrest-subject) (a-circuit circuit))
  (and (fully-learnt-p a-subject a-circuit)
       (fully-learnt-p a-subject (circuit-avow (circuit a-circuit)))))


;;; control how CHREST gets tested
(defmethod test-chrest ((an-obs learner-observer) &optional (rest ()))
  (declare (ignore rest))
  (when (current-subject an-obs)
    (test-current-subject (current-subject an-obs) (current-circuit an-obs))
    (notify-observers-of-test-history-change (current-subject an-obs))))

(defmethod test-chrest ((a-subject chrest-subject) &optional (rest ()))
  (declare (ignore rest))
  (test-current-subject a-subject (current-circuit a-subject))
  (notify-observers-of-test-history-change a-subject))

(defmethod test-current-subject ((a-subject chrest-subject) (a-course curriculum))
   (dolist (a-circuit (curriculum a-course))
     (test-current-subject-result a-subject a-circuit)))

(defmethod test-current-subject ((a-subject chrest-subject) (a-circuit circuit))
  (test-current-subject-result a-subject a-circuit)) ; just do test without automatically displaying result

(defmethod test-current-subject-result ((self chrest-subject) (a-circuit circuit))
  (let ((solution (decompose-given-pattern self (circuit a-circuit) 
                                           (format nil "~a - test" (name a-circuit)))))
    (update-test-history self (list a-circuit solution))
    (when (learn-whilst-testing-p self)
      (train-subject-circuit-only self a-circuit))
    solution))

;; create a simple structure for the tested-pattern lists
(defstruct TESTED-PATTERN name circuit chunks (subjects nil) (total 0))


(defmethod decompose-given-pattern ((a-subject chrest-subject) (a-pattern pattern) a-name)
  "Decomposes pattern in accordance with eye strategy, i.e. scans if a strategy is given"
  (clear-recognised-patterns a-subject)
  (unless (learn-whilst-testing-p a-subject)
     (set-recall-mode a-subject))
  (prog1  ; return decomposed pattern
     (if (eye-strategy-none-p a-subject)
        (decompose-pattern a-pattern (get-LTM a-subject))
        (prog2
         (scan-given-scene a-subject a-pattern a-name)
         (get-recognised-chunks a-subject)))
     (set-learn-mode a-subject)))

(defmethod scan-and-learn ((a-subject chrest-subject) (a-pattern pattern) a-name)
  "Learns the given pattern in accordance with eye strategy, i.e. scans if a strategy is given"
  (set-learn-mode a-subject)
  (if (eye-strategy-none-p a-subject)
    (learn a-pattern (get-LTM a-subject) a-subject)
    (scan-given-scene a-subject a-pattern a-name)))



