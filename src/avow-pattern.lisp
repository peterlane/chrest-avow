(in-package :chrest-avow)

;; avow-pattern

;; Peter Lane, 22nd February 2001

;; provide a pattern class for identifying and displaying AVOW diagrams


;; ------------ class definitions and methods ---------------

(defclass AVOW-FEATURE (feature) ())

(defclass AVOW-PATTERN (pattern) 
  ((my-lines :accessor my-lines :initform nil)
   (my-vertical-relation-list :accessor vertical-relation-list :initform nil)
   (my-horizontal-relation-list :accessor horizontal-relation-list :initform nil)
   (my-box-dimensions :accessor box-dimensions :initform nil)))


(defmethod avow-pattern-p ((a-pat avow-pattern)) T)
(defmethod avow-pattern-p ((a-pat t)) ())

;; new constructors for this class

(defun create-new-avow-pattern (&optional (new-pattern ()))
   (make-instance 'avow-pattern :pattern (alexandria:ensure-list new-pattern)))

(defmethod create-new-pattern-for ((a-pat avow-pattern) &optional (new-pattern ()))
  (declare (ignore a-pat))
  (create-new-avow-pattern new-pattern))

(defmethod create-new-pattern-for ((a-feat avow-feature) &optional (new-pattern ()))
   "Also, may use a feature as a prime for the target pattern"
   (declare (ignore a-feat))
   (create-new-avow-pattern new-pattern))

(defmethod new-null-for ((a-pat avow-pattern))
  (declare (ignore a-pat))
  (null-avow-pattern))

(defun null-avow-pattern () (create-new-avow-pattern))

;; handle the relabelling of an avow feature
(defun mapping-exists-p (a-label a-mapping)
   (member a-label a-mapping :key #'first))

(defun get-mapped-label (a-label a-mapping)
   "Should only be called if mapping-exists-p returned true"
   (second (first (mapping-exists-p a-label a-mapping))))

(defun make-replacement (a-label a-mapping)
   (if (mapping-exists-p a-label a-mapping)
      (get-mapped-label a-label a-mapping)
      a-label))

(defun make-side-replacement (a-side a-mapping)
   (let ((new-name (make-replacement (box-side-name a-side) a-mapping)))
      (make-box-side :name new-name :type (box-side-type a-side))))

(defmethod relabel-avow-feature ((a-feature avow-feature) a-mapping)
   (let ((a-feat (feature a-feature)))
      ; exhaust the three types of avow-feature in a conditional
      (cond ((box-p a-feat)
              (create-box-feature 
               (make-replacement (box-label a-feat) a-mapping)))
             ((eq-reln-p a-feat)
              (create-reln-feature
               (make-side-replacement (eq-reln-side-1 a-feat) a-mapping)
               (make-side-replacement (eq-reln-side-2 a-feat) a-mapping)))
             ((lt-reln-p a-feat)
              (create-less-than-feature
               (make-side-replacement (lt-reln-side-1 a-feat) a-mapping)
               (make-side-replacement (lt-reln-side-2 a-feat) a-mapping))))))

(defmethod relabel-avow ((a-pat avow-pattern) a-mapping)
   "Relabel each feature in avow pattern, and create a new pattern from the relabelled features"
   (create-new-avow-pattern
    (loop for a-feat in (get-pattern a-pat)
           collect (relabel-avow-feature a-feat a-mapping))))

;; comparison routines -- get-my-match, get-strict-match, size-of, matching-patterns-p
;;                        are all used as in standard pattern
(defmethod equal-features-p ((feat-a avow-feature) (feat-b avow-feature))
   "Test if two avow features are equal"
   (or (and (is-box-p feat-a) (is-box-p feat-b)
             (equal (get-box-label feat-a) (get-box-label feat-b)))
        (and (equal-type-p feat-a feat-b)
              (equal-sides-p (get-first-side feat-a) (get-first-side feat-b))
              (equal-sides-p (get-second-side feat-a) (get-second-side feat-b)))))

;; for learning whole objects
(defmethod get-all-object ((a-pattern avow-pattern))
   "Select an unused object at random and return all avow features in a-pattern relating to it."
   (labels ((get-object-names (a-pattern)
               "Extract the names of all objects in the given pattern"
               (loop for feature in (get-pattern a-pattern)
                      when (is-box-p feature) collect (get-box-label feature)
                      when (is-eq-reln-p feature) append (list (get-first-side-name feature)
                                                                (get-second-side-name feature))))
             (has-object-test (object-name)
               "Return test to check if given feature refers to the named object"
               (lambda (a-feature)
                 (member object-name (get-object-names (create-new-avow-pattern a-feature)))))
             (select-object-features (an-object)
               "Select all features in the pattern referring to the given object"
               (remove-if-not (has-object-test an-object) (get-pattern a-pattern))))          
      (select-object-features (alexandria:random-elt (get-object-names a-pattern)))))

;; for identification -- avow-pattern ignores visible-feature-p, 
;;                       instead specialising extract-features
(defmethod extract-features ((a-pattern avow-pattern) a-point radius)
  "Return a new avow pattern containing only the visible features
  Subtract-points should not be here - problem in consistency of pattern location across pattern types"
  (labels ((visible-box-p (a-box a-point radius)
              "Check if centre or any corner of given box is within radius of a-point"
              (some #'(lambda (a-pt) (points-adjacent-p a-pt radius a-point))
                     (list (centre-of a-box) (top-left a-box) (top-right a-box)
                            (bottom-left a-box) (bottom-right a-box))))
            (visible-boxes (a-point radius)
              "Return those points visible at current fixation point"
              (loop for a-box in (box-dimensions a-pattern)
                     when (visible-box-p a-box a-point radius) collect (box-name a-box)))
            (visible-features (visible-boxes)
              "Return those features in pattern which are visible, i.e. refer to visible boxes"
              (flet ((feature-in-visible-boxes-p (a-feature)
                        "Checks if all boxes in the feature are visible"
                        (every #'(lambda (a-box) (member a-box visible-boxes)) (get-boxes a-feature))))
                 (remove-if-not #'feature-in-visible-boxes-p (get-pattern a-pattern)))))
     (create-new-avow-pattern
      (visible-features (visible-boxes (subtract-points a-point (make-point 27 17)) radius)))))

;; read/write methods for avows

(defmethod write-object ((a-pattern avow-pattern))
   "Convert a pattern into a writable list"
   (list 'avow-pattern
          (mapcar #'write-object (get-pattern a-pattern))
          (has-end-p a-pattern)))

(defmethod write-object ((a-feature avow-feature))
   "Convert an avow-feature into a writable list"
   (list (point-h (location a-feature))
          (point-v (location a-feature))         
          (cond ((is-box-p a-feature) (list 'BOX (get-box-label a-feature)))
                 ((is-eq-reln-p a-feature) 
                  (list 'EQ 
                         (list (box-side-type (get-first-side a-feature))
                                (box-side-name (get-first-side a-feature)))
                         (list (box-side-type (get-second-side a-feature))
                                (box-side-name (get-second-side a-feature)))))
                 ((is-lt-reln-p a-feature) 
                  (list 'LT 
                         (list (box-side-type (get-first-side a-feature))
                                (box-side-name (get-first-side a-feature)))
                         (list (box-side-type (get-second-side a-feature))
                                (box-side-name (get-second-side a-feature)))))
                 (T (error "Invalid feature type")))))


(defmethod read-avow-pattern ((a-list list))
   "Convert a list back into an avow-pattern"
   (assert (= 2 (length a-list)))
   (let ((new-avow (make-instance 'avow-pattern
                      :pattern (mapcar #'read-avow-feature (first a-list))
                      :has-end-p (second a-list))))
      (get-lines-for new-avow)
      new-avow))

(defmethod read-avow-feature ((a-list list))
   "Convert a list back into an avow-feature"
   (assert (= 3 (length a-list)))
   (make-instance 'avow-feature
      :location (make-point (first a-list) (second a-list))
      :feature (create-avow-feature (third a-list))))

(defmethod create-avow-feature ((a-list list))
   "Take a list and create the avow feature"
   (cond ((equal 'BOX (first a-list)) 
           (make-box :label (second a-list)))
          ((equal 'EQ (first a-list)) 
           (make-eq-reln :side-1 (create-avow-side (second a-list))
                         :side-2 (create-avow-side (third a-list))))
          ((equal 'LT (first a-list)) 
           (make-lt-reln :side-1 (create-avow-side (second a-list))
                         :side-2 (create-avow-side (third a-list))))
          (T (error "Unknown feature type"))))

(defmethod create-avow-side ((a-list list))
   "Take a pair and create relevant side"
   (cond ((equal 'T (first a-list)) (top-of (second a-list)))
          ((equal 'B (first a-list)) (bottom-of (second a-list)))
          ((equal 'L (first a-list)) (left-of (second a-list)))
          ((equal 'R (first a-list)) (right-of (second a-list)))
          (T (error "Unknown side type"))))

;; ---------- methods and classes specific to operation of avow-pattern ---------------

; the avow-feature class has three possible forms:
; a. a single box,
; b. an equality relation between two side-definitions
; c. an inequality relation between two side-definitions 
;    - this latter does not occur in pattern definition, but is used in constructing the line drawing
(defun create-new-avow-feature (new-feature)
  (make-instance 'avow-feature :feature new-feature))

; for the box form
(defstruct BOX label)

(defmethod is-box-p ((a-feat avow-feature))
   (box-p (feature a-feat)))

(defmethod get-box-label ((a-feat avow-feature))
   (box-label (feature a-feat)))

(defmethod create-box-feature (a-label)
  (create-new-avow-feature (make-box :label a-label)))

; for the relation forms
(defstruct RELN side-1 side-2)
(defstruct (eq-reln (:include reln)))
(defstruct (lt-reln (:include reln)))

(defmethod create-reln-feature (side-1 side-2)
  (create-new-avow-feature (make-eq-reln :side-1 side-1 :side-2 side-2)))

(defmethod create-less-than-feature (side-1 side-2)
  (create-new-avow-feature (make-lt-reln :side-1 side-1 :side-2 side-2)))

; some tests and comparisons for the avow-features

(defmethod equal-type-p ((feat-a avow-feature) (feat-b avow-feature))
  (or (and (is-box-p feat-a) (is-box-p feat-b))
      (and (is-eq-reln-p feat-a) (is-eq-reln-p feat-b))
      (and (is-lt-reln-p feat-a) (is-lt-reln-p feat-b))))

(defmethod is-reln-p ((a-feature avow-feature))
   "Test whether feature is a reln feature"
   (reln-p (feature a-feature)))

(defmethod is-eq-reln-p ((a-feature avow-feature))
   "Test whether feature is an eq-reln feature"
   (eq-reln-p (feature a-feature)))

(defmethod is-lt-reln-p ((a-feature avow-feature))
   "Test whether feature is an lt-reln feature"
   (lt-reln-p (feature a-feature)))


; these are the side descriptors for the AVOW boxes
(defstruct BOX-SIDE name type)

(defmethod top-of (label) (make-box-side :name label :type 'T))
(defmethod bottom-of (label) (make-box-side :name label :type 'B))
(defmethod left-of (label) (make-box-side :name label :type 'L))
(defmethod right-of (label) (make-box-side :name label :type 'R))

(defmethod equal-sides-p (side-a side-b)
   "Check if the two sides are equal: sides are either numbers or box-side structures"
   (cond ((or (null side-a) (null side-b))
           ())
          ((and (numberp side-a) (numberp side-b))
           (= side-a side-b))
          ((and (box-side-p side-a) (box-side-p side-b))
           (and (equal (box-side-type side-a) (box-side-type side-b))
                 (equal (box-side-name side-a) (box-side-name side-b))))
          (T (error "Unknown side type"))))

; from a feature, retrieve the first or second side
; return null if given a box-feature by mistake
(defmethod get-first-side ((a-feature avow-feature))
   "Retrieve first side of reln-feature, or null if given a box-feature by mistake"
   (cond ((is-box-p a-feature) ())
          ((is-reln-p a-feature) (reln-side-1 (feature a-feature)))
          (T (error "Invalid feature type"))))

(defmethod get-second-side ((a-feat avow-feature))
   "Retrieve second side of reln-feature, or null if given a box-feature by mistake"
   (cond ((is-box-p a-feat) ())
          ((is-reln-p a-feat) (reln-side-2 (feature a-feat)))
          (T (error "Invalid feature type"))))

; one-step functions to get information about separate sides
(defmethod get-first-side-name ((a-feat avow-feature))  
  (box-side-name (get-first-side a-feat)))

(defmethod get-second-side-name ((a-feat avow-feature))
  (box-side-name (get-second-side a-feat)))


;;; following help out with the display of the avow pattern


(defun avow-scale (a-coord)
  "Scale the given coord"
  (* 20 a-coord))

(defmethod get-right-side ((a-pat avow-pattern))
  (loop for a-line in (get-lines-for a-pat)
        maximize (max (avow-line-x1 a-line) (avow-line-x2 a-line))))

(defmethod get-left-side ((a-pat avow-pattern))
  (loop for a-line in (get-lines-for a-pat)
        minimize (min (avow-line-x1 a-line) (avow-line-x2 a-line))))

(defmethod get-bottom-side ((a-pat avow-pattern))
  (loop for a-line in (get-lines-for a-pat)
        maximize (max (avow-line-y1 a-line) (avow-line-y2 a-line))))

(defmethod get-top-side ((a-pat avow-pattern))
  (loop for a-line in (get-lines-for a-pat)
        minimize (min (avow-line-y1 a-line) (avow-line-y2 a-line))))

(defmethod get-width ((a-pat avow-pattern))
   "Compute the width of the drawn pattern"
   (unless (my-lines a-pat)
      (get-lines-for a-pat))
   (if (null-p a-pat)
      10
      (+ 10 (avow-scale (- (get-right-side a-pat) (get-left-side a-pat))))))

(defmethod get-height ((a-pat avow-pattern))
   "Compute the height of the drawn pattern"
   (unless (my-lines a-pat)
      (get-lines-for a-pat))
   (if (null-p a-pat)
      10
      (+ 10 (avow-scale (- (get-bottom-side a-pat) (get-top-side a-pat))))))

;;; construction of line drawing for a given avow pattern

;; structure for lines
(defstruct AVOW-LINE "line drawn from (x1 y1) to (x2 y2)" x1 y1 x2 y2 label)


;; each AVOW pattern uses its list of equal relations to identify which 
;; elements of the drawn lines should have the same coordinates.  
;; The basic constraints on the AVOW box (left-right top-bottom ordering) 
;; serve to order the lines.

(defmethod construct-relations ((a-pattern avow-pattern))
   "Add the ordering relations into the horizontal/vertical relation lists"
   (labels ((relations-not-entered-p ()
               "Check if relation lists not yet entered"
               (or (endp (horizontal-relation-list a-pattern))
                    (endp (vertical-relation-list a-pattern))))
             (insert-basic-relations ()
               "The basic relations for the sides of each box are inserted into the pattern's appropriate relation list"
               (flet ((add-to-horizontal-relation-list (a-feature)
                         (pushnew a-feature (horizontal-relation-list a-pattern) :test #'equal-features-p))
                       (add-to-vertical-relation-list (a-feature)
                         (pushnew a-feature (vertical-relation-list a-pattern) :test #'equal-features-p)))
                  (dolist (box (get-boxes a-pattern))
                     (add-to-vertical-relation-list (create-less-than-feature (bottom-of box) (top-of box)))
                     (add-to-horizontal-relation-list (create-less-than-feature (left-of box) (right-of box))))))
             (include-equal-relations ()
               "Alter the side labels in relation lists to reflect the equality relations"
               (setf (horizontal-relation-list a-pattern) 
                      (normalise-features a-pattern (horizontal-relation-list a-pattern)))
               (setf (vertical-relation-list a-pattern) 
                      (normalise-features a-pattern (vertical-relation-list a-pattern)))))
      (when (relations-not-entered-p)
         (insert-basic-relations)
         (include-equal-relations))))

(defmethod normalise-sides ((a-pattern avow-pattern) (side-list list) applies-p apply-feature)
  "Uses the elements in avow-pattern to normalise the side descriptions in side-list,
      i.e. by making equal sides use the same name: this routine is generic to normalise lines and sides, due to import of applies-p and apply-feature"
  (labels ((propagate (done-items remaining-items rejected-items equal-list)
               "propagate works through the items until everything has been converted"
               (assert (and (listp done-items) (listp remaining-items) (listp rejected-items) 
                              (listp equal-list)))
               (flet ((equals-applies-to-item-p (an-item equal-list)
                         "Check whether some relation in equal-list applies to the item"
                         (flet ((applicable-p (test-item) (apply applies-p an-item (list test-item))))
                            (some #'applicable-p equal-list)))
                       (apply-equals-to-item (an-item equal-list)
                         "Make a single replacement of element in line or relation based on equal-list"
                         (loop for a-feat in equal-list
                                when (apply applies-p an-item (list a-feat)) 
                                return (apply apply-feature an-item (list a-feat))
                                finally (return an-item))))
                  (cond ((endp remaining-items) done-items)
                         ((and rejected-items
                                (member (first remaining-items) rejected-items :test #'equal-features-p))
                          (error "Cycle found in pattern definition.")) ; check for cycles
                         ((equals-applies-to-item-p (first remaining-items) equal-list)
                          (propagate done-items 
                                     (cons (apply-equals-to-item (first remaining-items) equal-list)
                                            (rest remaining-items))
                                     (cons (first remaining-items) rejected-items)
                                     equal-list))
                         (T (propagate (adjoin (first remaining-items) done-items
                                                :test #'equal-features-p)             
                                       (rest remaining-items) () equal-list))))))
     (propagate () side-list () (get-pattern a-pattern))))

(defmethod normalise-features ((a-pattern avow-pattern) (side-list list))
   "Uses elements in a-pattern to normalise (make standard) the described sides in side-list"
   (flet ((feature-applies-to-item-p (a-reln a-feature)
             "Check whether feature applies to the given relation"
             (or (equal-sides-p (get-first-side a-reln) (get-second-side a-feature))
                  (equal-sides-p (get-second-side a-reln) (get-second-side a-feature))))
           (apply-feature-to-item (a-reln a-feature)
             "Apply the feature to the given relation"
             (cond ((equal-sides-p (get-first-side a-reln) (get-second-side a-feature))
                     (create-less-than-feature (get-first-side a-feature) (get-second-side a-reln)))
                    ((equal-sides-p (get-second-side a-reln) (get-second-side a-feature))
                     (create-less-than-feature (get-first-side a-reln) (get-first-side a-feature)))
                    (T a-reln))))
      (normalise-sides a-pattern side-list #'feature-applies-to-item-p #'apply-feature-to-item)))

(defmethod normalise-lines ((a-pattern avow-pattern) (line-list list))
   "Uses elements in a-pattern to normalise (make-standard) the described lines"
   (flet ((applies-p (a-line a-feature)
             "Check whether an element of a-line is the second element of a-feature"
             (or (equal-sides-p (avow-line-x1 a-line) (get-second-side a-feature))
                  (equal-sides-p (avow-line-y1 a-line) (get-second-side a-feature))
                  (equal-sides-p (avow-line-x2 a-line) (get-second-side a-feature))
                  (equal-sides-p (avow-line-y2 a-line) (get-second-side a-feature))))
           (apply-feature (a-line a-feature)
             "Make the substitution"
             (let ((new-line (copy-avow-line a-line)))
                (cond ((equal-sides-p (avow-line-x1 a-line) (get-second-side a-feature))
                        (setf (avow-line-x1 new-line) (get-first-side a-feature)))
                       ((equal-sides-p (avow-line-y1 a-line) (get-second-side a-feature))
                        (setf (avow-line-y1 new-line) (get-first-side a-feature)))
                       ((equal-sides-p (avow-line-x2 a-line) (get-second-side a-feature))
                        (setf (avow-line-x2 new-line) (get-first-side a-feature)))
                       ((equal-sides-p (avow-line-y2 a-line) (get-second-side a-feature))
                        (setf (avow-line-y2 new-line) (get-first-side a-feature)))
                       (T ()))
                new-line)))
      (normalise-sides a-pattern line-list #'applies-p #'apply-feature)))


(defmethod get-lines-for ((a-pat avow-pattern))
   "Extract lines for boxes in the pattern, and assigns coordinates to each line"
   (labels ((convert-lines (line-list vertical-list horizontal-list)
               "Uses element's position in vertical/horizontal-list to provide its coordinate"
               (flet ((convert-line (a-line)
                         (flet ((get-coord (a-feat a-list)
                                   "Return the position of feature in list"
                                   (if (position a-feat a-list :test #'equal-sides-p)
                                      (position a-feat a-list :test #'equal-sides-p)
                                      a-feat)))
                            (make-avow-line :x1 (get-coord (avow-line-x1 a-line) horizontal-list)
                                            :y1 (get-coord (avow-line-y1 a-line) vertical-list)
                                            :x2 (get-coord (avow-line-x2 a-line) horizontal-list)
                                            :y2 (get-coord (avow-line-y2 a-line) vertical-list)
                                            :label (avow-line-label a-line)))))
                  (mapcar #'convert-line line-list)))
             (create-avow-box (vertical-relns horizontal-relns)
               "Return a function to create avow box of appropriate size"
               (lambda (a-box)
                  (make-avow-box a-box 
                                 (first (convert-lines (normalise-lines a-pat (list (diagonal-line a-box)))
                                                        vertical-relns horizontal-relns))))))
      (unless (my-lines a-pat) ; only compute the lines if not done before
         (construct-relations a-pat)
         (let* ((line-drawing (normalise-lines a-pat (get-lines (get-boxes a-pat))))
                 (my-sorted-verticals (get-sorted-verticals line-drawing a-pat))
                 (my-sorted-horizontals (get-sorted-horizontals line-drawing a-pat)))
            (setf (my-lines a-pat)
                   (convert-lines line-drawing my-sorted-verticals my-sorted-horizontals))
            (setf (box-dimensions a-pat)
                   (mapcar (create-avow-box my-sorted-verticals my-sorted-horizontals)
                            (get-boxes a-pat)))))
      (my-lines a-pat)))

(defmethod get-lines (a-box-list)
   "Collect the five lines needed for drawing each AVOW box"
   (flet ((top-line (a-box)
             (make-avow-line :x1 (left-of a-box) :y1 (top-of a-box) 
                             :x2 (right-of a-box) :y2 (top-of a-box)
                             :label (list 'T a-box)))
           (bottom-line (a-box)
             (make-avow-line :x1 (left-of a-box) :y1 (bottom-of a-box)
                             :x2 (right-of a-box) :y2 (bottom-of a-box)
                             :label (list 'B a-box)))
           (left-line (a-box)
             (make-avow-line :x1 (left-of a-box) :y1 (top-of a-box) 
                             :x2 (left-of a-box) :y2 (bottom-of a-box)
                             :label (list 'L a-box)))
           (right-line (a-box)
             (make-avow-line :x1 (right-of a-box) :y1 (top-of a-box) 
                             :x2 (right-of a-box) :y2 (bottom-of a-box)
                             :label (list 'R a-box))))
      (loop for a-box in a-box-list
             collect (top-line a-box)
             collect (bottom-line a-box)
             collect (left-line a-box)
             collect (right-line a-box)
             collect (diagonal-line a-box))))

(defmethod diagonal-line (a-box)
   "Return diagonal line for a-box -- called by get-lines-for and get-lines"
   (make-avow-line :x1 (left-of a-box) :y1 (bottom-of a-box) 
                   :x2 (right-of a-box) :y2 (top-of a-box)
                   :label (list 'D a-box)))


(defmethod get-sorted ((line-list list) (relations list) get-coord-1 get-coord-2)
   "General sorting routine for extracting sorted lists of line sides -- note, relations specify a partial ordering only"
   (labels ((smaller-p (test-items end-item remaining-relations)
               "Checks if the test-item is smaller than the given end-item, based on provided relations"
               (flet ((has-same-side-as-given-fn (reln)
                         "Function returned tests a relation to see if it has the same first side as some element in items"
                         (some #'(lambda (side-b) (equal-sides-p (get-first-side reln) side-b)) test-items)))
                  (let ((further-sides 
                          (mapcar #'get-second-side
                                   (remove-if-not #'has-same-side-as-given-fn remaining-relations)))
                         (unused-relations
                          (remove-if #'has-same-side-as-given-fn remaining-relations)))
                     (cond ((member end-item test-items :test #'equal-sides-p) T)
                            ((endp remaining-relations) ())
                            ((endp test-items) ())
                            (T 
                             (smaller-p further-sides end-item unused-relations)))))))
      (let ((the-sides (remove-duplicates 
                         (append (mapcar get-coord-1 line-list)
                                  (mapcar get-coord-2 line-list))
                         :test #'equal-sides-p)))
         (sort the-sides #'(lambda (x y) 
                              (or (member (create-less-than-feature x y) relations
                                            :test #'equal-features-p) ; sort-circuit direct relations
                                  (smaller-p (list x) y relations)))))))

(defmethod get-sorted-horizontals (line-list (a-pattern avow-pattern))
   "Return the sorted list of horizontal sides"
   (get-sorted line-list (horizontal-relation-list a-pattern)
               #'avow-line-x1 #'avow-line-x2))

(defmethod get-sorted-verticals (line-list (a-pattern avow-pattern))
   "Return the sorted list of vertical sides -- note reverse, because vertical coords go top-bottom, and horizontal left-right"
   (reverse 
    (get-sorted line-list (vertical-relation-list a-pattern)
                #'avow-line-y1 #'avow-line-y2)))

(defmethod get-top-of ((a-pattern avow-pattern))
   "Return the top edge of an avow pattern"
   (first (get-sorted-verticals (normalise-lines a-pattern (get-lines (get-boxes a-pattern))) a-pattern)))

(defmethod get-base-of ((a-pattern avow-pattern))
   "Return the bottom edge of an avow pattern"
   (alexandria:lastcar (get-sorted-verticals (normalise-lines a-pattern (get-lines (get-boxes a-pattern))) a-pattern)))

(defmethod get-left-of ((a-pattern avow-pattern))
   "Return the left edge of an avow pattern"
   (first (get-sorted-horizontals (normalise-lines a-pattern (get-lines (get-boxes a-pattern))) a-pattern)))

(defmethod get-right-of ((a-pattern avow-pattern))
   "Return the right edge of an avow pattern"
   (alexandria:lastcar (get-sorted-horizontals (normalise-lines a-pattern (get-lines (get-boxes a-pattern))) a-pattern)))

(defmethod get-boxes ((a-pattern avow-pattern))
   "Collect the box names referred to in given pattern"
   (remove-duplicates 
    (loop for a-feat in (get-pattern a-pattern)
           append (get-boxes a-feat))))

(defmethod get-boxes ((a-feature avow-feature))
   "Collect the box names referred to in given feature"
   (if (is-box-p a-feature)
      (list (get-box-label a-feature))
      (list (get-first-side-name a-feature) (get-second-side-name a-feature))))


; equality test for lines
(defmethod equal-features-p ((line-a t) (line-b t))
   (and (equal-sides-p (avow-line-x1 line-a) (avow-line-x1 line-b))
         (equal-sides-p (avow-line-y1 line-a) (avow-line-y1 line-b))
         (equal-sides-p (avow-line-x2 line-a) (avow-line-x2 line-b))
         (equal-sides-p (avow-line-y2 line-a) (avow-line-y2 line-b))))



;; define a class and methods for an avow-box, which holds the relevant dimensions

(defclass AVOW-BOX ()
  ((box-name :accessor box-name :initarg :box-name)
   (top-side :accessor top-side :initarg :top-side)
   (left-side :accessor left-side :initarg :left-side)
   (bottom-side :accessor bottom-side :initarg :bottom-side)
   (right-side :accessor right-side :initarg :right-side)))

(defmethod get-top-side ((a-box avow-box)) (avow-scale (top-side a-box)))
(defmethod get-bottom-side ((a-box avow-box)) (avow-scale (bottom-side a-box)))
(defmethod get-left-side ((a-box avow-box)) (avow-scale (left-side a-box)))
(defmethod get-right-side ((a-box avow-box)) (avow-scale (right-side a-box)))

(defmethod make-avow-box (box-name diagonal-line)
   (make-instance 'avow-box :box-name box-name 
                     :top-side (avow-line-y1 diagonal-line) 
                     :bottom-side (avow-line-y2 diagonal-line) 
                     :left-side (avow-line-x1 diagonal-line) 
                     :right-side (avow-line-x2 diagonal-line)))

(defmethod centre-of ((a-box avow-box))
  (make-point (floor (/ (+ (get-top-side a-box) (get-bottom-side a-box)) 2))
              (floor (/ (+ (get-left-side a-box) (get-right-side a-box)) 2))))

(defmethod top-right ((a-box avow-box))
  (make-point (get-right-side a-box) (get-top-side a-box)))

(defmethod top-left ((a-box avow-box))
  (make-point (get-left-side a-box) (get-top-side a-box)))

(defmethod bottom-right ((a-box avow-box))
  (make-point (get-right-side a-box) (get-bottom-side a-box)))

(defmethod bottom-left ((a-box avow-box))
  (make-point (get-left-side a-box) (get-bottom-side a-box)))



