(in-package :chrest-avow)

;; the circuit class provides a handle for storing a list of circuit features 
;; this list may be manipulated and compared

(defclass CIRCUIT ()
  ((my-name :accessor name :initarg my-name :type string)
   (my-circuit :accessor circuit :initarg my-circuit :type circuit-pattern)))

(defun create-new-circuit (&optional (new-name ()) (new-circuit (null-circuit-pattern)))
  (make-instance 'circuit
    'my-name new-name
    'my-circuit new-circuit))

(defmethod set-name ((a-circuit circuit) new-name)
   (setf (name a-circuit) new-name))

(defmethod set-circuit ((a-circuit circuit) (new-circuit circuit-pattern))
  (setf (circuit a-circuit) new-circuit))


(defmethod describe-me ((self circuit))
   (format t "~&Circuit: ~a~&" (name self))
   (mapc #'describe-me (get-pattern (circuit self)))
   ()) ; return nil

(defmethod describe-scene ((self circuit))
   (mapcar #'describe-resistor (get-pattern (circuit self))))


;; read/write the subject class
(defmethod write-object ((a-circuit circuit))
   "Convert a circuit into a writable list"
   (list (name a-circuit)
          (write-object (circuit a-circuit))))

(defmethod read-circuit-object ((a-list list))
   "Convert a list into a circuit object"
   (assert (= 2 (length a-list)))
   (create-new-circuit (first a-list) (read-a-pattern (second a-list))))

;; the curriculum class provides a handle for storing groups of circuits
;; ready for convenient training/testing

(defclass CURRICULUM ()
  ((my-name :accessor name :initarg my-name :type string)
   (my-curriculum :accessor curriculum :initarg my-curriculum :type list)))

(defun create-new-curriculum (&optional (new-name ()) (new-curriculum ()))
  (make-instance 'curriculum
    'my-name new-name
    'my-curriculum new-curriculum))

(defmethod set-name ((self curriculum) new-name)
  (setf (name self) new-name))

(defmethod set-curriculum ((self curriculum) new-curriculum)
  (setf (curriculum self) new-curriculum))

(defmethod describe-me ((self curriculum))
  (format t "~&Curriculum: ~a~&" (name self))
  (loop for item in (curriculum self)
        count item into total-items
        do 
        (format t "~& ~&(~a)~&" total-items)
        (describe-me item)))


; **** provide some sample circuits ****

(defun circuit-1 ()
  (create-new-circuit 
   "Single"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'GND '(1 1))))))

(defun circuit-2 ()
  (create-new-circuit
   "Parallel"
   (create-new-circuit-pattern
    (list 
     (create-new-resistor 'A 'POS 'GND '(1 1))
     (create-new-resistor 'B 'POS 'GND '(2 1))))))

(defun circuit-3 ()
  (create-new-circuit
   "Series"
   (create-new-circuit-pattern 
    (list 
     (create-new-resistor 'A 'POS 'ONE '(1 1))
     (create-new-resistor 'B 'ONE 'GND '(1 2))))))

(defun circuit-4 ()
  (create-new-circuit
   "Trio 1-2"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(2 1))
     (create-new-resistor 'B 'ONE 'GND '(1 2))
     (create-new-resistor 'C 'ONE 'GND '(3 2))))))

(defun circuit-5 ()
  (create-new-circuit
   "Trio 2-1"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(1 1))
     (create-new-resistor 'B 'POS 'ONE '(3 1))
     (create-new-resistor 'C 'ONE 'GND '(2 2))))))

(defun circuit-6 ()
  (create-new-circuit
   "Trio 2+1"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(1 1))
     (create-new-resistor 'B 'ONE 'GND '(1 3))
     (create-new-resistor 'C 'POS 'GND '(2 2))))))

(defun circuit-7 ()
  (create-new-circuit
   "Trio 1+2"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'GND '(1 2))
     (create-new-resistor 'B 'POS 'ONE '(2 1))
     (create-new-resistor 'C 'ONE 'GND '(2 3))))))

(defun circuit-8 ()
  (create-new-circuit
   "Four 1-3"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(2 1))
     (create-new-resistor 'B 'ONE 'GND '(1 2))
     (create-new-resistor 'C 'ONE 'GND '(2 2))
     (create-new-resistor 'D 'ONE 'GND '(3 2))))))

(defun circuit-9 ()
  (create-new-circuit
   "Four 1+3"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'GND '(1 2))
     (create-new-resistor 'B 'POS 'ONE '(2 1))
     (create-new-resistor 'C 'ONE 'TWO '(2 2))
     (create-new-resistor 'D 'TWO 'GND '(2 3))))))

(defun circuit-10 ()
  (create-new-circuit
   "Four 2+2"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(1 1))
     (create-new-resistor 'B 'ONE 'GND '(1 2))
     (create-new-resistor 'C 'POS 'TWO '(2 1))
     (create-new-resistor 'D 'TWO 'GND '(2 2))))))

(defun circuit-11 ()
  (create-new-circuit
   "Four 1-2-1"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(2 1))
     (create-new-resistor 'B 'ONE 'TWO '(1 2))
     (create-new-resistor 'C 'ONE 'TWO '(3 2))
     (create-new-resistor 'D 'TWO 'GND '(2 3))))))

(defun circuit-12 ()
  (create-new-circuit
   "Four 1+1-2"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'GND '(1 2))
     (create-new-resistor 'B 'POS 'ONE '(3 1))
     (create-new-resistor 'C 'ONE 'GND '(2 3))
     (create-new-resistor 'D 'ONE 'GND '(4 3))))))

(defun circuit-13 ()
  (create-new-circuit
   "Four (2+1)-1"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(1 1))
     (create-new-resistor 'B 'ONE 'TWO '(1 3))
     (create-new-resistor 'C 'POS 'TWO '(3 2))
     (create-new-resistor 'D 'TWO 'GND '(2 4))))))

(defun circuit-14 ()
  (create-new-circuit
   "Problem 25"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(1 1))
     (create-new-resistor 'B 'POS 'TWO '(4 1))
     (create-new-resistor 'C 'ONE 'THREE '(1 2))
     (create-new-resistor 'D 'TWO 'THREE '(3 2))
     (create-new-resistor 'E 'TWO 'THREE '(4 2))
     (create-new-resistor 'F 'TWO 'THREE '(5 2))
     (create-new-resistor 'G 'THREE 'GND '(2 3))))))

(defun circuit-15 ()
  (create-new-circuit
   "Problem 26"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(3 1))
     (create-new-resistor 'B 'ONE 'TWO '(3 2))
     (create-new-resistor 'C 'TWO 'THREE '(1 3))
     (create-new-resistor 'D 'TWO 'THREE '(3 3))
     (create-new-resistor 'E 'TWO 'THREE '(5 3))
     (create-new-resistor 'F 'THREE 'GND '(2 4))
     (create-new-resistor 'G 'THREE 'GND '(4 4))
     (create-new-resistor 'H 'POS 'FOUR '(6 2))
     (create-new-resistor 'I 'FOUR 'GND '(6 3))))))

(defun circuit-16 ()
  (create-new-circuit
   "six 1-2-1-2"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(2 1))
     (create-new-resistor 'B 'ONE 'TWO '(1 2))
     (create-new-resistor 'C 'ONE 'TWO '(3 2))
     (create-new-resistor 'D 'TWO 'THREE '(2 3))
     (create-new-resistor 'E 'THREE 'GND '(1 4))
     (create-new-resistor 'F 'THREE 'GND '(3 4))))))

(defun circuit-eo12 ()
  (create-new-circuit
   "four ac -> bd"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS' ONE '(1 1))
     (create-new-resistor 'C 'POS 'ONE '(2 1))
     (create-new-resistor 'B 'ONE 'GND '(1 2))
     (create-new-resistor 'D 'ONE 'GND '(2 2))))))

(defun circuit-eo13 ()
  (create-new-circuit
   "five abc -> de"
   (create-new-circuit-pattern
    (list 
     (create-new-resistor 'A 'POS 'ONE '(1 1))
     (create-new-resistor 'B 'POS 'ONE '(3 1))
     (create-new-resistor 'C 'POS 'ONE '(5 1))
     (create-new-resistor 'D 'ONE 'GND '(2 2))
     (create-new-resistor 'E 'ONE 'GND '(4 2))))))

(defun circuit-eo14 ()
  (create-new-circuit
   "six ab -> c -> def"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(2 1))
     (create-new-resistor 'B 'POS 'ONE '(4 1))
     (create-new-resistor 'C 'ONE 'TWO '(3 2))
     (create-new-resistor 'D 'TWO 'GND '(1 3))
     (create-new-resistor 'E 'TWO 'GND '(3 3))
     (create-new-resistor 'F 'TWO 'GND '(5 3))))))

(defun circuit-eo15 ()
  (create-new-circuit
   "six abc -> de -> f"
   (create-new-circuit-pattern
    (list 
     (create-new-resistor 'A 'POS 'ONE '(1 1))
     (create-new-resistor 'B 'POS 'ONE '(3 1))
     (create-new-resistor 'C 'POS 'ONE '(5 1))
     (create-new-resistor 'D 'ONE 'TWO '(2 2))
     (create-new-resistor 'E 'ONE 'TWO '(4 2))
     (create-new-resistor 'F 'TWO 'GND '(3 3))))))

(defun circuit-eo17 ()
  (create-new-circuit
   "Problem 17"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(3 1))
     (create-new-resistor 'B 'POS 'TWO '(5 1))
     (create-new-resistor 'C 'ONE 'THREE '(1 2))
     (create-new-resistor 'D 'ONE 'THREE '(3 2))
     (create-new-resistor 'E 'ONE 'TWO '(4 2))
     (create-new-resistor 'F 'THREE 'GND '(2 3))
     (create-new-resistor 'G 'TWO 'GND '(5 3))))))

(defun circuit-eo18 ()
  (create-new-circuit
   "Problem 18"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(1 1))
     (create-new-resistor 'B 'POS 'TWO '(4 1))
     (create-new-resistor 'C 'ONE 'THREE '(1 2))
     (create-new-resistor 'D 'TWO 'THREE '(3 2))
     (create-new-resistor 'E 'TWO 'FOUR '(4 2))
     (create-new-resistor 'F 'TWO 'FOUR '(6 2))
     (create-new-resistor 'G 'THREE 'GND '(2 3))
     (create-new-resistor 'H 'FOUR 'GND '(5 3))))))

(defun circuit-eo19 ()
  (create-new-circuit
   "3 x triples"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(2 1))
     (create-new-resistor 'B 'POS 'TWO '(4 1))
     (create-new-resistor 'C 'POS 'TWO '(6 1))
     (create-new-resistor 'D 'POS 'THREE '(8 1))
     (create-new-resistor 'E 'ONE 'GND '(1 2))
     (create-new-resistor 'F 'ONE 'GND '(3 2))
     (create-new-resistor 'G 'TWO 'GND '(5 2))
     (create-new-resistor 'H 'THREE 'GND '(7 2))
     (create-new-resistor 'I 'THREE 'GND '(9 2))))))

(defun circuit-eo20 ()
  (create-new-circuit
   "3 x vertical triples"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(1 2))
     (create-new-resistor 'B 'POS 'TWO '(2 1))
     (create-new-resistor 'C 'TWO 'ONE '(2 3))
     (create-new-resistor 'D 'ONE 'THREE '(1 4))
     (create-new-resistor 'E 'THREE 'FOUR '(1 6))
     (create-new-resistor 'F 'ONE 'FOUR '(2 5))
     (create-new-resistor 'G 'FOUR 'GND '(1 8))
     (create-new-resistor 'H 'FOUR 'FIVE '(2 7))
     (create-new-resistor 'I 'FIVE 'GND '(2 9))))))

(defun circuit-eo22 ()
  (create-new-circuit
   "Waterfall"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(2 1))
     (create-new-resistor 'B 'ONE 'GND '(1 2))
     (create-new-resistor 'C 'ONE 'TWO '(3 2))
     (create-new-resistor 'D 'TWO 'GND '(2 3))
     (create-new-resistor 'E 'TWO 'THREE '(4 3))
     (create-new-resistor 'F 'THREE 'GND '(3 4))
     (create-new-resistor 'G 'THREE 'FOUR '(5 4))
     (create-new-resistor 'H 'FOUR 'GND '(4 5))
     (create-new-resistor 'I 'FOUR 'GND '(6 5))))))

(defun circuit-eo23 ()
  (create-new-circuit
   "4 x triples"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(1 2))
     (create-new-resistor 'B 'POS 'TWO '(3 1))
     (create-new-resistor 'C 'TWO 'ONE '(3 3))
     (create-new-resistor 'D 'POS 'THREE '(4 1))
     (create-new-resistor 'E 'POS 'THREE '(6 1))
     (create-new-resistor 'H 'THREE 'FOUR '(5 3))
     (create-new-resistor 'F 'ONE 'FIVE '(1 4))
     (create-new-resistor 'G 'ONE 'FIVE '(3 4))
     (create-new-resistor 'J 'FIVE 'GND '(2 5))
     (create-new-resistor 'I 'FOUR 'SIX '(5 4))
     (create-new-resistor 'K 'SIX 'GND '(4 5))
     (create-new-resistor 'L 'SIX 'GND '(6 5))))))

(defun circuit-eo24 ()
  (create-new-circuit
   "4 x triples (2)"
   (create-new-circuit-pattern
    (list
     (create-new-resistor 'A 'POS 'ONE '(2 1))
     (create-new-resistor 'B 'POS 'TWO '(5 1))
     (create-new-resistor 'C 'ONE 'THREE '(1 2))
     (create-new-resistor 'D 'ONE 'THREE '(3 2))
     (create-new-resistor 'E 'TWO 'FOUR '(4 2))
     (create-new-resistor 'F 'TWO 'FOUR '(6 2))
     (create-new-resistor 'G 'THREE 'FIVE '(2 3))
     (create-new-resistor 'H 'FOUR 'SIX '(4 3))
     (create-new-resistor 'I 'FOUR 'SIX '(6 3))
     (create-new-resistor 'J 'FIVE 'GND '(1 4))
     (create-new-resistor 'K 'FIVE 'GND '(3 4))
     (create-new-resistor 'L 'SIX 'GND '(5 4))))))

(defun circuit-eo25 ()
  (create-new-circuit
   "Waterfall + series"
   (create-new-circuit-pattern
    (list 
     (create-new-resistor 'A 'POS 'ONE '(1 2))
     (create-new-resistor 'B 'ONE 'GND '(1 4))
     (create-new-resistor 'C 'POS 'TWO '(3 1))
     (create-new-resistor 'D 'TWO 'THREE '(2 3))
     (create-new-resistor 'E 'THREE 'GND '(2 5))
     (create-new-resistor 'F 'TWO 'FOUR '(4 2))
     (create-new-resistor 'G 'FOUR 'FIVE '(3 4))
     (create-new-resistor 'H 'FIVE 'GND '(3 6))
     (create-new-resistor 'I 'FOUR 'SIX '(5 3))
     (create-new-resistor 'J 'SIX 'SEVEN '(4 4))
     (create-new-resistor 'K 'SIX 'SEVEN '(6 4))
     (create-new-resistor 'L 'SEVEN 'GND '(4 6))
     (create-new-resistor 'M 'SEVEN 'GND '(6 6))))))

; **** curricula ****

(defun curriculum-1 ()
  (create-new-curriculum
   "Core circuits"
   (list
    (circuit-1)
    (circuit-2)
    (circuit-3))))

(defun curriculum-2 ()
  (create-new-curriculum
   "Study training"
   (list 
    (circuit-1) (circuit-1) (circuit-1) (circuit-1) (circuit-1)
    (circuit-1) (circuit-3) (circuit-2) (circuit-3) (circuit-2)
    (circuit-3) (circuit-2) (circuit-5) (circuit-6) (circuit-5)
    (circuit-6) (circuit-4) (circuit-7) (circuit-8) (circuit-9)
    (circuit-10) (circuit-11) (circuit-12) (circuit-13)
    ; (circuit-14) (circuit-15) ; not used, because of huge training cost
    )))

(defun curriculum-3 ()
  (create-new-curriculum
   "Easy first ordering"
   (list (circuit-1)  (circuit-3)  (circuit-2)  (circuit-5)  (circuit-6)
         (circuit-9)  (circuit-12) (circuit-11) (circuit-13) (circuit-8)
         (circuit-10) (circuit-eo12) (circuit-eo13) (circuit-eo14) (circuit-eo15)
         (circuit-14) (circuit-eo17)  (circuit-eo18)  (circuit-eo19)  (circuit-eo20)
         (circuit-15) (circuit-eo22) (circuit-eo23) (circuit-eo24) (circuit-eo25))))

(defun core-five ()
  (create-new-curriculum
   "Core five problems"
   (list (circuit-1) (circuit-3) (circuit-2) (circuit-5) (circuit-6))))

(defun complex-first ()
  (create-new-curriculum
   "Complex first tests"
   (list (circuit-eo25) (circuit-eo24) (circuit-eo23) (circuit-eo22)
         (circuit-15) (circuit-eo20) (circuit-eo19) (circuit-eo18)
         (circuit-eo17) (circuit-14) (circuit-eo15) (circuit-eo14)
         (circuit-eo13) (circuit-eo12) (circuit-10) (circuit-8)
         (circuit-13) (circuit-11) (circuit-12) (circuit-9))))

(defun simple-first ()
  (create-new-curriculum
   "Simple first tests"
   (list (circuit-9)  (circuit-12) (circuit-11) (circuit-13) (circuit-8)
         (circuit-10) (circuit-eo12) (circuit-eo13) (circuit-eo14) (circuit-eo15)
         (circuit-14) (circuit-eo17)  (circuit-eo18)  (circuit-eo19)  (circuit-eo20)
         (circuit-15) (circuit-eo22) (circuit-eo23) (circuit-eo24) (circuit-eo25))))

(defun small-test-a ()
  (create-new-curriculum
   "Small test a"
   (list
    (circuit-1)
    (circuit-2))))

(defun small-test-b ()
  (create-new-curriculum
   "Small test b"
   (list 
    (circuit-1)
    (circuit-2)
    (circuit-4))))

(defun small-test-c ()
  (create-new-curriculum
   "Small test c"
   (list 
    (circuit-1)
    (circuit-2)
    (circuit-5))))

; **** circuit list ****

(defun get-circuit-list ()
  (list (circuit-1) (circuit-2) (circuit-3) (circuit-4)
         (circuit-5) (circuit-6) (circuit-7) (circuit-8)
         (circuit-9) (circuit-10) (circuit-11) (circuit-12)
         (circuit-13) (circuit-16) (circuit-14) (circuit-15)
         (circuit-eo12) (circuit-eo13) (circuit-eo14) (circuit-eo15)
         (circuit-eo17) (circuit-eo18) (circuit-eo19) (circuit-eo20)
         (circuit-eo22) (circuit-eo23) (circuit-eo24) (circuit-eo25)
         (curriculum-1) (curriculum-2) (curriculum-3)
         (core-five) (simple-first) (complex-first)
         (small-test-a) (small-test-b) (small-test-c)))


