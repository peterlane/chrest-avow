(in-package :chrest-avow)

;;;; circuit-avow

;;;; Peter Lane, 22nd February 2001

;;;; convert circuits into AVOW diagrams

;; circuit-avow takes a circuit pattern and returns its equivalent 
;; AVOW diagram avow-pattern  -- relies on conversion routines of circuit into a list of nodes

(defmethod circuit-avow ((a-circuit circuit-pattern))
   "Circuit-avow converts given circuit into a collection of avow boxes"
   (flet ((create-single-box (a-resistor)
             (create-new-avow-pattern (create-box-feature (descriptor a-resistor))))
           (create-avow-boxes (a-circuit)
             (create-new-avow-pattern
              (remove-duplicates 
               (reduce #'append
                        (append (mapcar #'get-vertical-relations (get-node-list a-circuit))
                                 (mapcar #'get-horizontal-relations (get-node-list a-circuit))))
               :test #'equal-features-p))))
      (let ((new-avow
              (cond ((< (size-of a-circuit) 1) (null-avow-pattern)) ; nothing to do
                     ((equal 1 (size-of a-circuit)) 
                      (create-single-box (first (get-pattern a-circuit)))) ; single resistor
                     (T (create-avow-boxes a-circuit))))) ; general case
         (get-lines-for new-avow) ; set up the lines for the avow diagram
         new-avow)))


; get-vertical-relations returns list of vertical relations from the given node
(defmethod get-vertical-relations ((a-node circuit-graph-node))
   "Collect equal relations between the tops of the resistors to this node, the bottoms of those from, and the top-bottom across the node"
   (flet ((get-equal-sides (the-side node-side)
             (remove-duplicates
              (loop for first-node in (apply node-side a-node ())
                     append
                     (loop for second-node in (apply node-side a-node ())
                            when (not (equal first-node second-node))
                            collect (create-reln-feature
                                     (apply the-side (smaller-label-p first-node second-node) ())
                                     (apply the-side (larger-label-p first-node second-node) ()))))
              :test #'equal-features-p)))
      (let ((equal-bases (get-equal-sides #'bottom-of #'get-from))
             (equal-tops (get-equal-sides #'top-of #'get-to))
             (equal-top-base ()))
         (dolist (first-node (get-from a-node))
            (dolist (second-node (get-to a-node))
               (push (create-reln-feature (bottom-of first-node) (top-of second-node)) 
                      equal-top-base)))
         (append equal-bases equal-tops equal-top-base))))


; get-horizontal-relations returns list of horizontal relations from given node
(defmethod get-horizontal-relations ((a-node circuit-graph-node))
   "Return a list of equal relations for the left-right ordering of elements to/from the given node"
   (labels ((get-lr-list (a-list &optional (result ()))
               "Run through list of nodes and return relns for adjacent resistors"
               (if (or (endp a-list) (endp (rest a-list)))
                  result
                  (get-lr-list 
                   (rest a-list) 
                   (cons (create-reln-feature (right-of (first a-list)) (left-of (second a-list))) 
                          result)))))
      (let ((equal-lefts 
              (if (and (get-from a-node) (get-to a-node))
                 (list (create-reln-feature (left-of (get-first-from a-node))
                                             (left-of (get-first-to a-node))))
                 ()))
             (equal-rights
              (if (and (get-from a-node) (get-to a-node))
                 (list 
                  (create-reln-feature (right-of (get-last-from a-node))
                                       (right-of (get-last-to a-node))))
                 ()))
             (equal-lrs (append (get-lr-list (reverse (get-from a-node)))
                                 (get-lr-list (reverse (get-to a-node))))))
         
         (append equal-lefts equal-rights equal-lrs))))


