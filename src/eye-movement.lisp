(in-package :chrest-avow)

;;;; eye-movement.lisp

;;;; holds code for managing CHREST eye movements


;;; scan the image

(defmethod scan-given-scene ((a-subject chrest-subject) (a-pattern pattern) a-name)
  "Scans the given scene - learning or recall depends on state set by caller"
  (reset-fixation-history a-subject)
  (reset-active-node a-subject)
  (clear-chrest-stm a-subject)
  (clear-chrest-focused-pattern a-subject)
  (make-viewed-object a-subject a-pattern a-name)
  (notify-observers-of-environment-change a-subject)
  (do ((safety-net 1 (+ 1 safety-net))
       (current-fixation (get-initial-fixation-point a-subject (eye-strategy a-subject))
                         (get-next-fixation-point a-subject current-fixation)))
      ((or (> safety-net 100)
           (pattern-completed-p a-subject a-pattern)
           (exceeds-limits-p current-fixation (get-field-width a-subject) (get-field-height a-subject)))
       (add-to-fixation-history a-subject current-fixation))
    (add-to-fixation-history a-subject current-fixation)
    (add-to-stm a-subject (extract-novel-pattern a-subject (get-fixation-point current-fixation))))
  (notify-observers-of-fixation-record-change a-subject))

(defmethod get-field-width ((a-subject chrest-subject))
  (- (point-h (get-single-pattern-field-size (get-viewed-object-pattern a-subject))) 27))

(defmethod get-field-height ((a-subject chrest-subject))
  (- (point-v (get-single-pattern-field-size (get-viewed-object-pattern a-subject))) 17))


; get-next-fixation-point 
(defmethod get-next-fixation-point ((a-subject chrest-subject) (current-fixation fixation))
  (cond ((and (valid-node-p (active-node a-subject)) (active-node-image-incomplete-p a-subject))
         (get-complete-image-fixation-point a-subject current-fixation)) ; check for image completion
        ((has-children-p (active-node a-subject))
         (store-if-valid-node a-subject)
         (get-known-child-fixation-point a-subject current-fixation)) ; check for children - include all STM nodes?
        (T 
         (get-novel-or-default-fixation-point a-subject current-fixation))))

(defmethod valid-node-p ((a-node node))
  "A node is 'valid' for CHREST+ if it has an equivalence link"
  (has-equivalence-p a-node))

(defmethod pattern-completed-p ((a-subject chrest-subject) (a-pattern pattern))
  "T on learning if STM-contents equals the given pattern 
      T on testing  if recognised-chunks equal the given pattern"
  (equal-patterns-p (get-stored-pattern a-subject) a-pattern))

(defmethod get-stored-pattern ((a-subject chrest-subject))
  "Returns STM-contents if in learning mode, or recognised-chunks if in recall mode"
  (cond ((learn-mode-p a-subject)
         (get-stm-contents (get-STM a-subject)))
        ((recall-mode-p a-subject)
         (get-recognised-chunks-pattern a-subject))
        (T
         (error "Invalid subject mode"))))

(defmethod store-if-valid-node ((a-subject chrest-subject))
  "Store the active-node as possible decomposition, assuming it is valid"
  (when (valid-node-p (active-node a-subject))
    (add-to-recognised-patterns a-subject (extract-novel-pattern a-subject (get-last-fixation a-subject)))))

;; retrieve the next fixation point depending on the current goal
(defmethod get-novel-or-default-fixation-point ((a-subject chrest-subject) (current-fixation fixation))
  "Returns a novel or default fixation based on current point, depending on whether it can find novel items within periphery"
  (store-if-valid-node a-subject)
  (reset-active-node a-subject)
  (clear-chrest-focused-pattern a-subject)
  (let ((novel-pts (get-novel-saccades a-subject current-fixation)))
    (if novel-pts
      (create-novel-fixation (alexandria:random-elt novel-pts))
      (get-next-default-fixation-point a-subject (eye-strategy a-subject) current-fixation))))

(defmethod get-known-child-fixation-point ((a-subject chrest-subject) (current-fixation fixation))
  "Returns a point which results in following a known test link, or else resets and takes novel/default point if nothing matches"
  (let ((potential-pts (favour-any-novel-saccades a-subject (find-points-which-match-test a-subject current-fixation))))
    (if potential-pts
      (take-test-fixation (alexandria:random-elt potential-pts) a-subject)
      (get-novel-or-default-fixation-point a-subject current-fixation)))) "Also recall/learn"

(defmethod get-complete-image-fixation-point ((a-subject chrest-subject) (current-fixation fixation))
  "Checks if point helps to complete current image -- checks against focused-pattern"
  (let ((potential-pts 
         (favour-any-novel-saccades a-subject
                                    (remove-if-not #'(lambda (a-point) (can-fill-out-image-p a-subject a-point))
                                                   (cons (get-fixation-point current-fixation)
                                                         (get-possible-saccade-list a-subject current-fixation))))))
    (if potential-pts
      (progn
        (let ((new-pt (alexandria:random-elt potential-pts)))
          (add-to-chrest-focused-pattern a-subject (extract-pattern a-subject new-pt))
          (create-image-fixation new-pt (get-node-image (active-node a-subject)))))
      (get-novel-or-default-fixation-point a-subject current-fixation)))) "Also recall/learn "

(defmethod find-points-which-match-test ((a-subject chrest-subject) (current-fixation fixation))
  "Returns a list of those points which provide a match for tests below subject's current active-node"
  (remove-if-not #'(lambda (a-point) (has-matching-test-p a-point a-subject))
                 (get-possible-saccade-list a-subject current-fixation)))

(defmethod get-matching-test (a-point (a-subject chrest-subject))
  "Returns the link below active-node whose test matches the image at a-point 
   -- or returns null link using root node if none found
  30/4/01 - not sufficient just to check link-key, but must confirm entire node-contents
   matches that of recent fixations"
  (let ((target-image (extract-recently-seen-pattern a-subject a-point)))
     (loop for test-link in (get-node-children (active-node a-subject))
            when (apply (matching-child-test-fn target-image) test-link ()) 
            return test-link
            finally (return (create-new-link (null-pattern) (get-LTM a-subject))))))

(defmethod has-matching-test-p (a-point (a-subject chrest-subject))
  "Indicates whether there exists a node below active-node whose test matches the image at a-point
  30/4/01 - not sufficient just to check link-key, but must confirm entire node-contents
   matches that of recent fixations"
  (let ((target-image (extract-recently-seen-pattern a-subject a-point)))
     (some (matching-child-test-fn target-image) (get-node-children (active-node a-subject)))))

(defmethod matching-child-test-fn ((target-image pattern))
   (lambda (test-link) (matching-patterns-p (get-node-contents (link-node test-link)) target-image)))

(defmethod take-test-fixation (a-point (a-subject chrest-subject))
  "Update subject's active node and return the new fixation"
  (let ((new-test (get-matching-test a-point a-subject)))
    (set-active-node a-subject (link-node new-test))
    (add-to-chrest-focused-pattern a-subject 
                                   (get-my-match (get-node-contents (link-node new-test)) 
                                                 (extract-recently-seen-pattern a-subject a-point)))
    (create-test-fixation a-point 
                          (get-my-match (get-node-contents (link-node new-test)) 
                                        (extract-recently-seen-pattern a-subject a-point)))))

(defmethod reset-active-node ((a-subject chrest-subject))
  (set-active-node a-subject (get-LTM a-subject)))

(defmethod set-active-node ((a-subject chrest-subject) (a-node node))
  (setf (active-node a-subject) a-node))

(defmethod active-node-image-incomplete-p ((a-subject chrest-subject))
  "Checks that the image of the active-node has not been completely recognised in current-focus"
  (not (matching-patterns-p (get-node-image (active-node a-subject)) 
                            (get-chrest-focused-pattern a-subject))))

(defmethod can-fill-out-image-p ((a-subject chrest-subject) a-point)
  "Tests whether (focused-pattern + pattern at a-point) fills out more of the active node's image 
   than just focused-pattern."
  (let ((current-image (get-node-image (active-node a-subject))))
    (is-larger-p (get-my-match (extract-recently-seen-pattern a-subject a-point) 
                               current-image)
                 (get-my-match (get-chrest-focused-pattern a-subject)
                               current-image))))

(defmethod extract-pattern ((a-subject chrest-subject) a-point)
  "Return the pattern at current fixation point"
  ; not sure why the add-points is required here - needed to get the fixation-point and extraction-point
  ; identical, but thought I was using identical coord systems!  Recheck how this code works
  (extract-features (get-viewed-object-pattern a-subject) 
                    (add-points a-point (make-point 27 17))
                    (my-focal-size a-subject)))

(defmethod extract-novel-pattern ((a-subject chrest-subject) a-point)
  "Return novel pattern at current fixation point - use STM in training, recalled chunks in recall"
  (create-new-pattern-for (extract-pattern a-subject a-point)
                          (loop for a-feat in (get-pattern (extract-pattern a-subject a-point))
                                when (feature-not-in-pattern-p a-feat 
                                                       (get-stored-pattern a-subject))
                                collect a-feat)))

(defmethod extract-recently-seen-pattern ((a-subject chrest-subject) a-point)
  "Return the pattern at current fixation point combined with pattern in current-focus"
  (let* ((visible-pattern (extract-pattern a-subject a-point))
         (current-focus (get-chrest-focused-pattern a-subject)))
    (combine-patterns (list current-focus visible-pattern))))

; get-initial-fixation-point returns the first point, switches on eye-strategy type

(defmethod get-initial-fixation-point ((a-subject chrest-subject) (s eye-strategy-reading))
  (declare (ignore a-subject s))
  (create-default-fixation (make-point 0 0)))  ; starts at top-left

(defmethod get-initial-fixation-point ((a-subject chrest-subject) (s eye-strategy-reverse))
  (declare (ignore s))
  (create-default-fixation 
   (make-point (get-field-width a-subject) (get-field-height a-subject)))) ; starts at bottom-right

(defmethod get-initial-fixation-point ((a-subject chrest-subject) (s eye-strategy-random))
  (declare (ignore s))
  (create-default-fixation 
   (get-random-point (get-field-width a-subject) (get-field-height a-subject)))) ; picks a random-point

(defmethod get-initial-fixation-point ((a-subject chrest-subject) (s eye-strategy-memory))
  (declare (ignore s))
  (create-default-fixation 
   (get-random-point (get-field-width a-subject) (get-field-height a-subject)))) ; picks a random-point

(defmethod get-initial-fixation-point ((a-subject chrest-subject) (s eye-strategy-follow))
  (declare (ignore s))
  (create-default-fixation 
   (get-random-point (get-field-width a-subject) (get-field-height a-subject)))) ; picks a random-point

(defmethod get-initial-fixation-point ((a-subject chrest-subject) (s eye-strategy-none))
  (declare (ignore a-subject s))
  (create-default-fixation (make-point 0 0)))  ; place at top-left


; get-next-default-fixation-point returns the first point, switches on eye-strategy type

(defmethod get-next-default-fixation-point ((a-subject chrest-subject) (s eye-strategy-reading) (current-fixation fixation))
  (declare (ignore s))
  (create-default-fixation (next-reading-point current-fixation (get-field-width a-subject))))

(defmethod get-next-default-fixation-point ((a-subject chrest-subject) (s eye-strategy-reverse) (current-fixation fixation))
  (declare (ignore s))
  (create-default-fixation (next-reverse-reading-point current-fixation (get-field-width a-subject))))

(defmethod get-next-default-fixation-point ((a-subject chrest-subject) (s eye-strategy-random) (current-fixation fixation))
  (declare (ignore s current-fixation))
  (create-default-fixation (get-random-point (get-field-width a-subject) (get-field-height a-subject))))

(defmethod get-next-default-fixation-point ((a-subject chrest-subject) (s eye-strategy-memory) (current-fixation fixation))
  (declare (ignore s current-fixation))
  (create-default-fixation 
   (get-random-point-avoiding-memory a-subject (get-field-width a-subject) (get-field-height a-subject))))

(defmethod get-next-default-fixation-point ((a-subject chrest-subject) (s eye-strategy-follow) (current-fixation fixation))
  (declare (ignore s))
  (create-default-fixation 
   (get-random-point-following-last a-subject current-fixation)))

(defmethod get-next-default-fixation-point ((a-subject chrest-subject) (s eye-strategy-none) (current-fixation fixation))
  (declare (ignore a-subject s current-fixation))
  (create-default-fixation (make-point 0 0)))

(defmethod next-reading-point ((current-fixation fixation) field-width)
  (if (> (point-h (get-fixation-point current-fixation)) (- field-width 20))
    (make-point 0 (+ (point-v (get-fixation-point current-fixation)) 20))
    (add-points (get-fixation-point current-fixation) (make-point 20 0))))

(defmethod next-reverse-reading-point ((current-fixation fixation) field-width)
  (if (< (point-h (get-fixation-point current-fixation)) 9)
    (make-point field-width (- (point-v (get-fixation-point current-fixation)) 20))
    (subtract-points (get-fixation-point current-fixation) (make-point 20 0))))

(defmethod get-random-point (field-width field-height)
  (make-point (random field-width) (random field-height)))

(defmethod get-random-point-avoiding-memory ((a-subject chrest-subject) field-width field-height)
  (let ((potential-point (get-random-point field-width field-height)))
    (loop as safety-net below 10
          until (avoids-previous-points-p potential-point (my-focal-size a-subject) 
                                          (mapcar #'get-fixation-point (get-fixation-history a-subject)))
          do (setf potential-point (get-random-point field-width field-height)))
    potential-point))

; try to follow last saccade, but if that puts you out of field-of-view, then take a random jump to periphery
(defmethod get-random-point-following-last ((a-subject chrest-subject) (current-fixation fixation))
  (let ((potential-point (add-points (get-fixation-point current-fixation) (get-last-saccade a-subject)))
        (alternative-saccades (get-possible-saccade-list a-subject current-fixation)))
    (if (or (equal potential-point (get-fixation-point current-fixation)) 
            (exceeds-limits-p potential-point (get-field-width a-subject) (get-field-height a-subject)))
      (if alternative-saccades
        (alexandria:random-elt alternative-saccades)
        (get-random-point (get-field-width a-subject) (get-field-height a-subject)))
      potential-point)))

(defmethod avoids-previous-points-p (potential-point focal-size (fixation-history list))
  "Returns T if the potential point is more than focal-size distance from every point in the fixation-history"
  (loop for a-point in fixation-history
        never (points-adjacent-p potential-point focal-size a-point)))

(defmethod exceeds-limits-p ((current-fixation fixation) width height)
  (exceeds-limits-p (get-fixation-point current-fixation) width height))

(defmethod exceeds-limits-p (current-point width height)
  (or (< (point-v current-point) -9)
      (< (point-h current-point) -9)
      (> (point-v current-point) height)
      (> (point-h current-point) width)))

(defmethod get-possible-saccade-list ((a-subject chrest-subject) (current-fixation fixation))
  "Uses size of field-of-view to provide a list of points in periphery - filters out those outside field."
  (flet ((outside-view-p (a-point) 
           (exceeds-limits-p a-point (get-field-width a-subject) (get-field-height a-subject))))
     (remove-if #'outside-view-p
                  (let* ((field-size (my-focal-size a-subject))
                          (mid-point (floor (* field-size (cos (/ pi 4)))))
                          (current-point (get-fixation-point current-fixation)))
                     (list (add-points current-point (make-point field-size 0))
                            (add-points current-point (make-point 0 field-size))
                            (add-points current-point (make-point (- field-size) 0))
                            (add-points current-point (make-point 0 (- field-size)))
                            (add-points current-point (make-point mid-point mid-point))
                            (add-points current-point (make-point (- mid-point) mid-point))
                            (add-points current-point (make-point mid-point (- mid-point)))
                            (add-points current-point (make-point (- mid-point) (- mid-point)))
                            current-point)))))

; search for a novel element within saccade list
(defmethod get-novel-saccades ((a-subject chrest-subject) (current-fixation fixation))
   (remove-if (previously-seen-point-fn a-subject)
                (get-possible-saccade-list a-subject current-fixation)))

; by returning only the novel saccades, this routine favours novelty
; - however, if there are no novel saccades, it returns the original list
(defmethod favour-any-novel-saccades ((a-subject chrest-subject) (fixation-list list))
   (let ((novel-saccades 
           (remove-if (previously-seen-point-fn a-subject) fixation-list)))
      (if novel-saccades novel-saccades fixation-list)))

(defmethod previously-seen-point-fn ((a-subject chrest-subject))
   "Return a function to check if point has been seen before by given subject"
   (lambda (a-point)
      (seen-before-p a-subject (extract-novel-pattern a-subject a-point))))
