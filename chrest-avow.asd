;; Definition of AVOW-model package for CHREST
;; -- written by Peter Lane, 1998-2019
;; -- developed in Macintosh Common Lisp whilst at CREDIT, University of Nottingham 1998-2001
;; -- repackaged in 2019, and tested with CLISP and SBCL

(defsystem "chrest-avow"
           :description "CHREST model for learning circuit and AVOW diagrams"
           :version "1.0.0"
           :serial t
           :components ((:file "src/package")
                        (:file "src/utility")
                        (:file "src/properties")
                        (:file "src/observer")
                        (:file "src/fixations")
                        (:file "src/pattern")
                        (:file "src/circuit-pattern")
                        (:file "src/circuit-avow")
                        (:file "src/avow-pattern")
                        (:file "src/circuit")
                        (:file "src/resistor")
                        (:file "src/chrest")
                        (:file "src/subject")
                        (:file "src/eye-movement")
                        (:file "src/stm-queue") 
                       )
          :depends-on ("alexandria")
          :in-order-to ((test-op (test-op "chrest-avow/tests"))))

(defsystem "chrest-avow/tests"
           :description "CHREST modified for AVOW diagrams"
           :serial t
           :components ((:file "tests/test-runner")
                        (:file "tests/test-pattern")
                        (:file "tests/test-avow-pattern")
                        (:file "tests/test-chrest")
                        (:file "tests/test-circuit")
                        (:file "tests/test-circuit-avow")
                        (:file "tests/test-circuit-pattern")
                        (:file "tests/test-resistor")
                        (:file "tests/test-stm-queue")
                        (:file "tests/test-subject")
                        (:file "tests/test-utility")
                        )
           :depends-on ("chrest-avow")
           :perform (test-op (o c) (symbol-call :chrest-avow :run-tests)))

